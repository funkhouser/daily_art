# Daily Art

Ideally this is a repository that becomes something of a gallery documenting different permutations of ideas
and how they intersect with each other. Obviously by the name
I hope that this becomes a daily endeavor: when railing against 
"creative block" with a strict routine does the outcome become
even more interesting?


## Initial Setup

Populate local.properties with the following information (day requirements may vary)

```
primitivePath={path to your main.go file for primitive}

```

## Day by Day

Run `./gradlew newDay` to create a new day definition

## Versioning

This is a rather awkward versioning problem. Ideally, running
any given days application will result (sans the random seeds which I will take
pains to document) in a true reproduction. That said, many times these are constructs
that have been mutated and combined in order to create something novel.

In these cases, does it make sense to better the common/shared libraries? What happens when
breaking api changes are needed? Preferably this does not result in requiring all entities in the code 
path to be represented in that single package/file or similar. The upside to doing that of course is that
each subsegment becomes very simple to export even if the namespace of the overall program gets wildly messy
(this could be helped by making everything package private I suppose).

The other possibly future (and the one that happened with "process") is mutating everything and just relying
on GIT to provide the historical view. Ideally this will not happen here. Requiring the user (me) to load an
old GIT branch is
1. Too much overhead to remember committing every cool idea separately and
1. Difficult to merge ideas from different "time periods"



Options:
1. Git versioning
    1. Possible mitigation: Creating a daily git hook that looks for new material and auto commits?
1. Package versioning
    1. Each daily art consolidates all its dependents into a single package
    1. Main obvious downsides are verbosity and duplication. Maybe it will also be annoying to deal with incompatible 
    versions of things? 
1. Core versioning
    1. Build out a shared core that is designed never to change. Each daily will contain all mutating classes
    within itself
    1. This should be easier to accomplish with both Hype (hopefully its up to date enough) and the data classes
    from Process carried over
    1. This will be combined with git versioning
        1. Going with this route: including a regenerate-all gradle target that creates all work!
        
    ## Outputting
    Things generally end up in "/outputs" under the appropriate namespace.
    
    To create png files from .tif files: 
    `for i in *.tif; do sips -s format png $i --out $i.png; done`