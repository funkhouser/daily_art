
import art.funk.daily.declareConvertOutputTask
import art.funk.daily.declareDayCreateTask
import art.funk.daily.declarePostDayTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.io.FileReader
import java.util.Properties

plugins {
    kotlin("jvm") version "1.3.31"
}

group = "art.funk"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(fileTree(mapOf("dir" to "lib", "include" to listOf("*.jar"))))

    implementation(group = "org.processing", name = "core", version = "3.3.7")
    implementation(group = "quil", name = "processing-svg", version = "3.3.7")
    implementation(group = "org.apache.xmlgraphics", name = "batik-transcoder", version = "1.9")
    implementation(group = "org.apache.xmlgraphics", name = "batik-codec", version = "1.9")
    implementation(group = "org.apache.xmlgraphics", name = "batik-swing", version = "1.9")
    implementation(group = "org.apache.xmlgraphics", name = "batik-svggen", version = "1.9")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

declareDayCreateTask()
declarePostDayTask()
declareConvertOutputTask()

fun loadProperties(): Map<String, Any> {
    val propertiesFile = File(project.rootDir, "local.properties")
    val properties = Properties()
    properties.load(FileReader(propertiesFile))
    val returnMap = mutableMapOf<String, Any>()
    properties.forEach { (key, value) ->
        returnMap.put(key as String, value)
    }
    return returnMap
}

project.rootDir
    .resolve("src")
    .resolve("main")
    .resolve("kotlin")
    .resolve("art")
    .resolve("funk")
    .resolve("daily")
    .resolve("y2019")
    .walkTopDown().forEach { file: File ->
        if (!file.isDirectory) {
            val dayTask = tasks.create<JavaExec>("run19${file.nameWithoutExtension}") {
                group = "art.funk.${file.parentFile.name}"
                classpath = sourceSets["main"].runtimeClasspath
                main = "art.funk.daily.y2019." + file.name.substring(0, 3).decapitalize() + "." + file.nameWithoutExtension
                doFirst {
                    systemProperties = loadProperties()
                }
            }
        }
    }