repositories {
    maven("https://jitpack.io")
}

plugins {
    `kotlin-dsl`
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation("com.fasterxml.jackson.module:jackson-module-jsonSchema:2.9.8")
    implementation("com.github.jkcclemens:khttp:-SNAPSHOT")
}

repositories {
    jcenter()
}