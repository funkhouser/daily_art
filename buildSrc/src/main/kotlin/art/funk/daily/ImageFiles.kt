package art.funk.daily

import java.io.File

object ImageFiles {

    fun convertToPng(tiffFile: File): File {
        val process = ProcessBuilder(listOf("sips", "-s", "format", "png", tiffFile.name, "--out", tiffFile.nameWithoutExtension + ".png"))
            .directory(tiffFile.parentFile.absoluteFile)
            .inheritIO()
            .start()

        val errorCode = process.waitFor()
        if (errorCode != 0) {
            throw RuntimeException("Error: $errorCode")
        }

        return tiffFile.resolveSibling(tiffFile.nameWithoutExtension + ".png")
    }
}