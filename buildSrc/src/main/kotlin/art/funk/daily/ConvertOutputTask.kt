package art.funk.daily

import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.TaskProvider
import java.io.File

open class ConvertOutputTask : DefaultTask() {

    init {
        group = "art.funk"
        description = "Convert from .tif output to .png"
    }

    companion object {
        const val TASK_NAME = "convertOutput"
    }


    @TaskAction
    fun run() {
        // Lazy way to select a day manually. Note that the month will be zero-offset instead of 1 (i.e. Jan = 0)
        val tiffFile = byMostRecentFile()
//        val tiffFile = byCurrentDate()

        if (!tiffFile.exists()) {
            throw RuntimeException(tiffFile.absolutePath + " Does not exist")
        }

        println("Converting ${tiffFile.absolutePath}")

        val resultFile = ImageFiles.convertToPng(tiffFile)
        if (!resultFile.exists()) {
            throw RuntimeException("${resultFile.name} Does not exist")
        }

        println("Resulting file ${resultFile.absolutePath}")
    }

    fun byCurrentDate(): File {
        val date = DayParser.SimpleDay(2019, 4, 31)
//        val date = DayParser.getCurrentDay()

        val packageName = "art.funk.daily.y${date.year}.${date.getMonthString()}"
        val outputsDirectory = project.rootDir.absoluteFile.resolve("outputs")

        val expectedFileName = packageName + "." + date.getMonthString().capitalize() + date.day + "Sketch.tif"
        val tiffFile = outputsDirectory.resolve(expectedFileName)

        return tiffFile
    }

    fun byMostRecentFile(): File {
        val outputsDirectory = project.rootDir.absoluteFile.resolve("outputs")

        val tiffFile = outputsDirectory
            .listFiles()
            .filter { it.extension == "tif" }
            .sortedByDescending { it.lastModified() }
            .first()

        return tiffFile

    }
}

fun Project.declareConvertOutputTask() = tasks.register(ConvertOutputTask.TASK_NAME, ConvertOutputTask::class.java)

val Project.convertOutputTaskProvider: TaskProvider<ConvertOutputTask>
    get() = tasks.named(ConvertOutputTask.TASK_NAME, ConvertOutputTask::class.java)