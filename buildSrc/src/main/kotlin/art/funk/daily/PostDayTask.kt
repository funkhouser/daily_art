package art.funk.daily

import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.TaskProvider
import org.gradle.kotlin.dsl.named
import java.io.File
import javax.imageio.ImageIO

open class PostDayTask : DefaultTask() {

    init {
        group = "art.funk"
        description = "Posts the current day result and posts it to the given api"
    }

    companion object {
        const val TASK_NAME = "postDay"
    }

    @TaskAction
    fun run() {
        // TODO Allow for an override. Guess its not super necessary
//        val date = DayParser.getCurrentDay()
        val date = DayParser.SimpleDay(2019, 4, 11)

        // TODO Find the appropriate file (or have it run the dynamically generated task)
        // For now, assume that the file has already been generated
        project.rootDir
            .resolve("outputs")
            .listFiles()
            .filter { file -> file.nameWithoutExtension.startsWith("art.funk.daily.y${date.year}.${date.getMonthString()}.${date.getMonthString().capitalize()}") }
            .forEach { file ->
                val postContent = PostContent(file)

                val response = khttp.post(
                    url = "https://api.tumblr.com/v2/blog/incredulity/posts",
                    data = postContent.createRequest())


                println(response.text)
            }

    }

}

// TODO Figure out the best way to extract the size data from the classes

class PostContent(val imageFile: File) {
    fun createRequest(): String {
        return """{
    "content": [
        {
            "type": "image",
            "media": [
                {
                    "type": "${getImageType()}",
                    "identifier": "${imageFile.name}",
                    "width": ${720},
                    "height": ${720}
                }
            ]
        }
    ]
}"""
    }

    private fun getImageType(): String {
        return when(imageFile.extension) {
            // Bitmap
            "bmp" -> "image/bmp"
            "gif" -> "image/gif"
            "png" -> "image/png"
            "jpg" -> "image/jpeg"
            "jpeg" -> "image/jpeg"
            "tif" -> "image/tiff"
            "tiff" -> "image/tiff"
            "svg" -> "image/svg+xml"
            else -> "image/png"
        }
    }
}


fun Project.declarePostDayTask() =
    tasks.register(PostDayTask.TASK_NAME, PostDayTask::class.java)

val Project.postDayTaskProvider: TaskProvider<PostDayTask>
    get() = tasks.named(PostDayTask.TASK_NAME, PostDayTask::class)