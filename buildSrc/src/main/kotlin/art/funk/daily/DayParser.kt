package art.funk.daily

import java.util.*

object DayParser {

    val months = listOf(
        "jan",
        "feb",
        "mar",
        "apr",
        "may",
        "jun",
        "jul",
        "aug",
        "sep",
        "oct",
        "nov",
        "dec"
    )

    data class SimpleDay(
        val year: Int,
        val month: Int,
        val day: Int) {


        fun getMonthString(): String {
            return months[month]
        }
    }

    fun getCurrentDay(): SimpleDay {
        val currentDate = Calendar.getInstance()
        val year = currentDate.get(Calendar.YEAR)
        val month = currentDate.get(Calendar.MONTH)
        val day = currentDate.get(Calendar.DAY_OF_MONTH)

        return SimpleDay(year, month, day)
    }
}