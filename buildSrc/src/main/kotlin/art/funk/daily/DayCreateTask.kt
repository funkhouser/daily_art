package art.funk.daily

import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.TaskProvider
import java.io.File
import java.util.*

open class DayCreateTask : DefaultTask() {

    init {
        group = "art.funk"
        description = "Creates a new processing class applet for the given day"
    }

    companion object {
        const val TASK_NAME = "newDay"

        const val OBJECT_NAME = "\${objectName}"
        const val YEAR = "\${year}"
        const val MONTH = "\${month}"
    }


    @TaskAction
    fun run() {
        val date = DayParser.getCurrentDay()

        val packageName = "art.funk.daily.y${date.year}.${date.getMonthString()}"
        val packageDirectory = project.rootDir.absolutePath + "/src/main/kotlin/" + packageName.replace(".", "/")

        File(packageDirectory).mkdirs()

        var objectName = date.getMonthString().capitalize() + date.day
        val fileName = objectName + ".kt"
        var newFile = File(packageDirectory + "/$fileName")

        if (newFile.exists()) {
            val resultPair = findDayIterationFile(newFile, 2)
            newFile = resultPair.first
            objectName = objectName + "_${resultPair.second}"
        }

        newFile.createNewFile()

        // TODO Write some stuff to this file
        val templateString = DayCreateTask::class.java.getResource("dailyTemplate").readText()

        val classString = templateString
            .replace(YEAR, "y" + date.year)
            .replace(MONTH, date.getMonthString())
            .replace(OBJECT_NAME, objectName)

        newFile.writeText(classString)

        println("Created ${newFile.absolutePath}")
    }

    private fun findDayIterationFile(file: File, i: Int): Pair<File, Int> {
        if (i == 100) {
            throw GradleException("what")
        }
        val newFile = File(file.absolutePath.substringBefore(".kt") + "_$i.kt")
        return if (newFile.exists()) {
            findDayIterationFile(file, i + 1)
        } else {
            newFile to i
        }
    }
}

fun Project.declareDayCreateTask() =
         tasks.register(DayCreateTask.TASK_NAME, DayCreateTask::class.java)

val Project.dayCreateTaskProvider: TaskProvider<DayCreateTask>
    get() = tasks.named(DayCreateTask.TASK_NAME, DayCreateTask::class.java)