package art.funk.daily.y2019.jul

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.random.Random

object Jul9 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jul9Sketch::class.java)
    }
}


// See https://processing.org/reference/libraries/svg/index.html
class Jul9Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720
    val random = Random(309)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        noLoop()
        beginRecord(SVG, "outputs/" + javaClass.name + ".svg")
    }

    override fun draw() {
        super.draw()
        background(Color(255, 255, 255, 255))
        stroke(Color(0, 0, 0, 0))
        fill(Color(100, 180, 255, 20))

        for (i in 0..500) {
            rectViaSizeOffsetRotation(
                DPoint(random.nextDouble(720.0), random.nextDouble(720.0)),
                random.nextDouble(300.0),
                random.nextDouble(300.0),
                0.0,
                random.nextDouble(2 * Math.PI)
            )
        }
        endRecord()
    }
}