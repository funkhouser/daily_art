package art.funk.daily.y2019.jul

import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color
import kotlin.math.cos
import kotlin.math.sin
import kotlin.random.Random

object Jul5_2 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jul5_2Sketch::class.java)
    }
}

class Jul5_2Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val random = Random(400)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {

        stroke(Color(10, 10, 10, 20))
        background(Color(255, 255, 255, 255))

        // Circles similar to the interpolation piece, but instead just have bezier
        // connections for each of the interpolated points.

        // In short, create a series of circles, with pertubations for each.
        // Move them along the x-axis with potentially a random-ness applied to the
        // y-axis if not some other function.  This combined with a rotation factor
        // will allow for a total piece


        // Another idea, maybe have a secondary circle set that is just for the bezier manipulations?
        val spiral = createSpiral()

        val xDelta = myWidth.toDouble() / (spiral.targetCircles.size - 1)
        val yDeltas = spiral.targetCircles.map { circle -> (spiral.radius * random.nextDouble() - spiral.radius / 2) / spiral.targetCircles.size }


        for (circleIndex in 0 until (spiral.targetCircles.size - 1)) {
            val circle0 = spiral.targetCircles[circleIndex]
            val circle1 = spiral.targetCircles[circleIndex + 1]

            val x0 = circleIndex * xDelta
            val y0 = myHeight / 2 + yDeltas.subList(0, circleIndex + 1).sum()
            val rotation0 = circleIndex * spiral.rotationPerIteration

            val x1 = x0 + xDelta
            val y1 = y0 + yDeltas[circleIndex + 1]
            val rotation1 = rotation0 + spiral.rotationPerIteration

            val rotationPerPoint = 2 * Math.PI / circle0.perturbations.size

            circle0.perturbations
                .zip(circle1.perturbations)
                .forEachIndexed { index: Int, (offset0: Double, offset1: Double) ->
                    val point0 = DPoint(
                        x0 + (offset0 + 1.0) * spiral.radius * cos(index * rotationPerPoint + rotation0),
                        y0 + (offset0 + 1.0) * spiral.radius * sin(index * rotationPerPoint + rotation0)
                    )

                    val point1 = DPoint(
                        x1 + (offset1 + 1.0) * spiral.radius * cos(index * rotationPerPoint + rotation1),
                        y1 + (offset1 + 1.0) * spiral.radius * sin(index * rotationPerPoint + rotation1)
                    )

                    line(point0, point1)
                }
        }

    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    private fun createSpiral(): Spiral {
        val circles = createCircleTargets()
        val radius = 200.0
        val rotationSpeed = 2 * Math.PI / circles.size
        return Spiral(
            radius,
            rotationSpeed,
            circles
        )
    }

    val circleCount = 5
    val iterations = 5
    private fun createCircleTargets(): List<PerturbedCircle> {
        val circles = mutableListOf<PerturbedCircle>()
        for (circleIndex in 0..circleCount) {
            var offsetList = listOf<Double>(
                random.nextDouble(-0.5, 0.5),
                random.nextDouble(-0.5, 0.5),
                random.nextDouble(-0.5, 0.5),
                random.nextDouble(-0.5, 0.5)
            )

            for (i in 0..iterations) {
                offsetList = offsetList
                    .windowedCircular(2)
                    .flatMap { (point0, point1) ->
                        val delta = point1 - point0
                        val newPoint = (delta / 2) * random.nextDouble(0.5) + point0
                        listOf(point0, newPoint, point1) }
            }

            circles.add(PerturbedCircle(offsetList))
        }

        return circles
    }


    class Spiral(
        val radius: Double,
        val rotationPerIteration: Double,
        val targetCircles: List<PerturbedCircle>
    )

    class PerturbedCircle(
        val perturbations: List<Double>
    )

}