package art.funk.daily.y2019.jul

import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color
import kotlin.math.cos
import kotlin.math.sin
import kotlin.random.Random

object Jul6 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jul6Sketch::class.java)
    }
}

class Jul6Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val random = Random(401)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {

        stroke(Color(10, 210, 200, 80))
        background(Color(255, 255, 255, 255))

        // Circles similar to the interpolation piece, but instead just have bezier
        // connections for each of the interpolated points.

        // In short, create a series of circles, with pertubations for each.
        // Move them along the x-axis with potentially a random-ness applied to the
        // y-axis if not some other function.  This combined with a rotation factor
        // will allow for a total piece


        // Another idea, maybe have a secondary circle set that is just for the bezier manipulations?
        val spiral = createSpiral()

        val xDelta = myWidth.toDouble() / (spiral.targetCircles.size - 1)
        val yDeltas = spiral.targetCircles.map { circle -> (spiral.radius * random.nextDouble() - spiral.radius / 2) / spiral.targetCircles.size }


        for (circleIndex in 0 until (spiral.targetCircles.size - 1)) {
            val strokeColor = Color(10, ((250.0 * circleIndex) / spiral.targetCircles.size).toInt(), 200, 80)
            stroke(strokeColor)
            val circle0 = spiral.targetCircles[circleIndex]
            val circle1 = spiral.targetCircles[circleIndex + 1]

            val x0 = circleIndex * xDelta
            val y0 = myHeight / 2 + yDeltas.subList(0, circleIndex + 1).sum()
            val rotation0 = circleIndex * spiral.rotationPerIteration

            val x1 = x0 + xDelta
            val y1 = y0 + yDeltas[circleIndex + 1]
            val rotation1 = rotation0 + spiral.rotationPerIteration

            val rotationPerPoint = 2 * Math.PI / circle0.perturbations.size

            circle0.perturbations
                .zip(circle1.perturbations)
                .forEachIndexed { index: Int, (offset0: Double, offset1: Double) ->

                    val initialPoint = DPoint(
                        x0 + (offset0 + 1.0) * spiral.radius * cos(index * rotationPerPoint + rotation0),
                        y0 + (offset0 + 1.0) * spiral.radius * sin(index * rotationPerPoint + rotation0)
                    )


                    val drawPath = true
                    if (drawPath) {
                        val pointsPerIteration = 100
                        for (pointIndex in 0 until pointsPerIteration) {
                            val percentage = pointIndex.toDouble() / pointsPerIteration
                            val nextPercentage = (pointIndex + 1).toDouble() / pointsPerIteration
                            val circleCenter = DPoint(
                                x0 + (x1 - x0) * percentage,
                                y0 + (y1 - y0) * percentage
                            )

                            val point0 = circleCenter + DPoint(
                                (offset0 * percentage + offset1 * (1 - percentage) + 1.0) * spiral.radius * cos(index * rotationPerPoint + rotation0 * percentage + rotation1 * (1 - percentage)),
                                (offset0 * percentage + offset1 * (1 - percentage) + 1.0) * spiral.radius * sin(index * rotationPerPoint + rotation0 * percentage + rotation1 * (1 - percentage))
                            )

                            val point1 = circleCenter + DPoint(
                                (offset0 * nextPercentage + offset1 * (1 - nextPercentage) + 1.0) * spiral.radius * cos(index * rotationPerPoint + rotation0 * nextPercentage + rotation1 * (1 - nextPercentage)),
                                (offset0 * nextPercentage + offset1 * (1 - nextPercentage) + 1.0) * spiral.radius * sin(index * rotationPerPoint + rotation0 * nextPercentage + rotation1 * (1 - nextPercentage))
                            )


                            line(point0, point1)
                        }
                    }

                    val drawDirectLine = false

                    if (drawDirectLine) {
                        val point1 = DPoint(
                            x1 + (offset1 + 1.0) * spiral.radius * cos(index * rotationPerPoint + rotation1),
                            y1 + (offset1 + 1.0) * spiral.radius * sin(index * rotationPerPoint + rotation1)
                        )

                        line(initialPoint, point1)
                    }
                }
        }

    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    private fun createSpiral(): Spiral {
        val circles = createCircleTargets()
        val radius = 180.0
        val rotationSpeed = 2 * Math.PI / circles.size
        return Spiral(
            radius,
            rotationSpeed,
            circles
        )
    }

    val circleCount = 5
    val iterations = 2
    private fun createCircleTargets(): List<PerturbedCircle> {
        val circles = mutableListOf<PerturbedCircle>()
        for (circleIndex in 0..circleCount) {
            var offsetList = listOf<Double>(
                random.nextDouble(-0.5, 0.5),
                random.nextDouble(-0.5, 0.5),
                random.nextDouble(-0.5, 0.5),
                random.nextDouble(-0.5, 0.5)
            )

            for (i in 0..iterations) {
                offsetList = offsetList
                    .windowedCircular(2)
                    .flatMap { (point0, point1) ->
                        val delta = point1 - point0
                        val newPoint = (delta / 2) * random.nextDouble(0.5) + point0
                        listOf(point0, newPoint, point1) }
            }

            circles.add(PerturbedCircle(offsetList))
        }

        return circles
    }


    class Spiral(
        val radius: Double,
        val rotationPerIteration: Double,
        val targetCircles: List<PerturbedCircle>
    )

    class PerturbedCircle(
        val perturbations: List<Double>
    )

}