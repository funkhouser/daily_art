package art.funk.daily.y2019.jul

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color

object Jul4 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jul4Sketch::class.java)
    }
}

class Jul4Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val a0 = 6.0
    val h0 = 160.0
    val b0 = 6.0
    val k0 = 160.0

    val a1 = -1.20 * 2
    val h1 = 660.0
    val b1 = -1.20 * 2
    val k1 = 660.0

    var i = 0
    val totalPoints = 900

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    val function: IterativeColoredLineFunction = { index ->

        val point0 = DPoint(
            a0 * Math.sqrt(index.toDouble()) * Math.sin(index * 5 / 100.0) + h0,
            b0 * Math.sqrt(index.toDouble()) * Math.cos(index * 7 / 100.0) + k0
        )

        val point1 = DPoint(
            a1 * Math.sqrt(index.toDouble()) * Math.cos(index / 100.0) + h1,
            b1 * Math.sqrt(index.toDouble()) * Math.sin(index / 100.0) + k1)

        ColoredLine(
            point0,
            point1,
            Color(100, 100, 100, 15)
        )
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeWidth(3.0)
    }

    override fun draw() {
        val result = function(i)
        if (Math.abs(result.point0.y - result.point1.y) < 100) {
            println("huh")
        }
        line(result)
        i += 1

        if (i >= totalPoints) {
            super.draw()
            save("outputs/" + javaClass.name)

        }
    }
}