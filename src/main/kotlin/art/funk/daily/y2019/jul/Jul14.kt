package art.funk.daily.y2019.jul

import art.funk.daily.core.*
import gifAnimation.GifMaker
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.random.Random

object Jul14 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jul14Sketch::class.java)
    }
}

class Jul14Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val random = Random(3020)

    var i = 0
    val actualIterations = 200
    val iterations = 1000

    val gifExport = GifMaker(this, "outputs/" + javaClass.name + ".gif")


    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        stroke(Color(255, 255, 255, 0))
        gifExport.setDelay(1000 / 12)
        gifExport.setRepeat(0)
    }

    override fun draw() {

        val y0 = i.toDouble() * myHeight / iterations
        val x0 = 0.0

        val y1 = myHeight - y0
        val x1 = myWidth.toDouble()

        val rangeMin = 200.0
        val rangeMax = 520.0

        val xDelta = 100.0

        i += 1

        fill(getFillColor())

        bezier(
            DPoint(x0, y0),
            DPoint(random.nextDouble(rangeMin, rangeMin + xDelta), random.nextDouble(rangeMin, rangeMax)),
            DPoint(random.nextDouble(rangeMax - xDelta, rangeMax), random.nextDouble(rangeMin, rangeMax)),
            DPoint(x1, y1)
        )

        fill(getFillColor2())

        bezier(
            DPoint(y0, x0),
            DPoint(random.nextDouble(rangeMin, rangeMin + xDelta), random.nextDouble(rangeMin, rangeMax)),
            DPoint(random.nextDouble(rangeMax - xDelta, rangeMax), random.nextDouble(rangeMin, rangeMax)),
            DPoint(y1, x1)
        )

        if (i >= actualIterations) {
            super.draw()
            save("outputs/" + javaClass.name)

            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()

            gifExport.finish()
        }

        if (i % 10 == 0) {
            gifExport.addFrame()
        }
    }

    private fun getFillColor(): Color {
        return Color(
            random.nextInt(100, 120),
            random.nextInt(10, 40),
            random.nextInt(180, 200),
            5
        )
    }

    private fun getFillColor2(): Color {
        return Color(
            random.nextInt(200, 220),
            random.nextInt(10, 40),
            random.nextInt(80, 100),
            5
        )
    }

}