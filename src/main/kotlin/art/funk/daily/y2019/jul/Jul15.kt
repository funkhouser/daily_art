package art.funk.daily.y2019.jul

import art.funk.daily.core.*
import art.funk.daily.core.applet.rotate
import gifAnimation.GifMaker
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.random.Random

object Jul15 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jul15Sketch::class.java)
    }
}

class Jul15Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val random = Random(3020)

    var i = 0
    val actualIterations = 200
    val iterations = 1000

    val gifExport = GifMaker(this, "outputs/" + javaClass.name + ".gif")


    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(230, 180, 220, 200))
        stroke(Color(255, 255, 255, 0))
        gifExport.setDelay(1000 / 12)
        gifExport.setRepeat(0)
    }

    override fun draw() {

        val y0 = myHeight.toDouble()
        val x0 = 0.0

        val y1 = myHeight.toDouble()
        val x1 = myWidth.toDouble()

        val rangeMin = 00.0
        val rangeMax = 620.0

        val xDelta = 100.0

        i += 1

        fill(getFillColor())

        rotate(20 * i * Math.PI / iterations, DPoint(360.0, 360.0))



        bezier(
            DPoint(x0, y0),
            DPoint(random.nextDouble(rangeMin, rangeMin + xDelta), 0.0),
            DPoint(random.nextDouble(rangeMax - xDelta, rangeMax), random.nextDouble(rangeMin, rangeMax)),
            DPoint(x1, y1)
        )

        fill(getFillColor2())

        bezier(
            DPoint(x0 + random.nextInt(200), y0 + random.nextInt(200)),
            DPoint(random.nextDouble(rangeMin, rangeMin + xDelta), 0.0),
            DPoint(random.nextDouble(rangeMax - xDelta, rangeMax), random.nextDouble(rangeMin, rangeMax)),
            DPoint(x1 - random.nextInt(200), y1 - random.nextInt(200))
        )

//        bezier(
//            DPoint(y0, x0),
//            DPoint(random.nextDouble(rangeMin, rangeMin + xDelta), random.nextDouble(rangeMin, rangeMax)),
//            DPoint(random.nextDouble(rangeMax - xDelta, rangeMax), random.nextDouble(rangeMin, rangeMax)),
//            DPoint(y1, x1)
//        )

        if (i >= actualIterations) {
            super.draw()
            save("outputs/" + javaClass.name)

            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()

            gifExport.finish()
        }

        if (i % 10 == 0) {
            gifExport.addFrame()
        }
    }

    private fun getFillColor(): Color {
        return Color(
            random.nextInt(100, 120),
            random.nextInt(10, 40),
            random.nextInt(180, 200),
            5
        )
    }

    private fun getFillColor2(): Color {
        val shade = random.nextInt(240, 250)
        return Color(
            shade,
            shade,
            shade,
            20
        )
    }

}