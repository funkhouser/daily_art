package art.funk.daily.y2019.jul

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color

object Jul9_2 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jul9_2Sketch::class.java)
    }
}

class Jul9_2Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val dt = 0.001
    val a = 0.95
    val b = 0.7
    val c = 0.6
    val d = 3.5
    val e = 0.25
    val f = 0.1

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {

        strokeWidth(4.0)
        background(Color(255, 255, 255, 255))

        var point = D3Point(0.1, 0.0, 0.0)
        var point0 = D3Point(0.8, 0.0, 0.0)

        for (i in 0..10000) {
            val t = i * dt

            val newPoint = aizawa(point)
            val newPoint0 = aizawa(point0)

            line(LinearGradientLine(
                scaledPoint(point),
                scaledPoint(point0),
                Color(0, 0, 0, 55),
                Color(255, 0, 0, 55)
            ))

            point = newPoint
            point0 = newPoint0
        }
    }

    private fun aizawa(point: D3Point): D3Point {
        val x = point.x
        val y = point.y
        val z = point.z

        val dx = (z - b) * x - d * y
        val dy = d * x + (z - b) * y
        val dz = c + a * z - z * z * z / 3 - x * x + f * z * x * x * x

        return D3Point(
            x + dt * dx,
            y + dt * dy,
            z + dt * dz
        )
    }

    private fun scaledPoint(x: Double, y: Double, z: Double): DPoint {
        return DPoint(360.0 + x * myWidth / 6, 360.0 + y * myHeight / 6 + z * 100 - 50.0)
    }

    private fun scaledPoint(point: D3Point): DPoint {
        return scaledPoint(point.x, point.y, point.z)
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

}