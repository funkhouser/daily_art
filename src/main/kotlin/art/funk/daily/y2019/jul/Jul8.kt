package art.funk.daily.y2019.jul

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.random.Random

object Jul8 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jul8Sketch::class.java)
    }
}

class Jul8Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val gridSize = 70.0
    val rows = kotlin.math.floor(myHeight / gridSize).toInt() + 1
    val columns = kotlin.math.floor(myWidth / gridSize).toInt() + 1

    val linesPer = 5
    val initialOffset = (gridSize / linesPer) / 2
    val lineOffset = (gridSize / linesPer)
    val strokeWidth = 10.0
    val random = Random(498043)

    val cellsDrawn = mutableListOf<Point>()

    var currentColor = Color(0, 0, 0, 255)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        noFill()
        strokeWidth(strokeWidth)
        strokeCap(StrokeCap.SQUARE)
        for (row in 0..rows) {
            for (column in 0..columns) {
                drawCell(column, row)
            }
        }

    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    // Maybe have this return a pair or multiple colors?
    private fun pickColor(column: Int, row: Int, gridDraw: GridDraw, lineIteration: Int): Color {
        val p = random.nextInt(100)
        return Color(p, p, p, 255)
    }

    private fun drawCell(column: Int, row: Int) {
        val modulus = 4
        val gridDraw = when {
            (column - row) % (modulus * 2) == 0 -> GridDraw.HORIZONTAL
            (column - row) % modulus == 0 -> GridDraw.VERTICAL
            (column - row + modulus * 100) % modulus == 1 -> GridDraw.LEFT_BOTTOM
            (column - row + modulus * 100) % modulus == (modulus - 1) -> GridDraw.RIGHT_TOP

            (column + row) % (modulus * 2) == 0 -> GridDraw.HORIZONTAL
            (column + row) % modulus == 0 -> GridDraw.VERTICAL
            (column + row + modulus * 100) % modulus == 1 -> GridDraw.LEFT_BOTTOM
            (column + row + modulus * 100) % modulus == (modulus - 1) -> GridDraw.RIGHT_TOP


            else -> GridDraw.NONE
        }
        drawCell(column, row, gridDraw)
    }

    private fun drawCell(column: Int, row: Int, gridDraw: GridDraw) {
        if (cellsDrawn.contains(Point(column, row))) {
            // Determines whether things get overdrawn or not
//            return
        }
        // Different effect, really cool in its own way
        cellsDrawn.add(Point(column, row))
        val topLeft = DPoint(column * gridSize, row * gridSize)
        for (i in 0 until linesPer) {
            val color = pickColor(column, row, gridDraw, i)
            stroke(color)
            when (gridDraw) {
                GridDraw.VERTICAL -> {
                    line(
                        topLeft + DPoint(i * lineOffset + initialOffset, 0.0),
                        topLeft + DPoint(i * lineOffset + initialOffset, gridSize)
                    )
                }
                GridDraw.HORIZONTAL -> {
                    line(
                        topLeft + DPoint(0.0, i * lineOffset + initialOffset),
                        topLeft + DPoint(gridSize, i * lineOffset + initialOffset)
                    )
                }
                GridDraw.LEFT_TOP -> {
                    arc(
                        topLeft,
                        i * lineOffset + initialOffset,
                        0.0,
                        Math.PI / 2
                    )
                }
                GridDraw.LEFT_BOTTOM -> {
                    arc(
                        topLeft + DPoint(0.0, gridSize),
                        i * lineOffset + initialOffset,
                        3 * Math.PI / 2,
                        2 * Math.PI
                    )
                }
                GridDraw.RIGHT_TOP -> {
                    arc(
                        topLeft + DPoint(gridSize, 0.0),
                        i * lineOffset + initialOffset,
                        Math.PI / 2,
                        Math.PI
                    )
                }
                GridDraw.RIGHT_BOTTOM -> {
                    arc(
                        topLeft + DPoint(gridSize, gridSize),
                        i * lineOffset + initialOffset,
                        Math.PI,
                        3 * Math.PI / 2
                    )
                }
                GridDraw.NONE -> {
                }
            }
        }

    }

    enum class GridDraw {
        VERTICAL,
        HORIZONTAL,
        LEFT_TOP,
        LEFT_BOTTOM,
        RIGHT_TOP,
        RIGHT_BOTTOM,
        NONE
    }
}