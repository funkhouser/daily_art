package art.funk.daily.y2019.jul

import art.funk.daily.core.*
import art.funk.daily.core.bitmap.BitMapCanvas
import art.funk.daily.y2019.jun.Jun4Sketch
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.math.min
import kotlin.random.Random

object Jul2 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jul2Sketch::class.java)
    }
}

class Jul2Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val pointCount = 100
    val bitMapCanvas = BitMapCanvas(myWidth, myHeight)
    val random = Random(600)

    val colorPoints = (arrayOfNulls<Any?>(pointCount).map { DPoint(
        random.nextDouble(myWidth.toDouble()),
        random.nextDouble(myHeight.toDouble())) to generateColor()
    } + listOf(
        DPoint(0.0, 0.0) to generateColor(),
        DPoint(myWidth.toDouble(), 0.0) to generateColor(),
        DPoint(0.0, myHeight.toDouble()) to generateColor(),
        DPoint(myWidth.toDouble(), myHeight.toDouble()) to generateColor()
    )).toMap()

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        // TODO Do a bunch of triangle gradients where each node of the triangle has an associated color
        // and the color for the triangle fill gets interpolated from the 3 nodes.

        val allVertices = createAllVertices(colorPoints.keys)
        val vertices = createConnectedGraph(allVertices)
        val triangles = consolidateToTriangles(vertices)


        bitMapCanvas.applyEveryPixel { x, y ->

            val dPoint = DPoint(x.toDouble(), y.toDouble())
            val triangles = triangles
                .filter { isPointInTriangle(dPoint, it) }

            if (triangles.size != 1) {
                println("huh")
            }

            getColor(dPoint, triangles[0])

        }

        triangles.forEach { drawTriangle(it) }

        applyBitMap(bitMapCanvas)
    }

    private fun drawTriangle(triangle: Triangle) {
        // The color should, I think, be based upon how far the point is along the normal of the opposite side from the given point.
        // In short, the proportion of color c0 (from point p0) should be 100% at point p0 and 0% along the side between p1 and p2
        // The place where the colors are equal is not the point that is equidistant to each point, but rather the centroid
        // which is found by the intersection of each of the points and the midpoint for the opposite side.

        // Brute force for the beginning, if the point is not in the given triangle, then do nothing. Else apply the computed color
        // A more normal way would be to find which triangle is actually viable for the given pixel

    }

    /**
     * Assumes that the point has already been guaranteed to be within the triangle
     */
    private fun getColor(point: DPoint, triangle: Triangle): Color {
        // Probably want to see how far along the normal vector of the corner this point is

        // Okay lets be lazy at first and just do a distance competition
        val point0Distance = (triangle.point0 - point).magnitude()
        val point1Distance = (triangle.point1 - point).magnitude()
        val point2Distance = (triangle.point2 - point).magnitude()

        val totalDistance = point0Distance + point1Distance + point2Distance

        val color0 = colorPoints[triangle.point0] ?: error("")
        val color1 = colorPoints[triangle.point1] ?: error("")
        val color2 = colorPoints[triangle.point2] ?: error("")
        val minDistance = min(point0Distance, min(point1Distance, point2Distance))

        return when {
            point0Distance == minDistance -> color0.interpolate(color1, (totalDistance - point1Distance) / totalDistance).interpolate(color2, (totalDistance - point2Distance) / totalDistance)
            point1Distance == minDistance -> color1.interpolate(color2, (totalDistance - point2Distance) / totalDistance).interpolate(color0, (totalDistance - point0Distance) / totalDistance)
            else -> color2.interpolate(color0, (totalDistance - point0Distance) / totalDistance).interpolate(color1, (totalDistance - point1Distance) / totalDistance)
        }
    }

    //    Melrose
//    HEX: #C7BDFD
//    RGB: rgb(199,189,253)
//    BIAS: 100%
//    Froly
//    HEX: #EF7072
//    RGB: rgb(239,112,114)
//    BIAS: 98%
//    Texas Rose
//    HEX: #FFB658
//    RGB: rgb(255,182,88)
//    BIAS: 82%
//    Texas
//    HEX: #F8EB99
//    RGB: rgb(248,235,153)
//    BIAS: 75%


    private fun generateColor(): Color {
        val colors = listOf(
            Color(199,189,253),
            Color(239,112,114),
            Color(255,182,88),
            Color(248,235,153)
        )

        return colors.random(random)
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    private fun createAllVertices(points: Collection<DPoint>): List<DVertex> {
        return points.flatMap { point0 ->
            points.mapNotNull { point1 ->
                if (point0 == point1 || point0.x > point1.x || (point0.x == point1.x && point0.y > point1.y)) {
                    null
                } else {
                    DVertex(point0, point1)
                }
            }
        }.sortedBy { it.distance() }
    }
    private fun createConnectedGraph(allVertices: List<DVertex>): List<DVertex> {
        val vertices = mutableListOf<DVertex>()

        // Construct the actually spanning tree beginning from the smallest
        // vertices such that no two vertices are overlapping
        allVertices.forEach { vertex ->
            if (!vertices.any { existingVertex -> linesIntersect(vertex, existingVertex) }) {
                vertices.add(vertex)
            }
        }

        // Vertices should now be the maximum viable set.
        return vertices
    }

    private fun getConnectionPoint(v0: DVertex, v1: DVertex): DPoint? {
        return when {
            v0.point0 == v1.point0 || v0.point1 == v1.point0 -> v1.point0
            v0.point0 == v1.point1 || v0.point1 == v1.point1 -> v1.point1
            else -> null
        }
    }

    private fun linesIntersect(v0: DVertex, v1: DVertex): Boolean {
        if (getConnectionPoint(v0, v1) != null) {
            return false
        }
        val crossingPointFactor0 = findCrossingPointFactor(v0, v1)
        val crossingPointFactor1 = findCrossingPointFactor(v1, v0)

        return crossingPointFactor0 > 0 && crossingPointFactor0 < 1 && crossingPointFactor1 > 0 && crossingPointFactor1 < 1
    }

    private fun findCrossingPointFactor(v0: DVertex, v1: DVertex): Double {
        val delta0 = DPoint(v0.point1.x - v0.point0.x, v0.point1.y - v0.point0.y)
        val delta1 = DPoint(v1.point1.x - v1.point0.x, v1.point1.y - v1.point0.y)
        val factor = ((v1.point0 - v0.point0) * delta1) / (delta0 * delta1)
        return factor
    }

    private fun consolidateToTriangles(vertices: List<DVertex>): List<Triangle> {
        val triangles: List<Triangle> = vertices.withIndex().flatMap { (index, vertex0) ->
            vertices.subList(index, vertices.size).mapNotNull { vertex1 ->
                val point = getConnectionPoint(vertex0, vertex1)
                when {
                    vertex0 == vertex1 -> {
                        null
                    }
                    point != null -> {
                        val triangleList = listOf(
                            vertex0.point0,
                            vertex0.point1,
                            vertex1.point0,
                            vertex1.point1).distinct()

                        val newVertexPoints = triangleList - point
                        val matchingVertex = findMatchingVertex(newVertexPoints[0], newVertexPoints[1], vertices)

                        if (matchingVertex != null) {
                            Triangle(triangleList[0], triangleList[1], triangleList[2])
                        } else {
                            null
                        }
                    }
                    else -> {
                        null
                    }
                }
            }
        }
        return triangles
    }

    private fun findMatchingVertex(point0: DPoint, point1: DPoint, vertices: List<DVertex>): DVertex? {
        return vertices.find { vertex ->
            (vertex.point0 == point0 || vertex.point0 == point1) && (vertex.point1 == point0 || vertex.point1 == point1)
        }
    }

    private fun isPointInTriangle(point: DPoint, triangle: Triangle): Boolean {
        return isSameSide(point, triangle.point2, triangle.point0, triangle.point1) &&
            isSameSide(point, triangle.point0, triangle.point1, triangle.point2) &&
            isSameSide(point, triangle.point1, triangle.point2, triangle.point0)
    }

    private fun isSameSide(test0: DPoint, test1: DPoint, point0: DPoint, point1: DPoint): Boolean {
        val cross0 = D3Point(point1 - point0).cross(D3Point(test0 - point0))
        val cross1 = D3Point(point1 - point0).cross(D3Point(test1 - point0))

        return cross0.z * cross1.z >= 0.0
    }
}