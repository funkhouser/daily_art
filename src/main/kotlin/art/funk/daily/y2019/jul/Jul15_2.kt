package art.funk.daily.y2019.jul

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.math.cos
import kotlin.math.sin
import kotlin.random.Random

object Jul15_2 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jul15_2Sketch::class.java)
    }
}

class Jul15_2Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val initialRadius = 130.0
    val radiusDelta = 110.0


    val circleCount = 15
    val iterations = 2

    val random = Random(715)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        val circles = createCircleTargets()

        stroke(Color(10, 10, 10, 50))

        val windowedCircles = circles.windowed(4, 1, false)
        noFill()
        background(Color(255, 255, 255, 255))

        for (i in 0 until(windowedCircles.size)) {
            val (c0, c1, c2, c3) = windowedCircles[i]

            val x0 = myWidth.toDouble() * i / (circleCount - 3)
            val y0 = verticalCenter(i)

            val center0 = DPoint(x0, y0)

            val x1 = x0 + myWidth.toDouble() / (circleCount - 3)
            val y1 = verticalCenter(i + 1)

            val center1 = DPoint(x1, y1)

            val midCenter0 = center0 * 0.3 + center1 * 0.7
            val midCenter1 = center0 * 0.7 + center1 * 0.3

            val pointCount = c0.perturbations.size
            val radius = initialRadius + (random.nextDouble() - 0.5) * radiusDelta * 2
            for (pointIndex in 0 until(pointCount)) {
                bezier(
                    applyOffset(center0, pointIndex, pointCount, c0.perturbations[pointIndex], radius),
                    applyOffset(midCenter0, pointIndex + 1, pointCount, c1.perturbations[pointIndex], radius),
                    applyOffset(midCenter1, pointIndex + 2, pointCount, c2.perturbations[pointIndex], radius),
                    applyOffset(center1, pointIndex + 3, pointCount, c3.perturbations[pointIndex], radius)
                )
            }
        }


    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    private fun applyOffset(center: DPoint, i: Int, pointCount: Int, offset: Double, radius: Double): DPoint {
        return center + DPoint(
            (cos(2 * Math.PI * i / pointCount) + offset) * radius,
            (sin(2 * Math.PI * i / pointCount) + offset) * radius
        )
    }

    private fun verticalCenter(i: Int): Double {
        return 20.0 * sin(5 * Math.PI * i / circleCount) + myHeight / 2.0
    }

    private fun createCircleTargets(): List<PerturbedCircle> {
        val circles = mutableListOf<PerturbedCircle>()
        for (circleIndex in 0..circleCount) {
            var offsetList = listOf<Double>(
                random.nextDouble(-0.5, 0.5),
                random.nextDouble(-0.5, 0.5),
                random.nextDouble(-0.5, 0.5),
                random.nextDouble(-0.5, 0.5)
            )

            for (i in 0..iterations) {
                offsetList = offsetList
                    .windowedCircular(2)
                    .flatMap { (point0, point1) ->
                        val delta = point1 - point0
                        val newPoint = (delta / 2) * random.nextDouble(0.5) + point0
                        listOf(point0, newPoint, point1) }
            }

            circles.add(PerturbedCircle(offsetList))
        }

        return circles
    }

    class Spiral(
        val radius: Double,
        val rotationPerIteration: Double,
        val targetCircles: List<PerturbedCircle>
    )

    class PerturbedCircle(
        val perturbations: List<Double>
    )
}