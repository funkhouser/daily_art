package art.funk.daily.y2019.jul

import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color
import kotlin.math.cos

object Jul11 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jul11Sketch::class.java)
    }
}

class Jul11Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720


    val phase = -25.115001
    val lowerA = 0.75 + 0.05 * cos(phase + 7.0)
    val upperA = 1.55 + 0.05 * cos(phase * 3.0)

    val dt = 0.001

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {

        var point0 = D3Point(1.15, -1.0, 0.0)
        var point1 = D3Point(1.12, -0.4, 0.0)
        background(Color(255, 255, 255, 255))

        val iterations = 1000
        for (i in 0..iterations) {
            val newPoint0 = hopfFibration(point0)
            val newPoint1 = hopfFibration(point1)

            val doublePoint = scaledPoint(point0)
            val doublePoint1 = scaledPoint(point1)

            stroke(Color((i.toDouble() / iterations * 255).toInt(), 155, 50, 100))

            line(
                doublePoint,
                doublePoint1
            )

//            line(
//                LinearGradientLine(
//                doublePoint,
//                doublePoint0,
//                Color(155, 155, 50, 100),
//                Color(155, 155, 50, 100)
//            ))

            point0 = newPoint0
            point1 = newPoint1
        }

    }

    private fun hopfFibration(point: D3Point): D3Point {
        // These could seriously use some better names
        val dotted = point.dot(point)
        val r2 = lowerA * lowerA + dotted
        val c = upperA / (r2 * r2)

        val deltaX = 2.0 * c * ((-upperA) * point.y + point.x * point.z)
        val deltaY = 2.0 * c * (upperA * point.x + point.y * point.z)
        val deltaZ = lowerA * lowerA - point.x * point.x - point.y * point.y + point.z * point.z

        return D3Point(
            point.x + deltaX * dt,
            point.y + deltaY * dt,
            point.z + deltaZ * dt
        )
    }

    private fun scaledPoint(x: Double, y: Double, z: Double): DPoint {
        return DPoint(360.0 + x * myWidth / 2.5 + z * 100, 360.0 + y * myHeight / 2.5 + z * 50)
    }

    private fun scaledPoint(point: D3Point): DPoint {
        return scaledPoint(point.x, point.y, point.z)
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}