package art.funk.daily.y2019.jul

import art.funk.daily.core.*
import art.funk.daily.core.bitmap.BitMapCanvas
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.math.cos
import kotlin.math.sin
import kotlin.random.Random

object Jul1 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jul1Sketch::class.java)
    }
}

class Jul1Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val random = Random(45)

    val halfHeight = 720 / 2
    val halfWidth = 720 / 2

    val center = DPoint(360.0, 360.0)

//    var center0 = DPoint(50.0, 100.0)
//    var center1 = DPoint(670.0, 620.0)

    val color0 = Color(0, 240, 230, 10)
    val color1 = Color(226, 213, 176, 50)

    val circles = 10
    val lines = 3000
    val radius = 150.0

    val bitMap = BitMapCanvas(myWidth, myHeight)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        bitMap.setBackground(Color(255, 255, 255, 255))

        for (circle in 0 until circles) {

            val center0 = center + DPoint(
                360.0 * cos(random.nextDouble(Math.PI * 2)),
                360.0 * sin(random.nextDouble(Math.PI * 2)))


            // Either opposite or random
            val center1 = center + DPoint(
                360.0 * cos(random.nextDouble(Math.PI * 2)),
                360.0 * sin(random.nextDouble(Math.PI * 2)))


            for (i in 0 until lines) {
                val x0 = center0.x + random.nextDouble(radius) * cos(random.nextDouble(Math.PI * 2))
                val y0 = center0.y + random.nextDouble(radius) * sin(random.nextDouble(Math.PI * 2))

                val x1 = center1.x + random.nextDouble(radius) * cos(random.nextDouble(Math.PI * 2))
                val y1 = center1.y + random.nextDouble(radius) * sin(random.nextDouble(Math.PI * 2))

//                bitMap.line(
//                    DPoint(x0, y0),
//                    DPoint(x1, y1),
//                    color0
//                )

                bitMap.line(LinearGradientLine(
                    DPoint(x0, y0),
                    DPoint(x1, y1),
                    color0,
                    color1
                ))
            }
        }



        applyBitMap(bitMap)
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}