package art.funk.daily.y2019.jul

import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color
import kotlin.math.sqrt

object Jul7 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jul7Sketch::class.java)
    }
}

class Jul7Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val minStroke = 12.0
    val maxStroke = 40.0

    val count = 20

    val color0 = Color(145, 120, 180, 255)
    val color1 = Color(245, 120, 180, 255)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        fill(Color(255, 255, 255, 15))

        for (i in 0 until count) {
            val width = minStroke + (maxStroke - minStroke) * (i.toDouble() / count)
            strokeWidth(width)
            gradientArc(
                DPoint(720.0, 720.0),
                i * sqrt(720.0 * 720.0 * 2) / count,
                Math.PI,
                3 * Math.PI / 2,
                color0,
                color1
            )
        }
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}