package art.funk.daily.y2019.jul

import art.funk.daily.core.*
import art.funk.daily.core.sort.SortCreator
import art.funk.daily.core.sort.SortType
import processing.core.PApplet
import java.awt.Color
import kotlin.random.Random

object Jul16_2 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jul16_2Sketch::class.java)
    }
}

class Jul16_2Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val lines = 5
    val sortType = SortType.QUICK

    val lineWidth = myWidth.toDouble() / (lines * 5.5)
    val lineSpacing = (myWidth - lineWidth * lines) / (lines * 2)

    val random = Random(50403)

    val goMini = true
    val perLine = 20
    val miniLineWidth = lineWidth / perLine
    val totalPerLine = myWidth.toDouble() / lines
    val miniLineSpacing = (totalPerLine - (perLine * miniLineWidth)) / (perLine * 2)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {

        background(Color(255, 255, 255, 255))
        strokeWidth(lineWidth)

        val sortCreator = SortCreator(lines, random)
        val sortResult = sortCreator.runSort(sortType)

        val xStep = myWidth.toDouble() / lines
        val yStep = myHeight.toDouble() / (sortResult.size - 1)


        // Idea: If multiple "lines/strokes" represent each sortIndex then we could
        // naturally generate a weaving pattern. Otherwise this weaving pattern might
        // require more specific logic when a swap occurs
        sortResult.windowed(2, 1)
            .withIndex()
            .forEach { (index: Int, rowsPair) ->
                val current: List<Int> = rowsPair[0]
                val next: List<Int> = rowsPair[1]

                current.withIndex()
                    .forEach { (currentIndex, value) ->
                        val nextIndex = next.indexOf(value)

                        if (goMini) {
                            strokeWidth(miniLineWidth)
                            for (i in 0 until(perLine)) {
                                val point0 = DPoint(
                                    currentIndex * xStep + i * miniLineWidth + (2 * i + 1) * miniLineSpacing,
                                    index * yStep)
                                val point1 = DPoint(
                                    nextIndex * xStep + i * miniLineWidth + (2 * i + 1) * miniLineSpacing,
                                    (index + 1) * yStep
                                )
                                line(
                                    point0,
                                    point1)
                                line(
                                    point0.mirrored(),
                                    point1.mirrored())
                            }
                        } else {
                            val point0 = DPoint(
                                currentIndex * xStep + lineSpacing,
                                index * yStep)
                            val point1 = DPoint(
                                nextIndex * xStep + lineSpacing,
                                (index + 1) * yStep)
                            line(
                                point0,
                                point1
                            )
                            line(
                                point0.mirrored(),
                                point1.mirrored()
                            )
                        }

                    }
            }

    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}