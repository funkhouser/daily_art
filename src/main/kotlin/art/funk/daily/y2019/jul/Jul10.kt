package art.funk.daily.y2019.jul

import art.funk.daily.core.*
import art.funk.daily.core.bitmap.BitMapCanvas
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color

object Jul10 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jul10Sketch::class.java)
    }
}

class Jul10Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val dt = 0.001
    val a = 0.95
    val b = 0.7
    val c = 0.6
    val d = 3.5
    val e = 0.25
    val f = 0.1

    val bitMap = BitMapCanvas(myWidth, myHeight)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {

        strokeWidth(4.0)
        stroke(Color(255, 255, 255, 0))
        bitMap.setBackground(Color(255, 255, 255, 255))

        var point = D3Point(1.0, 0.0, 0.0)
        var point0 = D3Point(-1.0, 0.0, 0.0)
        var point1 = D3Point(0.0, 0.5, 0.0)
        var point2 = D3Point(0.0, -0.5, 0.0)

        for (i in 0..4000) {
            val t = i * dt

            val newPoint = aizawa(point)
            val newPoint0 = aizawa(point0)
            val newPoint1 = aizawa(point1)
            val newPoint2 = aizawa(point2)

            bitMap.line(LinearGradientLine(
                scaledPoint(point),
                scaledPoint(point0),
                Color(50, 40, 90, 55),
                Color(50, 40, 190, 55)
            ))

            bitMap.line(LinearGradientLine(
                scaledPoint(point0),
                scaledPoint(point1),
                Color(50, 40, 90, 5),
                Color(50, 40, 190, 5)
            ))

            bitMap.line(LinearGradientLine(
                scaledPoint(point1),
                scaledPoint(point2),
                Color(50, 40, 90, 5),
                Color(50, 40, 190, 55)
            ))

            bitMap.line(LinearGradientLine(
                scaledPoint(point2),
                scaledPoint(point),
                Color(50, 40, 90, 5),
                Color(50, 40, 190, 5)
            ))

            point = newPoint
            point0 = newPoint0
            point1 = newPoint1
            point2 = newPoint2
        }


        applyBitMap(bitMap)
    }

    private fun aizawa(point: D3Point): D3Point {
        val x = point.x
        val y = point.y
        val z = point.z

        val dx = (z - b) * x - d * y
        val dy = d * x + (z - b) * y
        val dz = c + a * z - z * z * z / 3 - x * x + f * z * x * x * x

        return D3Point(
            x + dt * dx,
            y + dt * dy,
            z + dt * dz
        )
    }

    private fun scaledPoint(x: Double, y: Double, z: Double): DPoint {
        return DPoint(360.0 + x * myWidth / 6, 360.0 + y * myHeight / 6 + z * 100 - 50.0)
    }

    private fun scaledPoint(point: D3Point): DPoint {
        return scaledPoint(point.x, point.y, point.z)
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

}