package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import art.funk.daily.core.diamond.DiamondCanvas
import art.funk.daily.core.math.linearRange
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.random.Random

object Jun11_3 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun11_3Sketch::class.java)
    }
}

class Jun11_3Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val stepSize = 7.0
    val random = Random(10)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        strokeWidth(0.0)
        background(Color(255, 255, 255, 255))
        stroke(Color(20, 20, 20, 150))
        strokeWidth(2.0)
    }

    override fun draw() {
        var start = DPoint(0.0, 0.0)
        var finish = DPoint(100.0, 100.0)
        var color = Color(255, 255, 255, 255)

        val iterations = (myHeight / stepSize).toInt()
        for (i in 0..iterations) {
            color = Color(
                Math.min(255, Math.max(0, color.red + random.nextInt(-5, 5))),
                Math.min(255, Math.max(0, color.green + random.nextInt(-5, 5))),
                Math.min(255, Math.max(0, color.blue + random.nextInt(-5, 5))),
                255)
            fill(color)
            val point0 = DPoint(i * stepSize, i * stepSize)
            val point1 = DPoint((i + 1) * stepSize, (i + 1) * stepSize)

            val control0 = DPoint(random.nextDouble(720.0), random.nextDouble(720.0))
            val control1 = DPoint(random.nextDouble(720.0), random.nextDouble(720.0))

            bezier(
                point0,
                control0,
                control1,
                point1
            )
        }

        super.draw()
        save("outputs/" + javaClass.name)
    }


}