package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color

object Jun23 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun23Sketch::class.java)
    }
}

class Jun23Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val minRadius = 20.0
    val strokeWidth = 15.0
    val spacing = 20.0
    val count = 5
    val maxRadius = minRadius + spacing * (count + 1) + strokeWidth

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeCap(SQUARE)
        strokeWidth(strokeWidth)
        fill(Color(255, 255, 255, 0))

        val color0 = Color(60, 50, 130, 225)
        val color1 = Color(160, 50, 130, 225)

        val x0 = 440.0
        val y0 = 160.0

        val x1 = x0 - maxRadius
        val y1 = 540.0

        val delta = 0.2

        for (i in 0..count) {
            val radius = minRadius + spacing * i + strokeWidth / 2.0

            stroke(color0)
            arc(
                DPoint(x0, y0),
                radius,
                -Math.PI,
                Math.PI / 2)

            line(
                DPoint(x0, y0 + radius),
                DPoint(0.0, y0 + radius))
            line(
                DPoint(x0 - delta, y0 + radius),
                DPoint(x0 + delta, y0 + radius)
            )


            line(
                LinearGradientLine(
                    DPoint(x0 - radius, y0),
                    DPoint(x0 - radius, y1),
                    color0,
                    color1))

            line(
                LinearGradientLine(
                    DPoint(x0 - radius, y0),
                    DPoint(x0 - radius, y1),
                    color0,
                    color1))

//            line(
//                DPoint(x0 - radius, y0),
//                DPoint(x0 - radius, y1))

            stroke(color1)

            arc(
                DPoint(x1, y1),
                radius,
                0.0,
                3 * Math.PI / 2
            )

            line(
                DPoint(x1 + radius, y1 - delta),
                DPoint(x1 + radius, y1 + 5 * delta))

            line(
                DPoint(x1, y1 - radius),
                DPoint(720.0, y1 - radius)
            )

            line(
                DPoint(x1 - delta, y1 - radius),
                DPoint(x1 + delta, y1 - radius)
            )

        }
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}