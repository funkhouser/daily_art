package art.funk.daily.y2019.jun

import art.funk.daily.core.DPoint
import art.funk.daily.core.applet.rotate
import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color

object Jun29 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun29Sketch::class.java)
    }
}

class Jun29Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val strokeWidth = 10.0
    val center = DPoint(360.0, 360.0)

    val circleOffset = 50.0
    val lineOffset = 11.0
    val dropCount = 1
    val lines = 65
    val minRadius = strokeWidth

    val color0 = Color(255, 131, 147, 255)
    val color1 = Color(255, 210, 110, 255)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        noFill()
        strokeWidth(strokeWidth)
        stroke(Color(255, 210, 245, 255))

        for (circleIndex in 0 until dropCount) {
            val radians = Math.PI + Math.PI  * circleIndex / dropCount
            val x = Math.cos(radians) * circleOffset + center.x
            val y = Math.sin(radians) * circleOffset + center.y
            for (i in 0 until lines) {
                stroke(color0.interpolate(color1, i.toDouble() / lines))
                arc(
                    DPoint(x, y),
                    minRadius + i * lineOffset,
                    -Math.PI,
                    0.0
                )

                line(
                    DPoint(x - (minRadius + i * lineOffset), y),
                    DPoint(x - (minRadius + i * lineOffset) + 720.0, y + 720.0)
                )

                line(
                    DPoint(x + (minRadius + i * lineOffset), y),
                    DPoint(x + (minRadius + i * lineOffset) + 720.0, y + 720.0)
                )
            }
        }

    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}