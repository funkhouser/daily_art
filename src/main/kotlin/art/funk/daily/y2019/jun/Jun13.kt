package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import gifAnimation.GifMaker
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.random.Random

object Jun13 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun13Sketch::class.java)
    }
}

class Jun13Sketch : PApplet() {

    val myHeight = 400
    val myWidth = 400

    val controlSize = myHeight.toDouble()
    val middle = myHeight / 2.0

    var i = 0
    val totalIterations = 300
    val iterationsPerFrame = 12

    val radius = 200.0
    val random = Random(1233)

    var fillColor = Color(255, 255, 255, 55)

    val gifExport = GifMaker(this, "outputs/" + javaClass.name + ".gif")


    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeWidth(0.5)
        stroke(Color(0, 0, 0, 155))
        gifExport.setDelay(1000 / 12)
        gifExport.setRepeat(0)
    }

    override fun draw() {
        if (i % iterationsPerFrame == 0) {
            gifExport.addFrame()
        }

        fillColor = Color(
            Math.min(255, Math.max(0, fillColor.red + random.nextInt(-5, 5))),
            Math.min(255, Math.max(0, fillColor.green + random.nextInt(-5, 5))),
            Math.min(255, Math.max(0, fillColor.blue + random.nextInt(-5, 5))),
            25)
        fill(fillColor)

        val point0 = DPoint(
            middle + radius * Math.cos(Math.PI * 2 / totalIterations * i),
            middle + radius * Math.sin(Math.PI * 2 / totalIterations * i)
        )

        val point1 = DPoint(
            middle + radius * Math.cos(Math.PI + Math.PI * 2 / totalIterations * i),
            middle + radius * Math.sin(Math.PI + Math.PI * 2 / totalIterations * i)
        )

        val control0 = DPoint(
            random.nextDouble(controlSize),
            random.nextDouble(controlSize)
        )

        val control1 = DPoint(
            random.nextDouble(controlSize),
            random.nextDouble(controlSize)
        )


        bezier(
            point0,
            control0,
            control1,
            point1
        )

        i += 1


        if (i >= totalIterations) {
            super.draw()
            save("outputs/" + javaClass.name)
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()

            gifExport.finish()

        }
    }
}