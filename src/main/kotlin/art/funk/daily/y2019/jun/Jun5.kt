package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color

object Jun5 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun5Sketch::class.java)
    }
}

class Jun5Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    var i = 0
    val totalLines = 1200

    val a0 = 10.0
    val h0 = 360.0
    val b0 = 10.0
    val k0 = 200.0

    val a1 = -12.00 / 10
    val h1 = 360.0
    val b1 = -12.00 / 10
    val k1 = 700.0

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeWidth(0.6)
    }

    override fun draw() {
        indexedParametricParabolic(i) { index ->
            val point0 = DPoint(
                a0 * Math.sqrt(index.toDouble()) * Math.sin(index / 100.0) + h0,
                b0 * Math.sqrt(index.toDouble()) * Math.cos(index / 100.0) + k0)

            val point1 = DPoint(
                a1 * Math.sqrt(index.toDouble()) * Math.cos(index / 100.0) + h1,
                b1 * Math.sqrt(index.toDouble()) * Math.sin(index / 100.0) + k1)

            LinearGradientLine(
                point0,
                point1,
                Color(130, 160, 120, 155),
                Color(100, 160, 120, 55)
            )
        }


        i += 1
        if (i > totalLines) {
            super.draw()
            save("outputs/" + javaClass.name)
        }
    }

    private fun indexedParametricParabolic(
        index: Int,
        functions: IterativeGradientLineFunction
    ) {
        val result = functions(index)
        line(result)
    }
}