package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import gifAnimation.GifMaker
import processing.core.PApplet
import java.awt.Color

object Jun3_3 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun3_3Sketch::class.java)
    }
}

class Jun3_3Sketch : PApplet() {
    val myHeight = 720 / 2
    val myWidth = 720 / 2
    var i = 0
    val totalPoints = 1000
    val gifExport = GifMaker(this, "outputs/" + javaClass.name + ".gif")


    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeWidth(0.6)
        gifExport.setDelay(1000 / 12)
        gifExport.setRepeat(0)
    }

    // Two different hyperbolas
    val a0 = 10.0 / 2.0
    val h0 = 360.0 / 2.0
    val b0 = 10.0 / 2.0
    val k0 = 100.0 / 2.0

    val a1 = -12.00 / 2.0
    val h1 = 360.0 / 2.0
    val b1 = -12.00 / 2.0
    val k1 = 600.0 / 2.0

    override fun draw() {
        indexedParametricParabolic(i) { index ->
            val point0 = DPoint(
                a0 * Math.sqrt(index.toDouble()) * Math.sin(index / 100.0) + h0,
                b0 * Math.sqrt(index.toDouble()) * Math.cos(index / 100.0) + k0)

            val point1 = DPoint(
                a1 * Math.sqrt(index.toDouble()) * Math.cos(index / 100.0) + h1,
                b1 * Math.sqrt(index.toDouble()) * Math.sin(index / 100.0) + k1)

            ColoredLine(
                point0,
                point1,
                Color(160, 100, 100, 55)
            )
        }

        i += 1
        if (i > totalPoints) {
            super.draw()
            save("outputs/" + javaClass.name)

            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()

            gifExport.finish()
        }

        if (i % 10 == 0) {
            gifExport.addFrame()
        }
    }

    private fun indexedParametricParabolic(
        index: Int,
        functions: IterativeColoredLineFunction
    ) {
        val result = functions(index)
        line(result)
    }
}
