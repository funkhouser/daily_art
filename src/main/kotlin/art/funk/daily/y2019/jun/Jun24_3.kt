package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import art.funk.daily.core.applet.rotate
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color

object Jun24_3 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun24_3Sketch::class.java)
    }
}

class Jun24_3Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val strokes = 10
    val strokeWidth = 20.0
    val offset = 35.0

    val minRadius = 15.0

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeWidth(strokeWidth)
        stroke(Color(0, 0, 0, 250))
        strokeCap(SQUARE)
        fill(Color(255, 255, 255, 0))

        rotate(Math.PI / 4)

        val color0 = Color(248,235,196, 255)
        val color1 = Color(115,181,169, 255)

        val color2 = Color(230, 240, 255, 255)



        var x = -160.0
        while (x < myWidth * 2) {

            val center0 = DPoint(x, 0.0)

            for (i in 0 until strokes) {
                val radius = minRadius + i * offset

                stroke(color0)
                arc(
                    center0,
                    radius,
                    0.0,
                    Math.PI
                )
            }


            x += offset * (strokes - 1) + minRadius * 2
            val center1 = DPoint(x, 0.0)

            for (i in 0 until strokes) {
                val radius = minRadius + i * offset

                stroke(color1)
                arc(
                    center1,
                    radius,
                    Math.PI,
                    Math.PI * 2
                )
            }

            x += offset * (strokes - 1) + minRadius * 2
        }


    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}