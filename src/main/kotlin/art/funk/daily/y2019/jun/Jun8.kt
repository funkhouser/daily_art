package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.random.Random

object Jun8 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun8Sketch::class.java)
    }
}

class Jun8Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val random = Random(5)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)

    }

    val stepSize = 8.0

    override fun setup() {
        background(Color(255, 255, 255, 255))

        var lastY = 60.0
        for (x in 0..myWidth) {
            val y = random.nextDouble(stepSize) - (stepSize / 2) + lastY
            lastY = y
            line(
                DPoint(x.toDouble(), 720.0),
                DPoint(x.toDouble(), y),
                Color(10, 20, 100, 55)
            )
        }

        lastY = 260.0
        for (x in 0..myWidth) {
            val y = random.nextDouble(stepSize) - (stepSize / 2) + lastY
            lastY = y
            line(
                DPoint(x.toDouble(), 720.0),
                DPoint(x.toDouble(), y),
                Color(10, 20, 100, 155)
            )
        }

        lastY = 460.0
        for (x in 0..myWidth) {
            val y = random.nextDouble(stepSize) - (stepSize / 2) + lastY
            lastY = y
            line(
                DPoint(x.toDouble(), 720.0),
                DPoint(x.toDouble(), y),
                Color(10, 20, 100, 155)
            )
        }

        lastY = 660.0
        for (x in 0..myWidth) {
            val y = random.nextDouble(stepSize) - (stepSize / 2) + lastY
            lastY = y
            line(
                DPoint(x.toDouble(), 720.0),
                DPoint(x.toDouble(), y),
                Color(10, 20, 100, 155)
            )
        }
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

}