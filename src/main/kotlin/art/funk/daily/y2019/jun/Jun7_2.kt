package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import gifAnimation.GifMaker
import processing.core.PApplet
import java.awt.Color

object Jun7_2 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun7_2Sketch::class.java)
    }
}

class Jun7_2Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    var i = 0
    val totalPoints = 1800 * 2
    val gifExport = GifMaker(this, "outputs/" + javaClass.name + ".gif")


    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    val r0 = 40.0
    val bigR0 = 200.0
//    val a0 = r0 * 1
    var a0 = r0

    val r1 = 40.0
    val bigR1 = 200.0
    val a1 = r1 * 2

    val x0 = 360.0
    val y0 = 360.0

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeWidth(0.5)
    }

    override fun draw() {

        for (i0 in 0 until 10) {
            val index = i + i0
            val t = Math.pow(index / 3.0, 0.7) // / 20.0
            a0 = r0 + t
            val point0 = DPoint(
                (bigR0 - r0) * Math.cos(r0 / bigR0 * t) + a0 * Math.cos((1 - r0 / bigR0) * t) + x0,
                (bigR0 - r0) * Math.sin(r0 / bigR0 * t) - a0 * Math.sin((1 - r0 / bigR0) * t) + y0
            )


            val point1 = DPoint(x0,y0)

            val point2 = point1 - (point0 - point1)

            line(LinearGradientLine(
                point0,
                point1,
                Color(100, 160, 160, 55),
                Color(60, 100, 60, 35)
            ))

            line(LinearGradientLine(
                point2,
                point1,
                Color(100, 160, 160, 55),
                Color(60, 100, 60, 35)
            ))

            // Double with black
            line(ColoredLine(
                DPoint(point0.x, point0.y),
                DPoint(point1.x, point1.y),
                Color(0, 0, 0, 40)
            ))

            line(ColoredLine(
                DPoint(point2.x, point2.y),
                DPoint(point1.x, point1.y),
                Color(0, 0, 0, 40)
            ))


        }
        i += 10

        if (i > totalPoints) {
            super.draw()
            save("outputs/" + javaClass.name)

            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()

            gifExport.finish()
        }


//        if (i % 10 == 0) {
//            gifExport.addFrame()
//        }
    }
}