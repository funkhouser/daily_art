package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.random.Random

object Jun17 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun17Sketch::class.java)
    }
}

class Jun17Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val maxRadius = 320.0

    val color0 = Color(0x4c, 0xf1, 0xe5, 0xff)
    val color1 = Color(0x85, 0xfb, 0xb6, 0xff)

    val random = Random(35)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        linearGradientCircle(
            this,
            DPoint(360.0, 360.0),
            maxRadius) { radius: Double ->

            color0.interpolate(color1, radius / maxRadius)
        }

        stroke(Color(255, 255, 255, 255))
        fill(Color(255, 255, 255, 255))
        for (i in 0..10) {
            val size = random.nextDouble(30.0)
            val x = random.nextDouble(720.0)

            strokeWidth(size)
            line(
                DPoint(x, 0.0),
                DPoint(0.0, x))

            line(
                DPoint(x, 0.0),
                DPoint(720.0 + x, 720.0))

            line(DPoint(0.0, x),
                DPoint(720.0, x + 720.0))
        }
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}