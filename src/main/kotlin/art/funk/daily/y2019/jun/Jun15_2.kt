package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.random.Random

object Jun15_2 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun15_2Sketch::class.java)
    }
}

class Jun15_2Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val random = Random(10)


    val color0 = Color(249, 208, 136, 255)
    val color1 = Color(249, 163, 136, 255)
    
    val curveCount = 20
    val perCurveAmount = 40.0
    val stepOffset = 5.0

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        gradientRect(
            this,
            DPoint(0.0, 0.0),
            DPoint(720.0, 720.0)) { point ->

            color0.interpolate(color1, point.y / 720.0)
        }

        strokeWidth(1.0)
        stroke(Color(10, 10, 10, 220))
        fill(Color(255, 255, 255, 0))
        
        for (i in 0..curveCount) {
            bezier(
                DPoint(360.0 + i * stepOffset, 0.0),
                DPoint(perCurveAmount * i, 360.0),
                DPoint(720.0 - perCurveAmount * i, 360.0),
                DPoint(360.0 + i * stepOffset * 4, 720.0)
            )
            bezier(
                DPoint(360.0 - i * stepOffset, 0.0),
                DPoint(720.0 - perCurveAmount * i, 360.0),
                DPoint(perCurveAmount * i, 360.0),
                DPoint(360.0 - i * stepOffset * 4, 720.0)
            )

            bezier(
                DPoint(0.0, 0.0),
                DPoint(720.0 - perCurveAmount * i, 360.0),
                DPoint(perCurveAmount * i, 360.0),
                DPoint(720.0 - i * perCurveAmount, 720.0)
            )

            bezier(
                DPoint(720.0, 0.0),
                DPoint(perCurveAmount * i, 360.0),
                DPoint(720.0 - perCurveAmount * i, 360.0),
                DPoint(0.0 + i * perCurveAmount, 720.0)
            )
        }

        

    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}