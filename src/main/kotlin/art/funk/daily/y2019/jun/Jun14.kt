package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.random.Random

object Jun14 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun14Sketch::class.java)
    }
}

class Jun14Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    var i = 0
    val totalWaves = 10

    val random = Random(1)

    // How many rotations for the entire width of the screen
    val rotations = listOf(
        0.3 / 3,
        0.4 / 3,
        0.5 / 3,
        0.6 / 3,
        0.8 / 3,
        0.9 / 3,
        1.1 / 3,
        1.3 / 3,
        1.5 / 3
    ).reversed()

    val amplitude = 340.0
    
    val othersDelta = 180.0

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        fill(Color(255, 255, 255, 0))
    }

    override fun draw() {

        val rotation = rotations[i]
        for (xo in 0..(myWidth - 10)) {
            stroke(Color(
                100, 100 + i * 10, 240, 40
            ))


            val x = xo + 10
            val y0 = 360.0 + amplitude * Math.sin(rotation / myWidth * Math.PI * 2 * (myHeight - x))
            val y1 = 360.0 + amplitude * Math.sin(rotation / myWidth * Math.PI * 2 * (myHeight - (x + 1)))

//            strokeWidth(0.5)
//            line(
//                DPoint(x.toDouble(), y0),
//                DPoint((x + 1).toDouble(), y1)
//            )

            strokeWidth(1.0)
//            line(
//                DPoint(x.toDouble(), y0),
//                DPoint(x.toDouble(), 720.0 - y0))


            line(
                DPoint(y0, x.toDouble()),
                DPoint(720.0 - y0, x.toDouble())
            )

            stroke(Color(10, 10, 10, 10))
            line(
                DPoint(y0, x.toDouble() - 500.0),
                DPoint(720.0 - y0, x.toDouble() + 500.0)
            )

            stroke(Color(
                100, 100 + i * 10, 240, 40
            ))

            line(
                DPoint(y0, x.toDouble()),
                DPoint(720.0 - y0, x.toDouble())
            )

        }

        i += 1

        if (i >= rotations.size) {
            fill(Color(255, 255, 255, 255))
            stroke(Color(255, 255, 255, 255))

            rectViaCorners(
                DPoint(720.0, 720.0),
                DPoint(0.0, 700.0),
                0.0)

            rectViaCorners(
                DPoint(0.0, 0.0),
                DPoint(720.0, 20.0),
                0.0)


            super.draw()
            save("outputs/" + javaClass.name)
        }

    }
}