package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.math.max
import kotlin.math.min
import kotlin.math.pow
import kotlin.math.sin

object Jun24 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun24Sketch::class.java)
    }
}

class Jun24Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val strokeWidth = 20.0

    val period = Math.PI / 200.0
    val amplitude = 30.0

    val offset = 25.0
    val halfCount = 20

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeWidth(strokeWidth)
        stroke(Color(40, 100, 20, 255))
        fill(Color(255, 255, 255, 0))
        strokeCap(PConstants.ROUND)

        val baseline = 360.0
        var previousPoint = DPoint(0.0, baseline)
        for (x in 0..myWidth) {
            val y = amplitude * sin(period * x) + baseline
            val point = DPoint(x.toDouble(), y)


            for (i in -halfCount .. halfCount) {
                val actualY = y + i * offset
                stroke(
                    Color(
                        40,
                        80,
                        20,
                        transparency(myHeight - actualY)
                    ))

                line(
                    previousPoint + DPoint(0.0, i * offset),
                    point + DPoint(0.0, i * offset)
                )

                stroke(
                    Color(
                        40,
                        40,
                        20,
                        transparency(myHeight / 2 + actualY) / 2
                    )
                )

                line(
                    previousPoint.mirrored() + DPoint(i * offset, 0.0),
                    point.mirrored() + DPoint(i * offset, 0.0)
                )
            }


            previousPoint = point
        }
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    private fun transparency(y: Double): Int {
        return max(0.0, min(255.0, 55.0 * (y / myHeight).pow(3.0))).toInt()
    }
}
