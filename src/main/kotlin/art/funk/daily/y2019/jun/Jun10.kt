package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color

object Jun10 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun10Sketch::class.java)
    }
}

class Jun10Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val midPoint = DPoint(
        myWidth / 2.0,
        myHeight / 2.0
    )

    var i = 0
    val totalIterations = 1100
    val rotationSpeed = 1 / 12.0
    val pointCount = 9
    val radius = 100.0

    val xSqueeze = 0.8

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        stroke(Color(40, 40, 80, 225))
        strokeWidth(1.0)
    }

    override fun draw() {
//        for (i0 in 0 until 10) {

            stroke(Color(40, 40, percentile(220, 60, i, totalIterations), 220))
            rotationPath(this::circleFunction)
            rotationPath(this::circleFunction2)
            rotationPath(this::circleFunction3)
            rotationPath(this::circleFunction4)
            rotationPath(this::circleFunction5)
            i += 1
//        }
        if (i >= totalIterations) {
            super.draw()
            save("outputs/" + javaClass.name)
        }
    }

    private fun percentile(max: Int, min: Int, i: Int, totalIterations: Int): Int {
        val delta = (max - min)
        return (i.toDouble() / totalIterations * delta + min).toInt()
    }


    /**
     * Draws a circle (in 3d or nah, hmm) with rotation around the chosen path
     */
    private fun rotationPath(func: Jun10ParametricFunction) {
        val point = func(i)
        val nextPoint = func(i + 1)

        for (pointIndex in 0 until pointCount) {
            val ratio = 1.0
            val initialTheta = Math.PI * 2 / pointCount * pointIndex
            val theta = initialTheta + rotationSpeed * i
            val nextTheta = initialTheta + rotationSpeed * (i + 1)
            val pointPoint = point + DPoint(
                (radius + ratio * 2.0) * Math.cos(theta) * xSqueeze,
                (radius + ratio * 2.0) * Math.sin(theta))

            val nextPointPoint = nextPoint + DPoint(
                (radius + ratio * 2.0) * Math.cos(nextTheta) * xSqueeze,
                (radius + ratio * 2.0) * Math.sin(nextTheta))

            line(pointPoint, nextPointPoint)
        }
    }


    private fun circleFunction(i: Int): DPoint {
        val t = i / 500.0
        val radius = 720.0 - i / 20.0
        return DPoint(0.0, 0.0) + DPoint(
            radius * Math.cos(t),
            radius * Math.sin(t)
        )
    }


    private fun circleFunction2(i: Int): DPoint {
        val t = i / 500.0
        val radius = 520.0 - i / 12.0
        return DPoint(0.0, 0.0) + DPoint(
            radius * Math.cos(t),
            radius * Math.sin(t)
        )
    }

    private fun circleFunction3(i: Int): DPoint {
        val t = i / 500.0
        val radius = 320.0 - i / 9.0
        return DPoint(0.0, 0.0) + DPoint(
            radius * Math.cos(t),
            radius * Math.sin(t)
        )
    }

    private fun circleFunction4(i: Int): DPoint {
        val t = i / 500.0
        val radius = 720.0 - i / 20.0
        return DPoint(720.0, 720.0) + DPoint(
            radius * Math.cos(t + Math.PI),
            radius * Math.sin(t + Math.PI)
        )
    }

    private fun circleFunction5(i: Int): DPoint {
        val t = i / 500.0
        val radius = 520.0 - i / 12.0
        return DPoint(720.0, 720.0) + DPoint(
            radius * Math.cos(t + Math.PI),
            radius * Math.sin(t + Math.PI)
        )
    }
}

typealias Jun10ParametricFunction = (Int) -> DPoint