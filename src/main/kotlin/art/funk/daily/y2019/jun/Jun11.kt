package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import art.funk.daily.core.diamond.DiamondCanvas
import art.funk.daily.core.math.linearRange
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color

object Jun11 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun11Sketch::class.java)
    }
}

class Jun11Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val size = 40.0
    val size2 = 20.0

    val diamondCanvas = DiamondCanvas(myHeight + 40, myWidth + 40, size, this::diamondDraw)
//    val diamondCanvas2 = DiamondCanvas(myHeight + 40, myWidth + 40, size2, this::diamondDraw2)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
        diamondCanvas.rounding = 0.0
        diamondCanvas.applyBorderAsSpace = false
//        diamondCanvas2.rounding = 0.0
    }

    override fun setup() {
        strokeWidth(0.0)
        stroke(Color(20, 20, 20, 100))
//        diamondCanvas2.drawCanvas(this)
        diamondCanvas.drawCanvas(this)
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    private fun diamondDraw(diamond: DiamondCanvas.Diamond): DiamondCanvas.DiamondStyle {
        val style = DiamondCanvas.DiamondStyle(diamond)
        val color = HighColor(Color(
            linearRange(20, 250, diamond.x, diamondCanvas.xDiamondCount),
            linearRange(200, 20, diamond.y, diamondCanvas.yDiamondCount),
            50,
            50
        ))
        style.color = HighColor(Color(
            50, 50, 50, 20
        ))
        style.borderStyle = DiamondCanvas.BorderStyle(30, color)

        return style
    }

    private fun diamondDraw2(diamond: DiamondCanvas.Diamond): DiamondCanvas.DiamondStyle {
        val style = DiamondCanvas.DiamondStyle(diamond)
        val color = HighColor(Color(
            10, 10, 10, 20
        ))
        style.color = color
        style.borderStyle = DiamondCanvas.BorderStyle(0, color)
        return style
    }
}