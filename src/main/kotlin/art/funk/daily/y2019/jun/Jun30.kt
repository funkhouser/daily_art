package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color
import kotlin.math.pow
import kotlin.random.Random

object Jun30 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun30Sketch::class.java)
    }
}

class Jun30Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val random = Random(3)
    val nodeCount = 28
    val nodes = arrayOfNulls<Int?>(nodeCount).map { _ ->
        DPoint(random.nextDouble(720.0), random.nextDouble(720.0)) to
                Color(100 - random.nextInt(80), 150 - random.nextInt(90), 250 - random.nextInt(60), 255)
    }
    val strokeWidth = 15.0

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        gradientRect(
            this,
            DPoint(0.0, 0.0),
            DPoint(720.0, 720.0)
        ) { point ->

            var color = Color(255, 255, 255, 255)
            nodes.forEach { node -> color = color.interpolate(node.second, interpolateFunction(point, node.first)) }
            color
        }

        stroke(Color(255, 255, 255, 25))
        noFill()
        strokeWidth(strokeWidth)
        strokeCap(StrokeCap.PROJECT)

        val delta = 50.0
        RecursiveDraw.recurseAngleDraw(
            this,
            {
                bezier(
                    DPoint(random.nextDouble(delta) + 120.0, random.nextDouble(delta) + 600.0),
                    DPoint(random.nextDouble(delta) + 120.0, random.nextDouble(delta) + 600.0),
                    DPoint(random.nextDouble(delta) + 650.0, random.nextDouble(delta) + 600.0),
                    DPoint(random.nextDouble(delta) + 600.0, random.nextDouble(delta) + 600.0)
                )
            },
            0.01,
            0.998,
            880
        )
    }

    private fun interpolateFunction(point: DPoint, node: DPoint): Double {
        val distance = (point - node).magnitude()
        return (1.0 - distance / (DPoint(720.0, 720.0).magnitude())).pow(2.0)
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}