package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color
import kotlin.random.Random

object Jun12 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun12Sketch::class.java)
    }
}

class Jun12Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    var i = 0
    val iterations = 400
    val stepSize = (myHeight.toDouble()) / iterations

    var fillColor = Color(255, 255, 255, 255)
    val random = Random(230)


    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
    }

    override fun draw() {

        var start = DPoint(0.0, 0.0)
        var finish = DPoint(100.0, 100.0)

        fillColor = Color(
            Math.min(255, Math.max(0, fillColor.red + random.nextInt(-5, 5))),
            Math.min(255, Math.max(0, fillColor.green + random.nextInt(-5, 5))),
            Math.min(255, Math.max(0, fillColor.blue + random.nextInt(-5, 5))),
            255)
        fill(fillColor)
        val point0 = DPoint(i * stepSize, i * stepSize)
        val point1 = DPoint((i + 1) * stepSize, (i + 1) * stepSize)

        val control0 = DPoint(random.nextDouble(720.0), random.nextDouble(720.0))
        val control1 = DPoint(random.nextDouble(720.0), random.nextDouble(720.0))

        bezier(
            point0,
            control0,
            control1,
            point1
        )

        i += 1


        if (i >= iterations) {
            super.draw()
            save("outputs/" + javaClass.name)
        }
    }
}