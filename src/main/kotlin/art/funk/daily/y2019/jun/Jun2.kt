package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color

object Jun2 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun2Sketch::class.java)
    }
}

class Jun2Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val pointNumber = 720

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))

        strokeWidth(3.0)
        parametricParabolic(
            pointNumber
        ) { i ->
            Jun2Line(
                DPoint(100.0 + 50.0 * Math.cos(i / 20.0), i.toDouble()),
                DPoint(600.0 + 50.0 * Math.cos(i / 20.0), (myHeight - i).toDouble()),
                Color(i / 3, i / 3, 150 + i / 8, 100 - i / 40)
            )
        }
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    private fun parametricParabolic(
        pointCount: Int,
        functions: Jun2ParametricFunction
    ) {
        for (i in 0..pointCount) {
            val result = functions(i)
            line(
                result.point0,
                result.point1,
                result.color
            )
        }
    }

    private fun parametricParabolic(
        pointFunction0: IterativePointFunction,
        pointFunction1: IterativePointFunction,
        colorFunction: IterativeColorFunction,
        pointCount: Int
    ) {
        for (i in 0..pointCount) {
            val point0 = pointFunction0(i)
            val point1 = pointFunction1(i)


            line(
                point0,
                point1,
                colorFunction(i)
                )
        }
    }
}

data class Jun2Line(
    val point0: DPoint,
    val point1: DPoint,
    val color: Color
)
typealias Jun2ParametricFunction = (Int) -> Jun2Line