package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color

object Jun20 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun20Sketch::class.java)
    }
}

class Jun20Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        val radius0 = 80.0
        val radius1 = 300.0
        val offsetRadians = Math.PI / 2
        val offsetRadians1 = 0.0
        val offsetRadians2 = Math.PI // Math.PI * 3 / 4
        val perCircle = 3
        val radiansPerPoint = Math.PI * 2 / perCircle
        val center = DPoint(360.0, 360.0)
        val lineCount = 300
        val points0: List<DPoint> = IntArray(perCircle).mapIndexed { index, _ ->
            val point0 = center + DPoint(
                Math.cos(index * radiansPerPoint),
                Math.sin(index * radiansPerPoint)) * radius0

            val point1 = center + DPoint(
                Math.cos(index * radiansPerPoint + offsetRadians),
                Math.sin(index * radiansPerPoint + offsetRadians)) * radius1

            listOf(point0, point1)
        }.flatten()


        val points1 = IntArray(perCircle).mapIndexed { index, _ ->
            val point0 = center + DPoint(
                Math.cos(index * radiansPerPoint),
                Math.sin(index * radiansPerPoint)) * radius0

            val point1 = center + DPoint(
                Math.cos(index * radiansPerPoint + offsetRadians1),
                Math.sin(index * radiansPerPoint + offsetRadians1)) * radius1

            listOf(point0, point1)
        }.flatten()

        val points2 = IntArray(perCircle).mapIndexed { index, _ ->
            val point0 = center + DPoint(
                Math.cos(index * radiansPerPoint),
                Math.sin(index * radiansPerPoint)) * radius0

            val point1 = center + DPoint(
                Math.cos(index * radiansPerPoint + offsetRadians2),
                Math.sin(index * radiansPerPoint + offsetRadians2)) * radius1

            listOf(point0, point1)
        }.flatten()

        val color0 = Color(159, 163, 234, 60)
        val color1 = Color(250, 130, 200, 60)
        val colorFunctions: List<IterativeColorFunction> =
            IntArray(perCircle).mapIndexed { index, _ ->
                { i: Int -> color0.interpolate(color1, index.toDouble() / perCircle) }
            }


        parabolicRotation(points0, colorFunctions, lineCount)
        parabolicRotation(points1, colorFunctions, lineCount)
        parabolicRotation(points2, colorFunctions, lineCount)
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    private fun parabolicRotation(points: List<DPoint>, colorFunctions: List<IterativeColorFunction>, lineCount: Int) {
        points.windowedCircular(3).forEachIndexed { index, (point0, point1, point2) ->
            parabolicDraw(
                point0,
                point2,
                point1,
                lineCount,
                colorFunctions[index % colorFunctions.size]
            )
        }
    }

    private fun parabolicDraw(
        startPoint: DPoint,
        endPoint: DPoint,
        pivotPoint: DPoint,
        lineCount: Int,
        colorFunction: IterativeColorFunction
    ) {
        // TODO Start the 'a' line at the startPoint and the 'b' line at the pivot point
        // Increment the 'a' point and the 'b' point on each iteration until conclusion
        for (i in 0..lineCount) {

            // TODO When turning this into a plugin-style piece change this to be a function such that
            // the user can either provide a delta from the line or simply a parametric function
            val aPoint = DPoint(
                startPoint.x + (pivotPoint.x - startPoint.x) * i / lineCount,
                startPoint.y + (pivotPoint.y - startPoint.y) * i / lineCount
            )

            val bPoint = DPoint(
                pivotPoint.x + (endPoint.x - pivotPoint.x) * i / lineCount,
                pivotPoint.y + (endPoint.y - pivotPoint.y) * i / lineCount
            )

            line(
                aPoint,
                bPoint,
                colorFunction(lineCount)
            )
        }
    }
}