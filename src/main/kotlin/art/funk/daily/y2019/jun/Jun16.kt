package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color
import kotlin.random.Random

object Jun16 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun16Sketch::class.java)
    }
}

class Jun16Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val random = Random(21)

    val rootColor = Color(249, 208, 136, 155)
    val secondaryColor = Color(249, 163, 136, 155)

//    val rootColor = Color(0, 0, 0, 255)
//    val secondaryColor = Color(255, 255, 0, 255)

    val rootColor1 = Color(253, 180, 221, 155)
    val secondaryColor1 = Color(253, 200, 201, 155)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {

        background(Color(255, 255, 255, 255))

        val rate = Math.PI / 100.0
        val center = DPoint(360.0, 360.0)

        gradientRect(
            this,
            DPoint(0.0, 0.0),
            DPoint(720.0, 720.0)) { point ->

            val amount = (Math.sin((point - center).magnitude() * rate) + 1) / 2

            rootColor.interpolate(secondaryColor, amount)
        }

        gradientRect(
            this,
            DPoint(0.0, 0.0),
            DPoint(720.0, 720.0)) { point ->

            rootColor1.interpolate(secondaryColor1, point.x / 720.0)
        }


        fill(Color(255, 255, 255, 255))
        stroke(Color(255, 255, 255, 0))
        borderWindow(105.0)
        triangle(
            DPoint(0.0, 0.0),
            DPoint(360.0, 0.0),
            DPoint(0.0, 360.0))

        triangle(
            DPoint(360.0, 0.0),
            DPoint(720.0, 0.0),
            DPoint(720.0, 360.0))

        triangle(
            DPoint(720.0, 360.0),
            DPoint(720.0, 720.0),
            DPoint(360.0, 720.0))

        triangle(
            DPoint(360.0, 720.0),
            DPoint(0.0, 720.0),
            DPoint(0.0, 360.0))

        for (i in 0..10) {
            val size = random.nextDouble(40.0)
            val x = random.nextDouble(720.0)

            rectViaSize(
                DPoint(x, 0.0),
                size,
                720.0,
                0.0
            )
        }
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}