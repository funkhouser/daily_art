package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color

object Jun25_2 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun25_2Sketch::class.java)
    }
}

class Jun25_2Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720
    val strands = 9
    val offset = 25.0
    val minRadius = 10.0

    val color0 = Color(195, 195, 224, 25)
    val color1 = Color(222, 78, 117, 25)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        fill(Color(255, 255, 255, 0))
        strokeWidth(15.0)
        strokeCap(PConstants.PROJECT)

        val center0 = DPoint(200.0, 200.0)

        for (index in 0 until strands) {
            gradientArc(
                center0,
                minRadius + index * offset,
                -Math.PI / 2,
                3 * Math.PI / 2,
                color0,
                color1
            )

            val x0 = center0.x - (minRadius + index * offset)
            val y0 = center0.y

            val y1 = myHeight - center0.y

            line(
                LinearGradientLine(
                    DPoint(x0, y0),
                    DPoint(x0, y1),
                    color1,
                    color0
                )
            )

            val center1 = DPoint(center0.x, y1)
            gradientArc(
                center1,
                minRadius + index * offset,
                Math.PI,
                3 * Math.PI,
                color0,
                color1
            )

            line(
                LinearGradientLine(
                    DPoint(center1.x, center1.y + minRadius + index * offset),
                    DPoint(myWidth - center1.x, center1.y + minRadius + index * offset),
                    color1,
                    color0
                )
            )

            val center2 = DPoint(myWidth - center0.x, myHeight - center0.y)

            gradientArc(
                center2,
                minRadius + index * offset,
                Math.PI / 2,
                5 * Math.PI,
                color0,
                color1
            )

            line(
                LinearGradientLine(
                    DPoint(center2.x + (minRadius + index * offset), center2.y),
                    DPoint(center2.x + (minRadius + index * offset), myHeight - center2.y),
                    color1,
                    color0
                )
            )

            val center3 = DPoint(myWidth - center0.x, center0.y)

            gradientArc(
                center3,
                minRadius + index * offset,
                0.0,
                4 * Math.PI / 2,
                color0,
                color1
            )

            line(
                LinearGradientLine(
                    DPoint(center3.x, center3.y - (minRadius + index * offset)),
                    DPoint(center0.x, center3.y - (minRadius + index * offset)),
                    color1,
                    color0
                )
            )

            val center = DPoint(360.0, 360.0)
            stroke(Color(255, 255, 255, 155))
            arc(
                center,
                minRadius + index * offset,
                0.0,
                2 * Math.PI
            )
        }
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}