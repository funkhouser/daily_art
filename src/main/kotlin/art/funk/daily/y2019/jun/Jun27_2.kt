package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import gifAnimation.GifMaker
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.math.floor
import kotlin.random.Random

object Jun27_2 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun27_2Sketch::class.java)
    }
}

class Jun27_2Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val gridSize = 70.0
    val rows = floor(myHeight / gridSize).toInt() + 1
    val columns = floor(myWidth / gridSize).toInt() + 1

    val linesPer = 5
    val initialOffset = (gridSize / linesPer) / 2
    val lineOffset = (gridSize / linesPer)
    val strokeWidth = 14.0

    val random = Random(51)

    val color0 = Color(100, 235, 195, 255)
    val color1 = Color(10, 10, 10, 255)
    var color = color0

    var i = 0
    val growIterations = 1000
    val iterationsPerFrame = 5
    var currentCell: Pair<Point, GridDraw> =
        Pair(Point(random.nextInt(columns), random.nextInt(rows)), GridDraw.HORIZONTAL)
    var previousCell: Pair<Point, GridDraw> =
        Pair(Point(currentCell.first.x - 1, currentCell.first.y), GridDraw.HORIZONTAL)

    val cellsDrawn = mutableListOf<Point>()

    val enableGif = false
    val gifExport = GifMaker(this, "outputs/" + javaClass.name + ".gif")


    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))

        stroke(color0)
        strokeCap(PConstants.SQUARE)
        strokeWidth(strokeWidth)
        fill(Color(255, 255, 255, 0))

        gifExport.setDelay(1000 / 6)
        gifExport.setRepeat(0)
    }

    override fun draw() {

        drawCurrentCell()
        val nextLocation = getNextLocation()

        if (i >= growIterations || nextLocation == null) {
            super.draw()
            save("outputs/" + javaClass.name)
            if (enableGif) {
                gifExport.addFrame()
                gifExport.addFrame()
                gifExport.addFrame()
                gifExport.addFrame()

                gifExport.finish()
            }

            return
        }

        if (i % iterationsPerFrame == 0 && enableGif) {
            gifExport.addFrame()
        }

        val nextGridDraw = getNextGridDraw(nextLocation)

        previousCell = currentCell
        currentCell = Pair(nextLocation, nextGridDraw)

        i += 1
    }


    private fun drawCurrentCell() {
        drawCell(currentCell.first.x, currentCell.first.y, currentCell.second)
    }

    private fun getNextLocation(): Point? {
        val prevX = previousCell.first.x
        val prevY = previousCell.first.y
        val x = currentCell.first.x
        val y = currentCell.first.y
        var nextPointMaybe: Point? = when (currentCell.second) {
            GridDraw.VERTICAL -> if (y > prevY) {
                Point(x, y + 1)
            } else {
                Point(x, y - 1)
            }
            GridDraw.HORIZONTAL -> if (x > prevX) {
                Point(x + 1, y)
            } else {
                Point(x - 1, y)
            }
            GridDraw.LEFT_TOP -> if (x > prevX) {
                Point(x, y - 1)
            } else {
                Point(x - 1, y)
            }
            GridDraw.LEFT_BOTTOM -> if (x > prevX) {
                Point(x, y + 1)
            } else {
                Point(x - 1, y)
            }
            GridDraw.RIGHT_TOP -> if (x < prevX) {
                Point(x, y - 1)
            } else {
                Point(x + 1, y)
            }
            GridDraw.RIGHT_BOTTOM -> if (x < prevX) {
                Point(x, y + 1)
            } else {
                Point(x + 1, y)
            }
            GridDraw.NONE -> null
        }

        if (nextPointMaybe == null || outOfBounds(nextPointMaybe)) {
            nextPointMaybe = getRandomPoint()
        }

        return nextPointMaybe
    }

    private fun getNextGridDraw(next: Point): GridDraw {
        val current = currentCell.first
        val availableGridDraws = when {
            next.x - current.x == 1 -> listOf(GridDraw.HORIZONTAL, GridDraw.LEFT_BOTTOM, GridDraw.LEFT_TOP)
            next.x - current.x == -1 -> listOf(GridDraw.HORIZONTAL, GridDraw.RIGHT_BOTTOM, GridDraw.RIGHT_TOP)
            next.y - current.y == 1 -> listOf(GridDraw.VERTICAL, GridDraw.RIGHT_TOP, GridDraw.LEFT_TOP)
            next.y - current.y == -1 -> listOf(GridDraw.VERTICAL, GridDraw.RIGHT_BOTTOM, GridDraw.LEFT_BOTTOM)
            else -> listOf(GridDraw.NONE)
        }

        // For now: entirely unweighted with regards to which gets chosen next
        return availableGridDraws.random(random)
    }

    private fun outOfBounds(point: Point): Boolean {
        return !(point.x in 0..columns && point.y in 0..rows)
    }

    private fun getRandomPoint(): Point? {
        var i = 0
        val maxTries = 10
        var point = Point(random.nextInt(columns), random.nextInt(rows))
        while (cellsDrawn.contains(point) && i < maxTries) {
            point = Point(random.nextInt(columns), random.nextInt(rows))
            i += 1
        }
        return if (i >= maxTries) {
            null
        } else {
            point
        }
    }

    private fun drawCell(column: Int, row: Int, gridDraw: GridDraw) {
        if (cellsDrawn.contains(Point(column, row))) {
            // Determines whether things get overdrawn or not
//            return
        }
        // Different effect, really cool in its own way
        color = color0
        cellsDrawn.add(Point(column, row))
        val topLeft = DPoint(column * gridSize, row * gridSize)
        for (i in 0 until linesPer) {
            color = if (color == color0) {
                color1
            } else {
                color0
            }
            stroke(color)
            when (gridDraw) {
                GridDraw.VERTICAL -> {
                    line(
                        topLeft + DPoint(i * lineOffset + initialOffset, 0.0),
                        topLeft + DPoint(i * lineOffset + initialOffset, gridSize)
                    )
                }
                GridDraw.HORIZONTAL -> {
                    line(
                        topLeft + DPoint(0.0, i * lineOffset + initialOffset),
                        topLeft + DPoint(gridSize, i * lineOffset + initialOffset)
                    )
                }
                GridDraw.LEFT_TOP -> {
                    arc(
                        topLeft,
                        i * lineOffset + initialOffset,
                        0.0,
                        Math.PI / 2
                    )
                }
                GridDraw.LEFT_BOTTOM -> {
                    arc(
                        topLeft + DPoint(0.0, gridSize),
                        i * lineOffset + initialOffset,
                        3 * Math.PI / 2,
                        2 * Math.PI
                    )
                }
                GridDraw.RIGHT_TOP -> {
                    arc(
                        topLeft + DPoint(gridSize, 0.0),
                        i * lineOffset + initialOffset,
                        Math.PI / 2,
                        Math.PI
                    )
                }
                GridDraw.RIGHT_BOTTOM -> {
                    arc(
                        topLeft + DPoint(gridSize, gridSize),
                        i * lineOffset + initialOffset,
                        Math.PI,
                        3 * Math.PI / 2
                    )
                }
                GridDraw.NONE -> {
                }
            }
        }

    }

    enum class GridDraw {
        VERTICAL,
        HORIZONTAL,
        LEFT_TOP,
        LEFT_BOTTOM,
        RIGHT_TOP,
        RIGHT_BOTTOM,
        NONE
    }
}