package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color
import kotlin.random.Random

object Jun18 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun18Sketch::class.java)
    }
}

class Jun18Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val strokeColor = Color(205, 200, 250, 40)
    val lineCount = 300

    val random = Random(15)


    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        strokeWidth(1.0)
        background(Color(255, 255, 255, 255))

        val points = listOf(
//            DPoint(0.0, 0.0),
//            DPoint(0.0, 720.0),
//            DPoint(720.0, 0.0),
            DPoint(720.0, 720.0)
        ) + IntArray(20).map { i ->

            val x = skewedRandom(myWidth.toDouble())
            val y = skewedRandom(myHeight.toDouble())

            DPoint(x, y)
        }

        val colorFunctions: List<IterativeColorFunction> = IntArray(20).map { i ->
            val color =  Color(
                strokeColor.red - random.nextInt(120),
                strokeColor.green - random.nextInt(120),
                strokeColor.blue - random.nextInt(50),
                strokeColor.alpha
            )
            val function = { index: Int ->
                color
            }

            function
        }

        parabolicRotation(
            points,
            colorFunctions,
            lineCount
        )
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    private fun skewedRandom(size: Double): Double {
        val doubledRandomY = random.nextDouble(size / 2) + random.nextDouble(size / 2)

        return if (doubledRandomY > size / 2) {
            val delta = doubledRandomY - size /2
            size - delta
        } else {
            val delta = size / 2 - doubledRandomY
            delta
        }
    }

    private fun parabolicRotation(points: List<DPoint>, colorFunctions: List<IterativeColorFunction>, lineCount: Int) {
        points.windowedCircular(3).forEachIndexed { index, (point0, point1, point2) ->
            parabolicDraw(
                point0,
                point2,
                point1,
                lineCount,
                colorFunctions[index % colorFunctions.size]
            )
        }
    }

    private fun parabolicDraw(
        startPoint: DPoint,
        endPoint: DPoint,
        pivotPoint: DPoint,
        lineCount: Int,
        colorFunction: IterativeColorFunction
    ) {
        // TODO Start the 'a' line at the startPoint and the 'b' line at the pivot point
        // Increment the 'a' point and the 'b' point on each iteration until conclusion
        for (i in 0..lineCount) {

            // TODO When turning this into a plugin-style piece change this to be a function such that
            // the user can either provide a delta from the line or simply a parametric function
            val aPoint = DPoint(
                startPoint.x + (pivotPoint.x - startPoint.x) * i / lineCount,
                startPoint.y + (pivotPoint.y - startPoint.y) * i / lineCount
            )

            val bPoint = DPoint(
                pivotPoint.x + (endPoint.x - pivotPoint.x) * i / lineCount,
                pivotPoint.y + (endPoint.y - pivotPoint.y) * i / lineCount
            )

            line(
                aPoint,
                bPoint,
                colorFunction(lineCount)
            )
        }
    }
}