package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color

object Jun2_2 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun2_2Sketch::class.java)
    }
}

class Jun2_2Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720


    val pointNumber = 720

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))

        strokeWidth(3.0)
        parametricParabolic(
            pointNumber
        ) { i ->
            Jun2_2Line(
                DPoint(100.0 + 150.0 * Math.cos(i / 5.0), i.toDouble()),
                DPoint(600.0 + 150.0 * Math.cos(i / 5.0), (myHeight - i).toDouble()),
                Color(i / 3, i / 3, 150 + i / 8, 100 - i / 40)
            )
        }
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    private fun parametricParabolic(
        pointCount: Int,
        functions: Jun2_2ParametricFunction
    ) {
        for (i in 0..pointCount) {
            val result = functions(i)
            line(
                result.point0,
                result.point1,
                result.color
            )
        }
    }

}

data class Jun2_2Line(
    val point0: DPoint,
    val point1: DPoint,
    val color: Color
)
typealias Jun2_2ParametricFunction = (Int) -> Jun2_2Line