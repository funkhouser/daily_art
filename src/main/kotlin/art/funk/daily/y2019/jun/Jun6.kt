package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import gifAnimation.GifMaker
import processing.core.PApplet
import java.awt.Color

object Jun6 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun6Sketch::class.java)
    }
}

class Jun6Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720
    var i = 0
    val totalPoints = 1575 * 2
    val gifExport = GifMaker(this, "outputs/" + javaClass.name + ".gif")

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    val r0 = 80.0
    val bigR0 = 200.0
    val a0 = r0 * 3

    val r1 = 40.0
    val bigR1 = 200.0
    val a1 = r0 * 2


    val x0 = 360.0
    val y0 = 360.0

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeWidth(2.0)
        gifExport.setDelay(1000 / 12)
        gifExport.setRepeat(0)
    }

    override fun draw() {
        val index = i
        val t = index / 100.0
        val point0 = DPoint(
            (bigR0 - r0) * Math.cos(r0 / bigR0 * t) + a0 * Math.cos((1 - r0 / bigR0) * t) + x0,
            (bigR0 - r0) * Math.sin(r0 / bigR0 * t) - a0 * Math.sin((1 - r0 / bigR0) * t) + y0
        )

//            val t0 = t + 1
//            val point1 = DPoint(
//                (bigR0 - r0) * Math.cos(r0 / bigR0 * t0) + a0 * Math.cos((1 - r0 / bigR0) * t0) + x0 + 10.0,
//                (bigR0 - r0) * Math.sin(r0 / bigR0 * t0) - a0 * Math.sin((1 - r0 / bigR0) * t0) + y0 + 10.0)

        val point1 = DPoint(
            (bigR1 - r1) * Math.cos(r1 / bigR0 * t) + a1 * Math.cos((1 - r1 / bigR1) * t) + x0 + 10.0,
            (bigR1 - r1) * Math.sin(r1 / bigR0 * t) - a1 * Math.sin((1 - r1 / bigR1) * t) + y0 + 10.0
        )

        line(LinearGradientLine(
            point0,
            point1,
            Color(160, 100, 160, 155),
            Color(60, 0, 60, 55)
        ))

        // Double with black
        line(ColoredLine(
            DPoint(point0.x + 0.5, point0.y),
            DPoint(point1.x + 0.5, point1.y),
            Color(0, 0, 0, 50)
        ))

        i += 1
        if (i > totalPoints) {
            super.draw()
            save("outputs/" + javaClass.name)

            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()

            gifExport.finish()
        }

        if (i % 10 == 0) {
            gifExport.addFrame()
        }
    }


    private fun indexedParametricParabolic(
        index: Int,
        functions: IterativeGradientLineFunction
    ) {
        val result = functions(index)
        line(result)
    }
}