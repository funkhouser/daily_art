package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color
import kotlin.random.Random

object Jun15 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun15Sketch::class.java)
    }
}

class Jun15Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val random = Random(10)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))

        gradientRect(
            this,
            DPoint(0.0, 0.0),
            DPoint(720.0, 720.0)) { point ->

            Color(
                100,
                (55 * point.x / 720.0).toInt(),
                (255 * point.y / 720.0).toInt())

        }

        for (i in 0..2000) {
            val size = random.nextDouble(240.0)
            val rotation = random.nextDouble(Math.PI * 2)
            val x = random.nextDouble(720.0 + size) - size
            val y = random.nextDouble(720.0 + size) - size

            stroke(Color(255, 255, 255, 0))
            fill(Color(255, 255, 255, 5))
            rectViaSizeOffsetRotation(
                DPoint(x, y),
                size,
                size,
                3.0,
                rotation
            )
        }


        fill(Color(255, 255, 255, 255))
        val overlap = 40.0
        triangle(
            DPoint(0.0, 0.0),
            DPoint(360.0, 0.0),
            DPoint(0.0, 360.0))

        triangle(
            DPoint(360.0, 0.0),
            DPoint(720.0, 0.0),
            DPoint(720.0, 360.0))

        triangle(
            DPoint(720.0, 360.0),
            DPoint(720.0, 720.0),
            DPoint(360.0, 720.0))

        triangle(
            DPoint(360.0, 720.0),
            DPoint(0.0, 720.0),
            DPoint(0.0, 360.0))

        borderWindow(3.0)

    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }


}
