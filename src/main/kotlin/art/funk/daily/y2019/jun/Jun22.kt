package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color
import kotlin.random.Random

object Jun22 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun22Sketch::class.java)
    }
}

class Jun22Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    var i = 0
    val recurseIterations = 200

    val random = Random(290)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeWidth(0.2)
        stroke(Color(255, 255, 255, 0))
    }

    override fun draw() {
        RecursiveDraw.indexedRecurseAngleDraw(
            this,
            {
//                 this.line(ColoredLine(
//                     DPoint(100.0, 100.0),
//                     DPoint(6000.0, 6000.0),
//                     Color(50, 0, 0, 255)
//                 ))

//                this.stroke(Color(50, 0, 0, 155))
                this.fill(Color(155 - i / 2, 5, 5, i / 2))
                this.bezier(
                    DPoint(100.0, 100.0),
                    DPoint(random.nextDouble(1000.0), random.nextDouble(1000.0)),
                    DPoint(random.nextDouble(1000.0), random.nextDouble(1000.0)),
                    DPoint(6000.0, 6000.0)
                )
            },
            0.01,
            0.98,
            i,
            DPoint(0.0, 0.0)
        )

        RecursiveDraw.indexedRecurseAngleDraw(
            this,
            {
//                this.stroke(Color(50, 0, 0, 155))
                this.fill(Color(155 - i / 2, 5, 5, i / 2))
                this.bezier(
                    DPoint(100.0, 100.0),
                    DPoint(random.nextDouble(1000.0), random.nextDouble(1000.0)),
                    DPoint(random.nextDouble(1000.0), random.nextDouble(1000.0)),
                    DPoint(6000.0, 6000.0)
                )
            },
            -0.02,
            1.0,
            i,
            DPoint(0.0, 0.0)
        )

        if (i >= recurseIterations) {
            super.draw()
            save("outputs/" + javaClass.name)
        }

        i += 1

    }
}