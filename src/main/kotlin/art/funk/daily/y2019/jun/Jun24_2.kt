package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color

object Jun24_2 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun24_2Sketch::class.java)
    }
}

class Jun24_2Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val strokes = 5
    val strokeWidth = 20.0
    val offset = 35.0

    val minRadius = 107.0

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeWidth(strokeWidth)
        stroke(Color(0, 0, 0, 100))
        strokeCap(SQUARE)
        fill(Color(255, 255, 255, 0))


        var x = 150.0
        while (x < myWidth) {

            val center0 = DPoint(x, 360.0)

            for (i in 0 until strokes) {
                val radius = minRadius + i * offset

                arc(
                    center0,
                    radius,
                    0.0,
                    Math.PI
                )
            }

            x += offset * (strokes) + strokeWidth - minRadius
            val center1 = DPoint(x, 360.0)

            for (i in 0 until strokes) {
                val radius = minRadius + i * offset

                arc(
                    center1,
                    radius,
                    Math.PI,
                    Math.PI * 2
                )
            }

            x += offset * (strokes) + strokeWidth - minRadius
        }


    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}