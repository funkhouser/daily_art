package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color
import kotlin.random.Random

object Jun1_2 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun1_2Sketch::class.java)
    }
}

class Jun1_2Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val initialPoint = DPoint(myWidth.toDouble() / 2 + 100.0, myHeight.toDouble() / 2)
    val radius = 230.0

    val pointCount = 10
    val linesPer = 30

    val random = Random(10)

    var currentFrame = 0
    val totalFrames = 55

    val recurseAngle = Math.PI  / 20
    val shrink = 0.98

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeWidth(1.5)
    }

    override fun draw() {
        currentFrame += 1
        RecursiveDraw.indexedRecurseAngleDraw2(
            this,
            { myInt ->
                drawFlower()
            },
            recurseAngle,
            shrink,
            currentFrame
        )

        if (currentFrame > totalFrames) {
            super.draw()
            save("outputs/" + javaClass.name)
            finished = true
        }

    }

    private fun drawFlower() {
        val thetaDelta = Math.PI * 2 * 2 / 11
        var theta = 0.0
        val points = IntArray(pointCount).map { i ->
            val point = DPoint(
                initialPoint.x + radius * Math.cos(theta),
                initialPoint.y + radius * Math.sin(theta))
            theta += thetaDelta
            point
        }.toMutableList()

//        points.shuffle(random)

        parabolicRotation(
            points,
            listOf { _ ->
                Color(100, 230, 160, 30)
            },
            linesPer)
    }

    private fun parabolicRotation(points: List<DPoint>, colorFunctions: List<IterativeColorFunction>, lineCount: Int) {
        points.windowedCircular(3).forEachIndexed { index, (point0, point1, point2) ->
            parabolicDraw(
                point0,
                point2,
                point1,
                lineCount,
                colorFunctions[index % colorFunctions.size]
            )
        }
    }

    private fun parabolicDraw(
        startPoint: DPoint,
        endPoint: DPoint,
        pivotPoint: DPoint,
        lineCount: Int,
        colorFunction: IterativeColorFunction
    ) {
        // TODO Start the 'a' line at the startPoint and the 'b' line at the pivot point
        // Increment the 'a' point and the 'b' point on each iteration until conclusion
        for (i in 0..lineCount) {

            // TODO When turning this into a plugin-style piece change this to be a function such that
            // the user can either provide a delta from the line or simply a parametric function
            val aPoint = DPoint(
                startPoint.x + (pivotPoint.x - startPoint.x) * i / lineCount,
                startPoint.y + (pivotPoint.y - startPoint.y) * i / lineCount
            )

            val bPoint = DPoint(
                pivotPoint.x + (endPoint.x - pivotPoint.x) * i / lineCount,
                pivotPoint.y + (endPoint.y - pivotPoint.y) * i / lineCount
            )

            line(
                aPoint,
                bPoint,
                colorFunction(lineCount)
            )
        }
    }
}