package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.random.Random

object Jun26_2 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun26_2Sketch::class.java)
    }
}

class Jun26_2Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val gridSize = 50.0

    val linesPer = 3
    val initialOffset = (gridSize / linesPer) / 2
    val lineOffset = (gridSize / linesPer)
    val strokeWidth = 8.0
    val growIterations = 100

    val random = Random(50)

    val color0 = Color(100, 235, 195, 255)
    val color1 = Color(10, 10, 10, 255)
    var color = color0

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))

        stroke(color0)
        strokeCap(PConstants.SQUARE)
        strokeWidth(strokeWidth)
        fill(Color(255, 255, 255, 0))

        for (column in 0..(myWidth / gridSize).toInt()) {
            for (row in 0..(myHeight / gridSize).toInt()) {
                val gridDraw = GridDraw.values().random(random)
                drawCell(column, row, gridDraw)
            }
        }
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    private fun drawCell(column: Int, row: Int, gridDraw: GridDraw) {

        // Different effect, really cool in its own way
        // color = color0

        val topLeft = DPoint(column * gridSize, row * gridSize)
        for (i in 0 until linesPer) {
            color = if (color == color0) { color1 } else { color0 }
            stroke(color)
            when (gridDraw) {
                GridDraw.VERTICAL -> {
                    line(
                        topLeft + DPoint(i * lineOffset + initialOffset, 0.0),
                        topLeft + DPoint(i * lineOffset + initialOffset, gridSize)
                    )
                }
                GridDraw.HORIZONTAL -> {
                    line(
                        topLeft + DPoint(0.0, i * lineOffset + initialOffset),
                        topLeft + DPoint(gridSize, i * lineOffset + initialOffset)
                    )
                }
                GridDraw.LEFT_TOP -> {
                    arc(
                        topLeft,
                        i * lineOffset + initialOffset,
                        0.0,
                        Math.PI / 2
                    )
                }
                GridDraw.LEFT_BOTTOM -> {
                    arc(
                        topLeft + DPoint(0.0, gridSize),
                        i * lineOffset + initialOffset,
                        3 * Math.PI / 2,
                        2 * Math.PI
                    )
                }
                GridDraw.RIGHT_TOP -> {
                    arc(
                        topLeft + DPoint(gridSize, 0.0),
                        i * lineOffset + initialOffset,
                        Math.PI / 2,
                        Math.PI
                    )
                }
                GridDraw.RIGHT_BOTTOM -> {
                    arc(
                        topLeft + DPoint(gridSize, gridSize),
                        i * lineOffset + initialOffset,
                        Math.PI,
                        3 * Math.PI / 2
                    )
                }
                GridDraw.NONE -> {}
            }
        }

    }

    enum class GridDraw {
        VERTICAL,
        HORIZONTAL,
        LEFT_TOP,
        LEFT_BOTTOM,
        RIGHT_TOP,
        RIGHT_BOTTOM,
        NONE
    }
}