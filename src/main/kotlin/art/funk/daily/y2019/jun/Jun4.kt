package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.math.roundToInt
import kotlin.random.Random

object Jun4 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun4Sketch::class.java)
    }
}

class Jun4Sketch : PApplet() {
    
    data class Vertex(val point0: DPoint, val point1: DPoint)

    val myHeight = 720
    val myWidth = 720

    var i = 0
    val totalPoints = 100
    val random = Random(11)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        stroke(Color(0, 50, 50, 50))
        strokeWidth(1.0)

        
        val points = createPointsFractally()
        val allVertices = createAllVertices(points)
        val vertices = createConnectedGraph(allVertices)
        val triangles = consolidateToTriangles(vertices)

        triangles.forEach { drawTriangle(it) }
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)


//        i += 1
//        if (i >= totalPoints) {
//            super.draw()
//            save("outputs/" + javaClass.name)
//        }
    }
    
    private fun drawTriangle(triangle: Triangle) {
        val color = Color(100, 220 - random.nextInt(30), 255 - random.nextInt(10), 255 - random.nextInt(150))
        fill(color)
        triangle(triangle)
    }

    private fun createPointsFractally(): List<DPoint> {
//        var pointsList = listOf(
//            DPoint(200.0, 200.0),
//            DPoint(myWidth.toDouble() - 200, 200.0),
//            DPoint(myWidth.toDouble() - 200, myHeight.toDouble() - 200),
//            DPoint(200.0, myHeight.toDouble() - 200))

        var pointsList = listOf(
            DPoint(160.0, 160.0),
            DPoint(161.0, 160.0),
            DPoint(161.0, 161.0),
            DPoint(160.0, 161.0),

            DPoint(0.0, 0.0),
            DPoint(myWidth.toDouble(), 0.0),
            DPoint(myWidth.toDouble(), myHeight.toDouble()),
            DPoint(0.0, myHeight.toDouble()))

        
        val iterations = 2
        val expansionFactor = 4

        for (i in 0..iterations) {
            pointsList = pointsList.flatMap { point0 ->
                // Take only a few points
                (0..expansionFactor).mapNotNull { j ->
                    val point1 = pointsList.random(random) ?: error("")
                    if (point0 == point1 || point0.x > point1.x || (point0.x == point1.x && point0.y > point1.y)) {
                        null
                    } else {
                        interpolatePoints(point0, point1)
                    }
                } + point0
            }
        }

        return pointsList
    }

    private fun createAllVertices(points: List<DPoint>): List<Vertex> {
        return points.flatMap { point0 ->
            points.mapNotNull { point1 ->
                if (point0 == point1 || point0.x > point1.x || (point0.x == point1.x && point0.y > point1.y)) {
                    null
                } else {
                    Vertex(point0, point1)
                }
            }
        }.sortedBy { distance(it) }
    }

    private fun createConnectedGraph(allVertices: List<Vertex>): List<Vertex> {
        val vertices = mutableListOf<Vertex>()

        // Construct the actually spanning tree beginning from the smallest
        // vertices such that no two vertices are overlapping
        allVertices.forEach { vertex ->
            if (!vertices.any { existingVertex -> linesIntersect(vertex, existingVertex) }) {
                vertices.add(vertex)
            }
        }

        // Vertices should now be the maximum viable set.
        return vertices
    }

    private fun interpolatePoints(point0: DPoint, point1: DPoint): DPoint {
        val xDelta = point1.x - point0.x + 150.0
        val yDelta = point1.y - point0.y + 150.0

        // First attempt: mutate only the height (with a slight offset to (x,y) prevent exact overlaps). This will naturally create a grid.
        val x = bounded(random.nextDouble() * xDelta + point0.x, 0.0, myWidth.toDouble())
        val y = bounded(random.nextDouble() * yDelta + point0.y, 0.0, myHeight.toDouble())

        val newPoint = DPoint(x, y)

        return newPoint
    }

    private fun bounded(double: Double, min: Double, max: Double): Double {
        return Math.max(min, Math.min(max, double))
    }

    private fun linesIntersect(v0: Vertex, v1: Vertex): Boolean {
        if (getConnectionPoint(v0, v1) != null) {
            return false
        }
        val crossingPointFactor0 = findCrossingPointFactor(v0, v1)
        val crossingPointFactor1 = findCrossingPointFactor(v1, v0)

        return crossingPointFactor0 > 0 && crossingPointFactor0 < 1 && crossingPointFactor1 > 0 && crossingPointFactor1 < 1
    }

    private fun getConnectionPoint(v0: Vertex, v1: Vertex): DPoint? {
        return when {
            v0.point0 == v1.point0 || v0.point1 == v1.point0 -> v1.point0
            v0.point0 == v1.point1 || v0.point1 == v1.point1 -> v1.point1
            else -> null
        }
    }

    private fun findCrossingPointFactor(v0: Vertex, v1: Vertex): Double {
        val delta0 = DPoint(v0.point1.x - v0.point0.x, v0.point1.y - v0.point0.y)
        val delta1 = DPoint(v1.point1.x - v1.point0.x, v1.point1.y - v1.point0.y)
        val factor = ((v1.point0 - v0.point0) * delta1) / (delta0 * delta1)
        return factor
    }

    private fun distance(vertex: Vertex): Double {
        return (vertex.point0.x - vertex.point1.x) * (vertex.point0.x - vertex.point1.x) +
                (vertex.point0.y - vertex.point1.y) * (vertex.point0.y - vertex.point1.y)

    }


    private fun consolidateToTriangles(vertices: List<Vertex>): List<Triangle> {
        val triangles: List<Triangle> = vertices.withIndex().flatMap { (index, vertex0) ->
            vertices.subList(index, vertices.size).mapNotNull { vertex1 ->
                val point = getConnectionPoint(vertex0, vertex1)
                when {
                    vertex0 == vertex1 -> {
                        null
                    }
                    point != null -> {
                        val triangleList = listOf(
                            vertex0.point0,
                            vertex0.point1,
                            vertex1.point0,
                            vertex1.point1).distinct()

                        val newVertexPoints = triangleList - point
                        val matchingVertex = findMatchingVertex(newVertexPoints[0], newVertexPoints[1], vertices)

                        if (matchingVertex != null) {
                            Triangle(triangleList[0], triangleList[1], triangleList[2])
                        } else {
                            null
                        }
                    }
                    else -> {
                        null
                    }
                }
            }
        }
        return triangles
    }

    private fun findMatchingVertex(point0: DPoint, point1: DPoint, vertices: List<Vertex>): Vertex? {
        return vertices.find { vertex ->
            (vertex.point0 == point0 || vertex.point0 == point1) && (vertex.point1 == point0 || vertex.point1 == point1)
        }
    }
}