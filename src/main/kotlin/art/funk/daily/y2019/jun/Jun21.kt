package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.math.floor
import kotlin.math.max
import kotlin.math.roundToInt
import kotlin.random.Random

object Jun21 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun21Sketch::class.java)
    }
}

class Jun21Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val strokeWidth = 20.0
    val spacingVertical = 0
    val spacingHorizontal = 0
    val sideLength = 15 + strokeWidth.toInt()
    val hexHeight = Math.sqrt(3.0) * sideLength
    val hexWidth = 2 * sideLength


    val maxColumns = (myWidth / (hexWidth / Math.sqrt(3.0) + spacingHorizontal)).toInt()
    val maxRows = (myHeight / (hexHeight / Math.sqrt(1.0) + spacingVertical)).toInt() + 1

    val maxDistance = maxColumns + maxRows

    var columnIndex = 0

    val random = Random(320320)

    val colors = listOf(
        Color.decode("#FF402C"),
        Color.decode("#FF6B00"),
        Color.decode("#FEC82D"),
        Color.decode("#0DD078"),
        Color.decode("#00B2E5"),
        Color.decode("#DD2FB7")
    )

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        stroke(Color(10, 10, 10, 255))
        strokeWidth(strokeWidth)
    }

    override fun draw() {
        for (rowIndex in 0..maxRows) {
            val currentPoint = Point(columnIndex, rowIndex)
            var currentColor = Color(255, 255, 255, 255)

            val distance = hexDistance(currentPoint, Point(0, 0))
//            currentColor = ColorFactory.hsb(1.0 - (distance.toDouble() / maxDistance), 1.0, 1.0)

            val index = floor(6.6 * (distance.toDouble() / maxDistance)).roundToInt()
            currentColor = colors[min(5, index)]


//            currentColor = currentColor.interpolate(Color(255, 100, 200, 255), 1 - (distance.toDouble() / maxDistance))

//            for (seed in hexSeeds) {
//                val distance = hexDistance(currentPoint, seed.location)
//                if (distance < maxBlurDistance) {
//                    currentColor = currentColor.interpolate(seed.color, 1 - (distance.toDouble() / maxBlurDistance))
//                }
//            }




            stroke(currentColor)
            fill(Color(255, 255, 255, 255))
//            fill(Color(
//                currentColor.red * 2 / 3,
//                currentColor.green / 2,
//                currentColor.blue / 2,
//                currentColor.alpha
//            ))
//            fill(currentColor)

            drawHex(columnIndex, rowIndex)
        }

        columnIndex += 1
        if (columnIndex > maxColumns) {
            finished = true
        }

        if (finished) {
            save("outputs/" + javaClass.name)
        }
    }

    fun drawHex(column: Int, row: Int) {
        val yCenter: Double = if (column.rem(2) == 0) {
            hexHeight * row - (hexHeight / 2) + row * spacingVertical
        } else {
            hexHeight * row + row * spacingVertical
        }
        val xCenter = (hexWidth.toDouble() * column * 3 / 4) + column * spacingHorizontal

        beginShape()
        for (i in 0..5) {
            val point = hexCorner(DPoint(xCenter, yCenter), i)
            vertex(point.x.toFloat(), point.y.toFloat())
        }
        endShape(PConstants.CLOSE)
    }

    private fun hexCorner(center: DPoint, corner: Int): DPoint {
        val angleRadians = corner * Math.PI / 3
        return DPoint(
            center.x + sideLength * Math.cos(angleRadians),
            center.y + sideLength * Math.sin(angleRadians)
        )
    }

    // Hex distance using offset coordinates
    fun hexDistance(hex0: Point, hex1: Point): Int {
        val z0 = -(hex0.x + hex0.y)
        val z1 = -(hex1.x + hex1.y)
        return (Math.abs(hex0.x - hex1.x) + Math.abs(hex0.y - hex1.y) + Math.abs(z0 - z1)) / 2
    }
}