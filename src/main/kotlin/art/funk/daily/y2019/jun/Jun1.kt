package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.random.Random

object Jun1 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun1Sketch::class.java)
    }
}

class Jun1Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val center = DPoint(myWidth.toDouble() / 2, myHeight.toDouble() / 2)
    val radius = 300.0

    val pointCount = 100

    val random = Random(10)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeWidth(1.5)

        val thetaDelta = Math.PI * 2 / 3.4
        var theta = 0.0
        val points = IntArray(pointCount).map { i ->
            val point = DPoint(
                center.x + radius * Math.cos(theta),
                center.y + radius * Math.sin(theta))
            theta += thetaDelta
            point
        }.toMutableList()

//        points.shuffle(random)

        parabolicRotation(
            points,
            listOf { _ ->
                Color(100, 230, 160, 10)
            },
            pointCount)

    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    private fun parabolicRotation(points: List<DPoint>, colorFunctions: List<IterativeColorFunction>, lineCount: Int) {
        points.windowedCircular(3).forEachIndexed { index, (point0, point1, point2) ->
            parabolicDraw(
                point0,
                point2,
                point1,
                lineCount,
                colorFunctions[index % colorFunctions.size]
            )
        }
    }

    private fun parabolicDraw(
        startPoint: DPoint,
        endPoint: DPoint,
        pivotPoint: DPoint,
        lineCount: Int,
        colorFunction: IterativeColorFunction
    ) {
        // TODO Start the 'a' line at the startPoint and the 'b' line at the pivot point
        // Increment the 'a' point and the 'b' point on each iteration until conclusion
        for (i in 0..lineCount) {

            // TODO When turning this into a plugin-style piece change this to be a function such that
            // the user can either provide a delta from the line or simply a parametric function
            val aPoint = DPoint(
                startPoint.x + (pivotPoint.x - startPoint.x) * i / lineCount,
                startPoint.y + (pivotPoint.y - startPoint.y) * i / lineCount
            )

            val bPoint = DPoint(
                pivotPoint.x + (endPoint.x - pivotPoint.x) * i / lineCount,
                pivotPoint.y + (endPoint.y - pivotPoint.y) * i / lineCount
            )

            line(
                aPoint,
                bPoint,
                colorFunction(lineCount)
            )
        }
    }
}