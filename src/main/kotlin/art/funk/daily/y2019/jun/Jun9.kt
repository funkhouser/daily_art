package art.funk.daily.y2019.jun

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color

object Jun9 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(Jun9Sketch::class.java)
    }
}

class Jun9Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    var i = 0
    val totalRecursion = 0

    val spirographPoints = 5000

    val r0 = 8.0 * 3 / 2
    val bigR0 = 200.0 * 3 / 2
    val a0 = r0 * 3

    val r1 = 4.0 * 3 / 2
    val bigR1 = 200.0 * 3 / 2
    val a1 = r0 * 2

    val x0 = 0.0
    val y0 = 0.0

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
    }

    override fun draw() {
        RecursiveDraw.indexedRecurseAngleDraw(
            this,
            { index ->
                for (j in 0..spirographPoints) {
                    val t = j / 10.0
                    val point0 = DPoint(
                        (bigR0 - r0) * Math.cos(r0 / bigR0 * t) + a0 * Math.cos((1 - r0 / bigR0) * t) + x0,
                        (bigR0 - r0) * Math.sin(r0 / bigR0 * t) - a0 * Math.sin((1 - r0 / bigR0) * t) + y0)

                    val point1 = DPoint(
                        (bigR1 - r1) * Math.cos(r1 / bigR0 * t) + a1 * Math.cos((1 - r1 / bigR1) * t) + x0 + 10.0,
                        (bigR1 - r1) * Math.sin(r1 / bigR0 * t) - a1 * Math.sin((1 - r1 / bigR1) * t) + y0 + 10.0)

                    line(
                        point0,
                        point1,
                        Color(30, 30, 30, 30))
                }
            },
            0.4,
            0.98,
            i
        )

        i += 1


        if (i > totalRecursion) {
            super.draw()
            save("outputs/" + javaClass.name)
        }

    }
}