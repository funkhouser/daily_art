package art.funk.daily.y2019.may

import art.funk.daily.core.HighColor
import art.funk.daily.core.diamond.LinedDiamondCanvas
import processing.core.PApplet
import java.awt.Color
import kotlin.random.Random

object May18 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May18Sketch::class.java)
    }
}

class May18Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720
    val random = Random(10)
    val diamondSize = 10.0
    val borderWidth = 8.0
    val borderColor = HighColor(Color(10, 10, 10, 55))
    val borderStyle = LinedDiamondCanvas.BorderStyle(borderWidth, borderColor)
    val diamondCanvas =
        LinedDiamondCanvas(myHeight, myWidth, diamondSize, this::diamondDrawer, borderStyle)
    val diamondCanvas2 = LinedDiamondCanvas(
        myHeight,
        myWidth,
        diamondSize * 3 / 2,
        this::diamondDrawer,
        borderStyle
    )

    var previousRowStyles = mutableListOf<LinedDiamondCanvas.LinedDiamondStyle>()
    var currentRowStyles = mutableListOf<LinedDiamondCanvas.LinedDiamondStyle>()

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        diamondCanvas.doubleWithDraw = false
        diamondCanvas.drawCanvas(this)

//        diamondCanvas2.doubleWithDraw = false
//        diamondCanvas2.drawCanvas(this)
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    // The two left-hand edges will have connecting possibilities.
    fun diamondDrawer(diamond: LinedDiamondCanvas.LinedDiamond): LinedDiamondCanvas.LinedDiamondStyle {
        val diamondStyle: LinedDiamondCanvas.LinedDiamondStyle = LinedDiamondCanvas.LinedDiamondStyle(diamond)
        if (diamond.x == 0) {
            // Use some initial random states
        } else {
            if (diamond.y == 0) {
                previousRowStyles.clear()
                previousRowStyles.addAll(currentRowStyles)
                currentRowStyles.clear()
            }

            val isOffset = diamond.x % 2 == 1

            val topLeftNeighborStyle = if (isOffset) {
                previousRowStyles[diamond.y]
            } else {
                previousRowStyles.getOrNull(diamond.y - 1)
            }?.borderStyle ?: emptyBorderStyle()

            val bottomLeftNeighbor = if (isOffset) {
                previousRowStyles.getOrNull(diamond.y + 1)
            } else {
                previousRowStyles[diamond.y]
            }?.borderStyle ?: emptyBorderStyle()

            // TODO Use these two neighbors to drive this diamond
            val tunnelFromTop = topLeftNeighborStyle.topRight && topLeftNeighborStyle.bottomLeft && !topLeftNeighborStyle.bottomRight
            val tunnelFromBottom = bottomLeftNeighbor.topLeft && bottomLeftNeighbor.bottomRight && !bottomLeftNeighbor.topRight

            val transitionParameter: Double = random.nextDouble()
            val borderStyle: LinedDiamondCanvas.BorderStyle = when {
                tunnelFromTop && tunnelFromBottom  -> {
                    if (transitionParameter > 0.90) {
                        upTunnel()
                    } else if (transitionParameter > 0.8) {
                        downTunnel()
                    } else {
                        blockLeft()
                    }
                }
                tunnelFromTop -> {
                    if (transitionParameter > 0.9) {
                        blockTopLeft()
                    } else {
                        downTunnel()
                    }
                }
                tunnelFromBottom -> {
                    if (transitionParameter > 0.9) {
                        blockBottomLeft()
                    } else {
                        upTunnel()
                    }
                }
                else -> {
                    if (transitionParameter > 0.9) {
                        upTunnel()
                    } else if (transitionParameter > 0.8) {
                        downTunnel()
                    } else {
                        emptyBorderStyle()
                    }
                }
            }

            diamondStyle.borderStyle = borderStyle
        }

        currentRowStyles.add(diamondStyle)
        return diamondStyle
    }

    fun emptyBorderStyle(): LinedDiamondCanvas.BorderStyle {
        val style = LinedDiamondCanvas.BorderStyle(borderWidth, borderColor)
        style.topLeft = false
        style.topRight = false
        style.bottomLeft = false
        style.bottomRight = false

        return style
    }

    fun upTunnel(): LinedDiamondCanvas.BorderStyle {
        val style = LinedDiamondCanvas.BorderStyle(borderWidth, borderColor)
        style.bottomLeft = false
        style.topRight = false
        return style
    }

    fun downTunnel(): LinedDiamondCanvas.BorderStyle {
        val style = LinedDiamondCanvas.BorderStyle(borderWidth, borderColor)
        style.topLeft = false
        style.bottomRight = false
        return style
    }

    fun blockLeft(): LinedDiamondCanvas.BorderStyle {
        val style = LinedDiamondCanvas.BorderStyle(borderWidth, borderColor)
        style.topRight = false
        style.bottomRight = false
        return style
    }

    fun blockTopLeft(): LinedDiamondCanvas.BorderStyle {
        val style = LinedDiamondCanvas.BorderStyle(borderWidth, borderColor)
        style.topLeft = false
        style.topRight = false
        style.bottomRight = false
        return style
    }

    fun blockBottomLeft(): LinedDiamondCanvas.BorderStyle {
        val style = LinedDiamondCanvas.BorderStyle(borderWidth, borderColor)
        style.bottomLeft = false
        style.topRight = false
        style.bottomRight = false
        return style
    }
}