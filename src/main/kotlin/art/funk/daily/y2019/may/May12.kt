package art.funk.daily.y2019.may

import art.funk.daily.core.DPoint
import art.funk.daily.core.background
import art.funk.daily.core.line
import gifAnimation.GifMaker
import processing.core.PApplet
import java.awt.Color

object May12 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May12Sketch::class.java)
    }
}

class May12Sketch : PApplet() {

    val myHeight = 1080
    val myWidth = 1080

    val halfHeight = myHeight / 2
    val halfWidth = myWidth / 2

    var iteration = 0
    val totalIterations = 1500

    val shapes = mutableListOf<Shape>()

    val gifExport = GifMaker(this, javaClass.name + ".gif")

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        super.setup()
        background(Color(255, 255, 255))
        strokeWeight(3F)


        // 12 Frames per second
        gifExport.setDelay(1000 / 12)
        gifExport.setRepeat(0)
        gifExport.setTransparent(255)

        shapes.add(Shape(
            listOf(
                DPoint(10.0, 10.0),
                DPoint(25.0, 20.0)
            ),
            this::spinCollapse,
            { i: Int -> Color(10, 10, 10) }
        ))
    }


    override fun draw() {
        if (iteration % 10 == 0) {
            gifExport.addFrame()
        }
        if (iteration >= totalIterations) {
            finished = true
            save("outputs/" + javaClass.name)
            gifExport.finish()
        } else {
            drawShapes()
            iteration += 1
        }
    }

    private fun drawShapes() {
        shapes.forEach { shape ->
            val nextPoints = shape.pointUpdateFunction(iteration, shape.points)
            val color = shape.colorFunction(iteration)
            nextPoints.windowed(2, 1).forEach { (point0, point1) ->
                line(
                    point0.x,
                    point0.y,
                    point1.x,
                    point1.y,
                    color
                )
            }

            shape.points = nextPoints
        }
    }

    private val radiansPerIteration = 0.008
    private fun spinCollapse(i: Int, currentPoints: List<DPoint>): List<DPoint> {
        val thetaDelta = Math.PI * 2  * radiansPerIteration

        return currentPoints.map { (x, y) ->
            val currentAngle = when {
                y - halfHeight > 0 && x - halfWidth > 0 -> Math.atan2(y - halfHeight, x - halfWidth)
                y - halfHeight < 0 && x - halfWidth < 0 -> Math.atan2(y - halfHeight, x - halfWidth)
                y - halfHeight > 0 && x - halfWidth < 0 -> Math.atan2(halfWidth - x, y - halfHeight) + Math.PI / 2
                else -> Math.atan2(x - halfWidth, halfHeight - y) + 3 * Math.PI / 2
            }

            val radius = Math.sqrt((y - halfHeight) * (y - halfHeight) + (x - halfWidth) * (x - halfWidth))

            val newX = halfWidth + Math.cos(currentAngle + thetaDelta) * (radius - (radius / totalIterations))
            val newY = halfHeight + Math.sin(currentAngle + thetaDelta) * (radius - (radius / totalIterations))

            DPoint(newX, newY)
        }
    }

}

data class Shape(
    var points: List<DPoint>,
    var pointUpdateFunction: (Int, List<DPoint>) -> List<DPoint>,
    var colorFunction: (Int) -> Color
)