package art.funk.daily.y2019.may

import art.funk.daily.core.diamond.SpacedDiamondCanvas
import art.funk.daily.core.HighColor
import processing.core.PApplet
import java.awt.Color
import kotlin.random.Random

object May11 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May11Sketch::class.java)
    }
}

class May11Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val accentColor = HighColor(Color(175, 75, 140, 200))
    val accentColor2 = HighColor(Color(75, 175, 140, 200))

    val diamondSize = 20.0
    val diamondCanvas =
        SpacedDiamondCanvas(myHeight, myWidth, diamondSize, this::diamondDraw)
    val random = Random(5)


    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255).rgb)
        diamondCanvas.drawCanvas(this)
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    private fun diamondDraw(diamond: SpacedDiamondCanvas.Diamond): SpacedDiamondCanvas.DiamondStyle {
        val style = SpacedDiamondCanvas.DiamondStyle(diamond)
        if (random.nextInt(25) == 24) {
            style.color = accentColor
            style.borderStyle = SpacedDiamondCanvas.BorderStyle(2, HighColor(Color(180, 180, 180)))
        } else if (random.nextInt(35) == 24) {
            style.color = accentColor2
            style.borderStyle = SpacedDiamondCanvas.BorderStyle(2, HighColor(Color(180, 180, 180)))
        } else if (random.nextInt(2) == 0) {
            style.color = HighColor(Color(255, 255, 255, 220))
            style.borderStyle = SpacedDiamondCanvas.BorderStyle(2, HighColor(Color(200, 200, 200)))
        } else {
            style.color = HighColor(Color(255, 255, 255, 220))
            style.borderStyle = SpacedDiamondCanvas.BorderStyle(2, HighColor(Color(200, 200, 200, 0)))
        }
        return style
    }
}