package art.funk.daily.y2019.may

import art.funk.daily.core.background
import art.funk.daily.core.interpolate
import art.funk.daily.core.line
import art.funk.daily.core.strokeWidth
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.math.roundToInt

object May25 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May25Sketch::class.java)
    }
}

class May25Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val lineWidth = 15.0
    val lineSpacing = 25.0
    val lineCount = (Math.max(myWidth, myHeight) / (lineWidth + lineSpacing)).roundToInt() * 2 + 2

    val color00 = Color(85, 240, 255, 140)
    val color01 = Color(100, 100, 240, 140)

    val color10 = Color(240, 255, 200, 200)
    val color11 = Color(160, 160, 100, 100)


    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeWidth(lineWidth)

        for (i in 0..lineCount) {
            line(
                0.0,
                i * (lineWidth + lineSpacing),
                i * (lineWidth + lineSpacing),
                0.0,
                color00.interpolate(color01, (i.toDouble() / lineCount))
            )


            line(
                0.0,
                i * (lineWidth + lineSpacing) - myHeight,
                myWidth * 2 - i * (lineWidth + lineSpacing),
                myHeight.toDouble(),
                color10.interpolate(color11, (i.toDouble() / lineCount))
            )
        }
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}