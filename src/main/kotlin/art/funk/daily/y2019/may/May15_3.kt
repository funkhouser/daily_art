package art.funk.daily.y2019.may

import art.funk.daily.core.diamond.DiamondCanvas
import art.funk.daily.core.diamond.DiamondDrawer
import art.funk.daily.core.HighColor
import art.funk.daily.core.background
import processing.core.PApplet
import java.awt.Color
import kotlin.random.Random

object May15_3 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May15Sketch_3::class.java)
    }
}

class May15Sketch_3 : PApplet() {

    val myHeight = 720
    val myWidth = 720
    val random = Random(10)

    // First daily art with state machines: only 3 states and they drive the entire diamond
    // More interesting would be creating diamonds that "connect" border paths etc.
    enum class DiamondState(val diamondStyler: (diamond: DiamondCanvas.Diamond) -> DiamondCanvas.DiamondStyle) {
        BLUE({ diamond ->
            val style = DiamondCanvas.DiamondStyle(diamond)
            style.borderStyle = DiamondCanvas.BorderStyle(5, HighColor(Color(255, 255, 255, 255)))
            style.color = HighColor(Color(100, 120, 255, 255))
            style
        }),
        GREEN({ diamond ->
            val style = DiamondCanvas.DiamondStyle(diamond)
            style.borderStyle = DiamondCanvas.BorderStyle(5, HighColor(Color(255, 255, 255, 255)))
            style.color = HighColor(Color(100, 255, 120, 255))
            style
        }),
        BLANK({ diamond ->
            val style = DiamondCanvas.DiamondStyle(diamond)
            style.borderStyle = DiamondCanvas.BorderStyle(5, HighColor(Color(255, 255, 255, 255)))
            style.color = HighColor(Color(255, 255, 225, 255))
            style
        });

    }

    private var previousState: DiamondState = DiamondState.BLANK
    private val diamondDrawer: DiamondDrawer = { diamond: DiamondCanvas.Diamond ->
        val nextStepTrigger = random.nextDouble()
        val nextState: DiamondState = when(previousState) {
            DiamondState.BLUE -> {
                when {
                    nextStepTrigger <= 0.95 -> DiamondState.BLUE
                    nextStepTrigger <= 0.98 -> DiamondState.GREEN
                    else -> DiamondState.BLANK
                }
            }
            DiamondState.GREEN -> {
                when {
                    nextStepTrigger <= 0.95 -> DiamondState.GREEN
                    nextStepTrigger <= 0.98 -> DiamondState.BLANK
                    else -> DiamondState.BLUE
                }
            }
            DiamondState.BLANK -> {
                when {
                    nextStepTrigger <= 0.95 -> DiamondState.BLANK
                    nextStepTrigger <= 0.98 -> DiamondState.BLUE
                    else -> DiamondState.GREEN
                }
            }
        }

        previousState = nextState
        nextState.diamondStyler(diamond)
    }

    val diamondCanvas = DiamondCanvas(myHeight, myWidth, 10.0, diamondDrawer)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        diamondCanvas.drawCanvas(this)
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }


}