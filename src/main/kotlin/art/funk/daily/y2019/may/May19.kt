package art.funk.daily.y2019.may

import art.funk.daily.core.diamond.DiamondCanvas
import art.funk.daily.core.HighColor
import processing.core.PApplet
import java.awt.Color

object May19 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May19Sketch::class.java)
    }
}

class May19Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720
    val size = 20.0

    val minBorderSize = 1.0
    val maxBorderSize = 15.0

    val outsideXColor = HighColor(Color(255, 240, 100, 200))
    val insideXColor = HighColor(Color(200, 150, 255, 200))
    val borderColor = HighColor(Color(10, 10, 10, 255))


    val diamondCanvas = DiamondCanvas(myHeight, myWidth, size, this::diamondDraw)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        diamondCanvas.rounding = 0.0
        diamondCanvas.drawCanvas(this)
    }

    private fun diamondDraw(diamond: DiamondCanvas.Diamond): DiamondCanvas.DiamondStyle {
        val style = DiamondCanvas.DiamondStyle(diamond)
            // TODO Do something with quadrants sometime later
        if (diamond.x < diamondCanvas.xDiamondCount / 2 && diamond.y < diamondCanvas.yDiamondCount) {

        } else if (diamond.x < diamondCanvas.xDiamondCount / 2) {

        } else if (diamond.y < diamondCanvas.yDiamondCount) {

        } else {

        }

        val percentInside = Math.abs(diamond.x - diamondCanvas.xDiamondCount / 2.0) / (diamondCanvas.xDiamondCount / 2.0)
        style.color = outsideXColor.interpolate(insideXColor, percentInside)

        val percentBottom = diamond.y.toDouble() / diamondCanvas.yDiamondCount
        val borderWidth = minBorderSize * (1 - percentBottom) + maxBorderSize * percentBottom
        style.borderStyle = DiamondCanvas.BorderStyle(
            borderWidth.toInt(),
            borderColor
        )

        return style
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}