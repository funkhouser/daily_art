package art.funk.daily.y2019.may

import art.funk.daily.core.*
import art.funk.daily.core.applet.rotate
import gifAnimation.GifMaker
import processing.core.PApplet
import java.awt.Color
import java.util.stream.IntStream
import kotlin.random.Random

object May28_2 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May28_2Sketch::class.java)
    }
}

class May28_2Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    var j = 0
    val drawIterations = 200
    val stickCount = 15

    val pivotPoint = DPoint(700.0, 700.0)

    val random = Random(2)

    var currentAngle = Math.PI / 50
    var increaseAngle = Math.PI / 100

    val colors = IntArray(stickCount + 1).map {
        Color(
            10 + random.nextInt(100),
            100 + random.nextInt(100),
            200 + random.nextInt(50),
            180)
    }

    val gifExport = GifMaker(this, javaClass.name + ".gif")

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        strokeWidth(9.0)
        stroke(Color(60, 60, 60, 5))
        background(Color(255, 255, 255, 255))

        gifExport.setDelay(1000 / 12)
        gifExport.setRepeat(0)
    }

    override fun draw() {
        gifExport.addFrame()
        fill(Color(255, 255, 255, 255))
        rectViaSize(
            DPoint(0.0, 0.0),
            720.0,
            720.0,
            0.0)

        j += 1
        drawPopsicles()

        if (j > drawIterations) {
            save("outputs/" + javaClass.name)
            finished = true
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.finish()
        }
    }

    private fun drawPopsicles() {
        rotate(Math.PI, pivotPoint)
        RecursiveDraw.recurseAngleDraw(
            this,
            { colorIndex ->
                val fillColor = colors[colorIndex]

                fill(fillColor)
                rectViaSize(
                    DPoint(720.0, 720.0),
                    710.0,
                    70.0,
                    50.0
                )
            },
            currentAngle + increaseAngle * j,
            1.0,
            stickCount,
            pivotPoint
        )
    }
}