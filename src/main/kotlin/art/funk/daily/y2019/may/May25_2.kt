package art.funk.daily.y2019.may

import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color
import kotlin.math.roundToInt

object May25_2 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May25_2Sketch::class.java)
    }
}

class May25_2Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val lineWidth = 1.0
    val lineSpacing = 25.0
    val lineCount = (Math.max(myWidth, myHeight) / (lineWidth + lineSpacing)).roundToInt() * 2 + 2

    val color00 = Color(85, 240, 255, 100)
    val color01 = Color(100, 100, 240, 100)

    val color10 = Color(240, 255, 200, 200)
    val color11 = Color(160, 160, 100, 100)


    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeWidth(lineWidth)

        // TODO Create a decomposed parabolic (or maybe even generic function line?)
        // Essentially, drive the curve by a function and allow it to be composed of individual line segments

        // For now: see about creating such a parabolic using 3 points

//        for (i in 0..lineCount) {
//            // Creates a parabolic shading section
//            line(
//                0.0,
//                i * (lineWidth + lineSpacing) - myHeight,
//                i * (lineWidth + lineSpacing),
//                myHeight.toDouble(),
//                color01.interpolate(color11, (i.toDouble() / lineCount))
//            )
//        }

        parabolicDraw(
            startPoint = DPoint(0.0, myHeight.toDouble()),
            endPoint = DPoint(myWidth.toDouble(), 0.0),
            pivotPoint = DPoint(myWidth.toDouble(), myHeight.toDouble()),
            lineCount = 80
        ) { i ->
            Color(255, 255, 200, 200)
        }

        parabolicDraw(
            DPoint(myWidth.toDouble(), 0.0),
            DPoint(0.0, myHeight.toDouble()),
            DPoint(0.0, 0.0),
            80
        ) { i ->
            Color(200, 200, 255, 100)
        }

        parabolicDraw(
            startPoint = DPoint(0.0, 0.0),
            endPoint = DPoint(myWidth.toDouble(), myHeight.toDouble()),
            pivotPoint = DPoint(0.0, myHeight.toDouble()),
            lineCount = 80
        ) { i ->
            Color(255, 200, 200, 100)
        }



        parabolicDraw(
            startPoint = DPoint(myWidth.toDouble(), myHeight.toDouble()),
            endPoint = DPoint(0.0, 0.0),
            pivotPoint = DPoint(myWidth.toDouble(), 0.0),
            lineCount = 80
        ) { i ->
            Color(200, 255, 200, 100)
        }
    }


    private fun parabolicDraw(
            startPoint: DPoint,
            endPoint: DPoint,
            pivotPoint: DPoint,
            lineCount: Int,
            colorFunction: IterativeColorFunction) {

        // TODO Start the 'a' line at the startPoint and the 'b' line at the pivot point
        // Increment the 'a' point and the 'b' point on each iteration until conclusion
        for(i in 0..lineCount) {

            // TODO When turning this into a plugin-style piece change this to be a function such that
            // the user can either provide a delta from the line or simply a parametric function
            val aPoint = DPoint(
                startPoint.x + (pivotPoint.x - startPoint.x) * i / lineCount,
                startPoint.y + (pivotPoint.y - startPoint.y) * i / lineCount
            )

            val bPoint = DPoint(
                pivotPoint.x + (endPoint.x - pivotPoint.x) * i / lineCount,
                pivotPoint.y + (endPoint.y - pivotPoint.y) * i / lineCount

            )

            line(
                aPoint,
                bPoint,
                colorFunction(lineCount)
            )
        }
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}