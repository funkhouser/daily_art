package art.funk.daily.y2019.may

import art.funk.daily.core.HighColor
import art.funk.daily.core.background
import art.funk.daily.core.diamond.PrestyledLinedDiamondCanvas
import art.funk.daily.core.stroke
import gifAnimation.GifMaker
import processing.core.PApplet
import java.awt.Color
import kotlin.random.Random

object May29 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May29Sketch::class.java)
    }
}

class May29Sketch : PApplet() {

    val myHeight = 400
    val myWidth = 400

    val diamondSize = 20.0
    val strokeWidth = 5.0
    val defaultBorderStyle =
        PrestyledLinedDiamondCanvas.PrestyledBorderStyle(strokeWidth, HighColor(Color(255, 255, 255, 0)))

    val diamondCanvas = PrestyledLinedDiamondCanvas(myHeight, myWidth, diamondSize, defaultBorderStyle)

    val diamondQueue = mutableListOf<PrestyledLinedDiamondCanvas.PrestyledLinedDiamondStyle>()
    val seenDiamonds = mutableListOf<PrestyledLinedDiamondCanvas.PrestyledLinedDiamondStyle>()

    val iterations = 40
    var i = 0
    val random = Random(8)
    val gifExport = GifMaker(this, "outputs/" + javaClass.name + ".gif")


    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
        diamondCanvas.doubleWithDraw = true
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        stroke(Color(255, 255, 255, 255))
        val diamondStyles = diamondCanvas.diamondStyles

        diamondQueue.add(diamondStyles[diamondCanvas.xDiamondCount / 2][diamondCanvas.yDiamondCount / 2])
        diamondStyles.forEach { column ->
            column.forEach { diamondStyle ->
                diamondStyle.color = HighColor(Color(255, 255, 255, 255))
            }
        }

        gifExport.setDelay(1000 / 12)
        gifExport.setRepeat(0)
    }

    override fun draw() {
        gifExport.addFrame()
        background(Color(255, 255, 255, 255))
        i += 1
        if (i >= iterations) {
            super.draw()
            save("outputs/" + javaClass.name)
            diamondCanvas.drawCanvas(this)
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.addFrame()
            gifExport.finish()
            finished = true
        }

        val diamondsToAdd = mutableListOf<PrestyledLinedDiamondCanvas.PrestyledLinedDiamondStyle>()
        diamondQueue.forEach { style ->
            val neighbors = getNeighborDiamonds(style)

            style.color = HighColor(Color(
                Math.max(0, 150 - 5 * i),
                Math.max(0, 255 - 5 * i),
                Math.max(0, 180 - 5 * i),
                255))

            seenDiamonds.add(style)
            neighbors.getAdjacent()
                .filter { !seenDiamonds.contains(it) }
                .forEach { neighborStyle ->
                    if (random.nextDouble() > 0.1) {
                        diamondsToAdd.add(neighborStyle)
                    } else {
                        seenDiamonds.add(neighborStyle)
                    }
                }
        }

        diamondQueue.clear()
        diamondQueue.addAll(diamondsToAdd)
        diamondCanvas.drawCanvas(this)
    }

    // TODO Don't know whether I care to extract this
    fun getNeighborDiamonds(style: PrestyledLinedDiamondCanvas.PrestyledLinedDiamondStyle): NeighborDiamonds {
        val styles = diamondCanvas.diamondStyles

        val x = style.diamond.x
        val y = style.diamond.y

        val isOffset = style.diamond.x % 2 == 1

        val topRight = if (isOffset) {
            styles.getOrNull(x + 1)?.getOrNull(y)
        } else {
            styles.getOrNull(x + 1)?.getOrNull(y - 1)
        }

        val topLeft = if (isOffset) {
            styles.getOrNull(x - 1)?.getOrNull(y)
        } else {
            styles.getOrNull(x - 1)?.getOrNull(y - 1)
        }

        val bottomRight = if (isOffset) {
            styles.getOrNull(x + 1)?.getOrNull(y + 1)
        } else {
            styles.getOrNull(x + 1)?.getOrNull(y)
        }

        val bottomLeft = if (isOffset) {
            styles.getOrNull(x - 1)?.getOrNull(y + 1)
        } else {
            styles.getOrNull(x - 1)?.getOrNull(y)
        }

        return NeighborDiamonds(
            style,
            topLeft,
            topRight,
            bottomLeft,
            bottomRight
        )
    }

    data class NeighborDiamonds(
        val center: PrestyledLinedDiamondCanvas.PrestyledLinedDiamondStyle,
        val topLeft: PrestyledLinedDiamondCanvas.PrestyledLinedDiamondStyle?,
        val topRight: PrestyledLinedDiamondCanvas.PrestyledLinedDiamondStyle?,
        val bottomLeft: PrestyledLinedDiamondCanvas.PrestyledLinedDiamondStyle?,
        val bottomRight: PrestyledLinedDiamondCanvas.PrestyledLinedDiamondStyle?
    ) {
        fun getAdjacent(): List<PrestyledLinedDiamondCanvas.PrestyledLinedDiamondStyle> {
            return listOfNotNull(
                topLeft,
                topRight,
                bottomLeft,
                bottomRight
            )
        }
    }
}