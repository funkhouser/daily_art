package art.funk.daily.y2019.may

import art.funk.daily.core.diamond.SpacedDiamondCanvas
import art.funk.daily.core.HighColor
import art.funk.daily.core.background
import processing.core.PApplet
import java.awt.Color
import kotlin.random.Random

object May14_2 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May14Sketch_2::class.java)
    }
}

/**
 * Uses oversized borders to create a cool patterned overlapping effect
 */
class May14Sketch_2 : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val accentColor = HighColor(Color(75, 75, 140, 200))
    val accentColor2 = HighColor(Color(75, 175, 240, 200))

    val diamondSize = 20.0
    val diamondCanvas =
        SpacedDiamondCanvas(myHeight, myWidth, diamondSize, this::diamondDraw)
    val random = Random(5)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255))
        diamondCanvas.drawCanvas(this)
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    private fun diamondDraw(diamond: SpacedDiamondCanvas.Diamond): SpacedDiamondCanvas.DiamondStyle {
        val style = SpacedDiamondCanvas.DiamondStyle(diamond)
        if (random.nextInt(5) == 4) {
            style.color = accentColor
            style.borderStyle = SpacedDiamondCanvas.BorderStyle(50, HighColor(Color(80, 80, 80, 100)))
        } else if (random.nextInt(5) == 4) {
            style.color = accentColor2
            style.borderStyle = SpacedDiamondCanvas.BorderStyle(50, HighColor(Color(80, 80, 80, 100)))
        } else if (random.nextInt(2) == 0) {
            style.color = HighColor(Color(155, 155, 155, 120))
            style.borderStyle = SpacedDiamondCanvas.BorderStyle(50, HighColor(Color(100, 100, 100, 100)))
        } else {
            style.color = HighColor(Color(155, 155, 155, 20))
            style.borderStyle = SpacedDiamondCanvas.BorderStyle(50, HighColor(Color(100, 100, 100, 100)))
        }
        return style
    }
}