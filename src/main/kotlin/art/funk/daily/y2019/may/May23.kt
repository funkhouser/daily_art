package art.funk.daily.y2019.may

import art.funk.daily.core.*
import art.funk.daily.plugins.PrimitivePlugin
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.random.Random

object May23 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May23Sketch::class.java)
    }
}

class May23Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val seedCount = 10

    val spacingVertical = 0
    val spacingHorizontal = 0
    val sideLength = 15
    val strokeWidth = 0.0
    val hexHeight = Math.sqrt(3.0) * sideLength
    val hexWidth = 2 * sideLength


    val maxColumns = (myWidth / (hexWidth / Math.sqrt(3.0) + spacingHorizontal)).toInt()
    val maxRows = (myHeight / (hexHeight / Math.sqrt(1.0) + spacingVertical)).toInt() + 1

    var columnIndex = 0

    val maxBlurDistance = 10

    val random = Random(10)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(55, 55, 55, 255))

    }

    override fun draw() {

        for (rowIndex in 0..maxRows) {
            drawHexTriangles(columnIndex, rowIndex)
        }

        columnIndex += 1
        if (columnIndex > maxColumns) {
            finished = true
        }

        if (finished) {
            save("outputs/" + javaClass.name)
        }
    }

    private fun drawHexTriangles(column: Int, row: Int) {
        val color = getColor(column, row)
        val yCenter: Double = if (column.rem(2) == 0) {
            hexHeight * row - (hexHeight / 2) + row * spacingVertical
        } else {
            hexHeight * row + row * spacingVertical
        }
        val xCenter = hexWidth.toDouble() * column * 3 / 4 + column * spacingHorizontal

        for (i in 0..5) {
            val triColor = triColor(color, i)
            fill(triColor)
            stroke(
                Color(
                    triColor.red,
                    triColor.green,
                    triColor.blue,
                    triColor.alpha / 2
                )
            )
            beginShape()
            val center = DPoint(xCenter, yCenter)
            val point0 = hexCorner(center, i)
            val point1 = hexCorner(center, i + 1)
            vertex(center)
            vertex(point0)
            vertex(point1)
            endShape(PConstants.CLOSE)
        }
    }

    private fun triColor(color: Color, index: Int): Color {
        val (h, s, b) = Color.RGBtoHSB(color.red, color.green, color.blue, null)
        val newSat = Math.min(1.0, (s + 0.05 * index)).toFloat()
        val triColor = Color.getHSBColor(h, newSat, b)
        return triColor
    }

    private fun hexCorner(center: DPoint, corner: Int): DPoint {
        val angleRadians = corner * Math.PI / 3
        return DPoint(
            center.x + sideLength * Math.cos(angleRadians),
            center.y + sideLength * Math.sin(angleRadians)
        )
    }

    private fun getColor(column: Int, row: Int): Color {
        val percent = random.nextDouble()
        return if (percent > 0.8) {
            Color(
                200 + random.nextInt(40),
                50 + random.nextInt(40),
                100 + random.nextInt(40),
                255
            )
        } else {
            Color(
                55,
                55,
                55,
                255
            )
        }

    }
}