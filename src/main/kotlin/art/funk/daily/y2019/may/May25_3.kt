package art.funk.daily.y2019.may

import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color
import kotlin.math.roundToInt

object May25_3 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May25_3Sketch::class.java)
    }
}

class May25_3Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val lineWidth = 2.0
    val lineSpacing = 25.0
    val lineCount = (Math.max(myWidth, myHeight) / (lineWidth + lineSpacing)).roundToInt() * 2 + 2

    val color00 = Color(85, 240, 255, 100)
    val color01 = Color(100, 100, 240, 100)

    val color10 = Color(240, 255, 200, 200)
    val color11 = Color(160, 160, 100, 100)


    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeWidth(lineWidth)

        val point0 = DPoint(0.0, 0.0)
        val point1 = DPoint(myWidth.toDouble(), 0.0)
        val point2 = DPoint(myWidth.toDouble(), myHeight.toDouble())
        val point3 = DPoint(0.0, myHeight.toDouble())
        val colorFunctions = listOf(
            { i: Int -> Color(255, 255, 200, 200) },
            { i: Int -> Color(200, 200, 255, 100) },
            { i: Int -> Color(255, 200, 200, 100) },
            { i: Int -> Color(200, 255, 200, 100) })

        parabolicRotation(
            listOf(point0, point1, point2, point3),
            colorFunctions,
            80
        )

        val inset0 = 60.0
        val newPoints = listOf(
            DPoint(myWidth / 2.0, inset0),
            DPoint(myWidth - inset0, myHeight / 2.0),
            DPoint(myWidth / 2.0, myHeight - inset0),
            DPoint(inset0, myHeight / 2.0))

        parabolicRotation(
            newPoints,
            colorFunctions,
            80
        )

        val inset1 = 180.0
        val tertiaryPoints = listOf(
            DPoint(myWidth - inset1, inset1),
            DPoint(myWidth - inset1, myHeight - inset1),
            DPoint(inset1, myHeight - inset1),
            DPoint(inset1, inset1))
        parabolicRotation(
            tertiaryPoints,
            colorFunctions,
            80
        )
        
        val inset2 = 212.0
        val quadPoints = listOf(

            DPoint(myWidth - inset2, myHeight / 2.0),
            DPoint(myWidth / 2.0, myHeight - inset2),
            DPoint(inset2, myHeight / 2.0),
            DPoint(myWidth / 2.0, inset2))

        parabolicRotation(
            quadPoints,
            colorFunctions,
            80
        )
    }

    private fun parabolicRotation(points: List<DPoint>, colorFunctions: List<IterativeColorFunction>, lineCount: Int) {
        points.windowedCircular(3).forEachIndexed { index, (point0, point1, point2) ->
            parabolicDraw(
                point0,
                point2,
                point1,
                lineCount,
                colorFunctions[index % colorFunctions.size]
            )
        }
    }

    private fun parabolicDraw(
        startPoint: DPoint,
        endPoint: DPoint,
        pivotPoint: DPoint,
        lineCount: Int,
        colorFunction: IterativeColorFunction
    ) {
        // TODO Start the 'a' line at the startPoint and the 'b' line at the pivot point
        // Increment the 'a' point and the 'b' point on each iteration until conclusion
        for (i in 0..lineCount) {

            // TODO When turning this into a plugin-style piece change this to be a function such that
            // the user can either provide a delta from the line or simply a parametric function
            val aPoint = DPoint(
                startPoint.x + (pivotPoint.x - startPoint.x) * i / lineCount,
                startPoint.y + (pivotPoint.y - startPoint.y) * i / lineCount
            )

            val bPoint = DPoint(
                pivotPoint.x + (endPoint.x - pivotPoint.x) * i / lineCount,
                pivotPoint.y + (endPoint.y - pivotPoint.y) * i / lineCount
            )

            line(
                aPoint,
                bPoint,
                colorFunction(lineCount)
            )
        }
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}