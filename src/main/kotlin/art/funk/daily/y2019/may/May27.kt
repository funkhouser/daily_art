package art.funk.daily.y2019.may

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color

object May27 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May27Sketch::class.java)
    }
}

class May27Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    var i = 0

    val iterations = 800

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }


    val color0 = Color(200, 220, 255, 150)
    val color1 = Color(200, 255, 200, 150)

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeWidth(20.0)
        stroke(color0)
        fill(Color(255, 255, 255, 80))

    }

    override fun draw() {

        if (i >= iterations) {
            finished = true
            save("outputs/" + javaClass.name)
        }

        i += 1
        RecursiveDraw.indexedRecurseAngleDraw(
            this,
            { _ ->
                stroke(color0.interpolate(color1, i.toDouble() / iterations))
                rectViaSize(
                    DPoint(myWidth / 2.0, myHeight / 2.0),
                    250.0,
                    200.0,
                    30.0
                )
            },
            Math.PI / 30,
            0.99,
            i
        )
    }


}