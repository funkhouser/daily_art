package art.funk.daily.y2019.may

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants

object May26 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May26Sketch::class.java)
    }
}

class May26Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val iterations = 80
    val angle = Math.PI / 10
    val shrink = 0.98
    var i = 0

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        strokeWidth(2.0)

//        RecursiveDraw.recurseAngleDraw(
//            this,
//            this::drawShape,
//            angle,
//            shrink,
//            iterations
//        )
    }

    override fun draw() {
//        super.draw()
        RecursiveDraw.indexedRecurseAngleDraw(
            this,
            this::drawShape,
            angle,
            shrink,
            i
        )
        i += 1

        if (i >= iterations) {
            finished = true
            save("outputs/" + javaClass.name)
        }
    }

    private fun drawShape(i: Int) {
        line(100.0, 100.0,
            130.0, 700.0)

        line(200.0, 100.0,
            130.0, 700.0)
    }
}