package art.funk.daily.y2019.may

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color

object May21 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May21Sketch::class.java)
    }
}

class May21Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val spacingVertical = 0
    val spacingHorizontal = 0
    val sideLength = 20
    val hexHeight = Math.sqrt(3.0) * sideLength
    val hexWidth = 2 * sideLength

    val maxColumns = (myWidth / (hexWidth / Math.sqrt(3.0) + spacingHorizontal)).toInt()
    val maxRows = (myHeight / (hexHeight / Math.sqrt(1.0) + spacingVertical)).toInt() + 1

    var columnIndex = 0

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        strokeWidth(10.0)
    }

    fun drawHex(column: Int, row: Int) {
        val yCenter: Double = if (column.rem(2) == 0) {
            hexHeight * row - (hexHeight / 2) + row * spacingVertical
        } else {
            hexHeight * row + row * spacingVertical
        }
        val xCenter = (hexWidth.toDouble() * column * 3 / 4) + column * spacingHorizontal

        beginShape()
        for (i in 0..5) {
            val point = hexCorner(DPoint(xCenter, yCenter), i)
            vertex(point.x.toFloat(), point.y.toFloat())
        }
        endShape(PConstants.CLOSE)
    }

    private fun hexCorner(center: DPoint, corner: Int): DPoint {
        val angleRadians = corner * Math.PI / 3
        return DPoint(
            center.x + sideLength * Math.cos(angleRadians),
            center.y + sideLength * Math.sin(angleRadians)
        )
    }

    val initialColor = Color(245, 245, 245, 255)
    val finalColor = Color(100, 210, 255, 255)

    val minStroke = 1.0
    val maxStroke = 22.0

    override fun draw() {

        fill(Color(250, 250, 250, 155))

        stroke(initialColor.interpolate(finalColor, columnIndex.toDouble() / maxColumns))

        for (rowIndex in 0..maxRows) {
            val rowPercent = Math.abs((rowIndex.toDouble() / maxRows) * 2 - 0.5)
            strokeWidth(minStroke * (1 - rowPercent) + maxStroke * rowPercent)
            drawHex(columnIndex, rowIndex)
        }

        columnIndex += 1
        if (columnIndex > maxColumns) {
            finished = true
        }

        if (finished) {
            save("outputs/" + javaClass.name)
        }
    }
}