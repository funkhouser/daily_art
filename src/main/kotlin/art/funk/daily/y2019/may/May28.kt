package art.funk.daily.y2019.may

import art.funk.daily.core.*
import art.funk.daily.core.applet.rotate
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.random.Random

object May28 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May28Sketch::class.java)
    }
}

class May28Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    var i = 0
    val iterations = 18

    val pivotPoint = DPoint(700.0, 700.0)

    val random = Random(2)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        strokeWidth(7.0)
        stroke(Color(50, 50, 50, 255))
        background(Color(255, 255, 255, 255))
    }

    override fun draw() {
        if (i >= iterations) {
            finished = true
            save("outputs/" + javaClass.name)
        }

        rotate(Math.PI, pivotPoint)

        val fillColor = Color(
            10 + random.nextInt(100),
            100 + random.nextInt(100),
            200 + random.nextInt(50),
            180)

        fill(fillColor)

        RecursiveDraw.indexedRecurseAngleDraw(
            this,
            { _ ->
                rectViaSize(
                    DPoint(100.0, 0.0),
                    530.0,
                    50.0,
                    50.0
                )
            },
            Math.PI / 30,
            0.99,
            i,
            pivotPoint
        )

        i += 1
    }
}