package art.funk.daily.y2019.may

import art.funk.daily.core.HighColor
import art.funk.daily.core.diamond.OverlapDiamondCanvas
import processing.core.PApplet
import java.awt.Color
import kotlin.random.Random

object May13 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May13Sketch::class.java)
    }
}

class May13Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val accentColor = HighColor(Color(75, 175, 140, 100))

    val diamondSize = 20.0
    val diamondCanvas =
        OverlapDiamondCanvas(myHeight, myWidth, diamondSize, this::diamondDraw)

    val random = Random(5)

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255).rgb)
        diamondCanvas.drawCanvas(this)
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    private fun diamondDraw(diamond: OverlapDiamondCanvas.Diamond): OverlapDiamondCanvas.DiamondStyle {
        val style = OverlapDiamondCanvas.DiamondStyle(diamond)
        if(random.nextInt(17) == 16) {
            style.color = accentColor
            style.borderStyle = OverlapDiamondCanvas.BorderStyle(2, HighColor(Color(180, 180, 180)))
        } else {
            style.color = HighColor(Color(255, 255, 255, 120))
            style.borderStyle = OverlapDiamondCanvas.BorderStyle(2, HighColor(Color(200, 200, 200)))
        }

        return style
    }
}