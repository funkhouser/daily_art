package art.funk.daily.y2019.may

import art.funk.daily.core.DPoint
import art.funk.daily.core.background
import art.funk.daily.core.stroke
import art.funk.daily.core.strokeWidth
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.random.Random

object May20 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May20Sketch::class.java)
    }
}

class May20Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val wheels = 100
    val random = Random(10)

    val colors = listOf(
        Color(255, 200, 210, 200),
        Color(255, 220, 210, 200),
        Color(255, 220, 230, 200),
        Color(255, 240, 240, 200)
    )

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(0, 0, 0, 255))

        for (i in 0..wheels) {
            val pinwheel = FailedPinwheel(
                colors[i % colors.size],
                DPoint(random.nextDouble(myWidth.toDouble()), random.nextDouble(myHeight.toDouble())),
                lines = random.nextInt(20),
                lineWidth = 10,
                lineSpacing = random.nextInt(20),
                startAngle = (random.nextDouble(Math.PI) * 2).toFloat(),
                failureAngle = (random.nextDouble(Math.PI) * 2).toFloat(),
                failureLineLength = random.nextDouble(300.0).toFloat()
            )

            pinwheel.draw(this)
        }
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }
}

class FailedPinwheel(
    val color: Color,
    val location: DPoint,
    val lines: Int,
    val lineWidth: Int,
    val lineSpacing: Int,
    val startAngle: Float,
    val failureAngle: Float,
    val failureLineLength: Float) {

    fun draw(applet: PApplet) {
        // TODO Actually do the math and such that are required to draw these lines.
        // ARC x, y, width, height, startAngle stopAngle (in radians)
        applet.stroke(color)
        applet.strokeWidth(lineWidth.toDouble())
        applet.fill(0)
        for (t in 0..(lines - 1)) {
            val i = lines - t
            applet.arc(
                (location.x).toFloat(),
                (location.y).toFloat(),
                (i * lineWidth + i * lineSpacing + lineWidth).toFloat(),
                (i * lineWidth + i * lineSpacing + lineWidth).toFloat(),
                startAngle,
                failureAngle,
                0)

            // radius = i * lineWidth + i * lineSpacing
            // Position = (x + cos(theta) * radius
            val radius0 = ((i * lineWidth + i * lineSpacing + lineWidth).toDouble() / 2)
            val radius1 = radius0 + failureLineLength.toDouble()

            val x0 = (location.x + Math.cos(failureAngle.toDouble()) * radius0).toFloat()
            val y0 = (location.y + Math.sin(failureAngle.toDouble()) * radius0).toFloat()

//            val x1 = (location.x + Math.cos(failureAngle.toDouble()) * radius1).toFloat()
//            val y1 = (location.y + Math.sin(failureAngle.toDouble()) * radius1).toFloat()

            val slope = Pair((x0 + (x0 - location.x) / radius0), (y0 - (y0 - location.y) / radius0))
            // We want inverse of this

            val x1 = (slope.second * failureLineLength + x0).toFloat()
            val y1 = (slope.first * failureLineLength + y0).toFloat()

            // ROFFFLL At least the slope is correct
//            val x1 = (x0 + (x0 - location.x) / radius0 * failureLineLength).toFloat()
//            val y1 =  * failureLineLength).toFloat()

            // I don't remember exactly what I was trying to do with this line
//            applet.line(x0, y0, x1, y1)


            // Trying out with a purely vertical failed pinwheel
            applet.line(x0, y0, x0, y0 + failureLineLength)

        }
    }
}