package art.funk.daily.y2019.may

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.random.Random

object
May30 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May30Sketch::class.java)
    }
}

class May30Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val strokeColor = Color(255, 220, 250, 10)
    val lineCount = 300

    val random = Random(5)


    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        strokeWidth(1.0)
        background(Color(255, 255, 255, 255))

        val points = listOf(
//            DPoint(0.0, 0.0),
//            DPoint(0.0, 720.0),
//            DPoint(720.0, 0.0),
            DPoint(720.0, 720.0)
        ) + IntArray(80).map { i ->
            DPoint(random.nextDouble(myWidth.toDouble()), random.nextDouble(myHeight.toDouble()))
        }

        val colorFunctions: List<IterativeColorFunction> = IntArray(20).map { i ->
            val color =  Color(
                strokeColor.red - random.nextInt(20),
                strokeColor.green - random.nextInt(120),
                strokeColor.blue - random.nextInt(120),
                strokeColor.alpha
            )
            val function = { index: Int ->
                color
            }

            function
        }

        parabolicRotation(
            points,
            colorFunctions,
            lineCount
        )
    }

    override fun draw() {
        super.draw()
        save("outputs/" + javaClass.name)
    }

    private fun parabolicRotation(points: List<DPoint>, colorFunctions: List<IterativeColorFunction>, lineCount: Int) {
        points.windowedCircular(3).forEachIndexed { index, (point0, point1, point2) ->
            parabolicDraw(
                point0,
                point2,
                point1,
                lineCount,
                colorFunctions[index % colorFunctions.size]
            )
        }
    }

    private fun parabolicDraw(
        startPoint: DPoint,
        endPoint: DPoint,
        pivotPoint: DPoint,
        lineCount: Int,
        colorFunction: IterativeColorFunction
    ) {
        // TODO Start the 'a' line at the startPoint and the 'b' line at the pivot point
        // Increment the 'a' point and the 'b' point on each iteration until conclusion
        for (i in 0..lineCount) {

            // TODO When turning this into a plugin-style piece change this to be a function such that
            // the user can either provide a delta from the line or simply a parametric function
            val aPoint = DPoint(
                startPoint.x + (pivotPoint.x - startPoint.x) * i / lineCount,
                startPoint.y + (pivotPoint.y - startPoint.y) * i / lineCount
            )

            val bPoint = DPoint(
                pivotPoint.x + (endPoint.x - pivotPoint.x) * i / lineCount,
                pivotPoint.y + (endPoint.y - pivotPoint.y) * i / lineCount
            )

            line(
                aPoint,
                bPoint,
                colorFunction(lineCount)
            )
        }
    }
}