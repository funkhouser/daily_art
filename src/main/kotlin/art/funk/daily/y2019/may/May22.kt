package art.funk.daily.y2019.may

import art.funk.daily.core.*
import processing.core.PApplet
import processing.core.PConstants
import java.awt.Color
import kotlin.random.Random

object May22 {
    @JvmStatic
    fun main(args: Array<String>) {
        PApplet.main(May22Sketch::class.java)
    }
}

class May22Sketch : PApplet() {

    val myHeight = 720
    val myWidth = 720

    val seedCount = 10

    val spacingVertical = 0
    val spacingHorizontal = 0
    val sideLength = 15
    val strokeWidth = 0.0
    val hexHeight = Math.sqrt(3.0) * sideLength
    val hexWidth = 2 * sideLength


    val maxColumns = (myWidth / (hexWidth / Math.sqrt(3.0) + spacingHorizontal)).toInt()
    val maxRows = (myHeight / (hexHeight / Math.sqrt(1.0) + spacingVertical)).toInt() + 1

    var columnIndex = 0

    val maxBlurDistance = 10

    val hexSeeds = mutableListOf<HexSeed>()
    val random = Random(10)

    data class HexSeed(
        val location: Point,
        val color: Color
    )

    override fun settings() {
        super.settings()
        size(myWidth, myHeight)
    }

    override fun setup() {
        background(Color(255, 255, 255, 255))
        stroke(Color(20, 20, 20, 55))
        strokeWidth(strokeWidth)

        // Create the hex seeds
        for (i in 0..seedCount) {
            val point = Point(
                random.nextInt(maxColumns),
                random.nextInt(maxRows)
            )

            val color = Color(
                random.nextInt(255),
                random.nextInt(255),
                random.nextInt(255),
                255
            )

            hexSeeds.add(
                HexSeed(
                    point,
                    color
                )
            )
        }
    }

    override fun draw() {

        for (rowIndex in 0..maxRows) {
            val currentPoint = Point(columnIndex, rowIndex)
            var currentColor = Color(255, 255, 255, 255)
            for (seed in hexSeeds) {
                val distance = hexDistance(currentPoint, seed.location)
                if (distance < maxBlurDistance) {
                    currentColor = currentColor.interpolate(seed.color, 1 - (distance.toDouble() / maxBlurDistance))
                }
            }

            fill(currentColor)

            drawHex(columnIndex, rowIndex)
        }

        columnIndex += 1
        if (columnIndex > maxColumns) {
            finished = true
        }

        if (finished) {
            save("outputs/" + javaClass.name)
        }
    }

    fun drawHex(column: Int, row: Int) {
        val yCenter: Double = if (column.rem(2) == 0) {
            hexHeight * row - (hexHeight / 2) + row * spacingVertical
        } else {
            hexHeight * row + row * spacingVertical
        }
        val xCenter = (hexWidth.toDouble() * column * 3 / 4) + column * spacingHorizontal

        beginShape()
        for (i in 0..5) {
            val point = hexCorner(DPoint(xCenter, yCenter), i)
            vertex(point.x.toFloat(), point.y.toFloat())
        }
        endShape(PConstants.CLOSE)
    }

    private fun hexCorner(center: DPoint, corner: Int): DPoint {
        val angleRadians = corner * Math.PI / 3
        return DPoint(
            center.x + sideLength * Math.cos(angleRadians),
            center.y + sideLength * Math.sin(angleRadians)
        )
    }

    // Hex distance using offset coordinates
    fun hexDistance(hex0: Point, hex1: Point): Int {
        val z0 = -(hex0.x + hex0.y)
        val z1 = -(hex1.x + hex1.y)
        return (Math.abs(hex0.x - hex1.x) + Math.abs(hex0.y - hex1.y) + Math.abs(z0 - z1)) / 2
    }
}