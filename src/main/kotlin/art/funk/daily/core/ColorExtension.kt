package art.funk.daily.core

import processing.core.PApplet
import java.awt.Color
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt

typealias IterativeColorFunction = (Int) -> Color
typealias LinearColorFunction = (Double) -> Color

// The strategy here is going to be creating different classes for these returns
// but have each of these only for the short-term: at their root they will each
// have an RGB color that will be needed to convert back to in order to draw etc.

// It is possible that "HighColor" will need to be the common data form to push around
// .... we'll see.

// Unless otherwise noted all Double values are 0.0 to 1.0

// Using https://www.codeproject.com/Articles/19045/Manipulating-colors-in-NET-Part-1 as primary reference
class HslColor(
    val hue: Double,
    val saturation: Double,
    val lightness: Double
) {
    fun rotate(hue: Double): HslColor {
        return HslColor(this.hue + hue % 1.0, saturation, lightness)
    }
}

class HsbColor(
    val hue: Double,
    val saturation: Double,
    val brightness: Double
)

class CmykColor(
    val cyan: Double,
    val magenta: Double,
    val yellow: Double,
    val black: Double
)

// Red and Blue luma are not bounded by 1.0
class YuvColor(
    val luma: Double,      // y
    val blueLuma: Double,  // u
    val redLuma: Double    // v

) {
    companion object {
        val maxRedLuma = 0.615
        val maxBlueLuma = 0.436
    }
}

// TODO Implement ranges, functions, etc. here
class CieXyzColor(
    val x: Double,
    val y: Double,
    val z: Double
)

fun Color.interpolate(other: Color, amount: Double): Color {
    val result = Color(
        (other.red * amount + red * (1 - amount)).toInt(),
        (other.green * amount + green * (1 - amount)).toInt(),
        (other.blue * amount + blue * (1 - amount)).toInt(),
        (other.alpha * amount + alpha * (1 - amount)).toInt()
    )
    return result
}


// Inverse?
fun Color.opposite(): Color {
    return Color(
        255 - this.red,
        255 - this.green,
        255 - this.green,
        this.alpha
    )
}

/**
 * Rotates by hue which goes from 0.0 to 1.0
 */
fun Color.rotate(hue: Double): Color {
    val hslColor = ColorFactory.toHsl(this).rotate(hue)
    val rawColor = ColorFactory.color(hslColor)
    return Color(
        rawColor.red,
        rawColor.green,
        rawColor.blue,
        this.alpha
    )
}


/**
 * Creates colors. All alpha values are maximum in other colors
 */
object ColorFactory {

    fun color(color: HslColor): Color {
        return fromHsl(color.hue, color.saturation, color.lightness)
    }

    fun color(color: HsbColor): Color {
        return fromHsb(color.hue, color.saturation, color.brightness)
    }

    fun color(color: CmykColor): Color {
        return fromCmyk(color.cyan, color.magenta, color.yellow, color.black)
    }

    fun color(color: YuvColor): Color {
        return fromYuv(color.luma, color.blueLuma, color.redLuma)
    }

    fun toHsl(color: Color): HslColor {
        val maxComponent = max(color.red, max(color.green, color.blue))
        val minComponent = min(color.red, min(color.green, color.blue))
        val maxNormalized = maxComponent / 255.0
        val minNormalized = minComponent / 255.0
        val hueDegrees: Double = when {
            maxComponent == minComponent -> 0.0
            maxComponent == color.red && color.green >= color.blue -> {
                60.0 * (color.green - color.blue) / (maxNormalized - minNormalized)
            }
            maxComponent == color.red && color.green < color.blue -> {
                60.0 * (color.green - color.blue) / (maxNormalized - minNormalized) + 360.0
            }
            maxComponent == color.green -> {
                60.0 * (color.blue - color.red) / (maxNormalized - minNormalized) + 120.0
            }
            maxComponent == color.blue -> {
                60.0 * (color.red - color.green) / (maxNormalized - minNormalized) + 240.0
            }
            else -> error("This is odd")
        }

        val luminance = (maxNormalized + minNormalized) / 2.0
        val saturation = if (luminance == 0.0 || maxNormalized == minNormalized) {
            0.0
        } else if (luminance <= 0.5) {
            (maxNormalized - minNormalized) / (maxNormalized + minNormalized)
        } else {
            (maxNormalized - minNormalized) / (2.0 - maxNormalized - minNormalized)
        }

        return HslColor(hueDegrees / 360.0, saturation, luminance)
    }

    fun toHsb(color: Color): HsbColor {
        TODO()
    }

    fun toCmyk(color: Color): CmykColor {
        TODO()
    }

    fun toYuv(color: Color): YuvColor {
        TODO()
    }

    /**
     * Expects hue in 0.0 to 1.0
     * saturation in 0.0 to 1.0
     * brightness in 0.0 to 1.0
     */
    fun fromHsb(
        hue: Double,
        saturation: Double,
        brightness: Double
    ): Color {
        return Color.getHSBColor(
            hue.toFloat(),
            saturation.toFloat(),
            brightness.toFloat()
        )
    }

    fun fromHsl(
        hue: Double,
        saturation: Double,
        lightness: Double
    ): Color {
        val brightness = (2 * lightness + saturation * (1 - Math.abs(2 * lightness - 1))) / 2
        return fromHsb(
            hue,
            2 * (brightness - lightness) / brightness,
            brightness
        )
    }

    fun fromCmyk(
        cyan: Double,
        magenta: Double,
        yellow: Double,
        black: Double
    ): Color {
        return Color(
            (((1 - cyan) * (1 - black)) * 255).toInt(),
            (((1 - magenta) * (1 - black)) * 255).toInt(),
            (((1 - yellow) * (1 - black)) * 255).toInt(),
            255
        )
    }

    fun fromYuv(
        luma: Double,
        redLuma: Double,
        blueLuma: Double
    ): Color {
        return Color(
            ((luma + (1.139837398373983740 * redLuma)) * 255).toInt(),
            ((luma - (0.3946517043589703515 * blueLuma) - (0.5805986066674976801 * redLuma)) * 255).toInt(),
            (((luma * 2.032110091743119266 * blueLuma)) * 255).toInt(),
            255
        )
    }

}

data class ColoredLine(
    val point0: DPoint,
    val point1: DPoint,
    val color: Color
)

data class LinearGradientLine(
    val point0: DPoint,
    val point1: DPoint,
    val color0: Color,
    val color1: Color
)


/**
 * DPoint from 0.0 to 1.0 (x and y) to determine each pixel
 */
typealias GradientRectFunction = (DPoint) -> Color


/**
 * Keeping out of the extension functions for the moment
 */
fun gradientRect(
    pApplet: PApplet,
    corner: DPoint,
    opposite: DPoint,
    gradientFunction: GradientRectFunction
) {

    val xDelta = opposite.x - corner.x
    val yDelta = opposite.y - corner.y

    val xCount = xDelta.roundToInt()
    val yCount = yDelta.roundToInt()

    val xPerIteration = xDelta / xCount
    val yPerIteration = yDelta / yCount

    for (x0 in 0..xCount) {
        for (y0 in 0..yCount) {
            val point = DPoint(
                corner.x + xPerIteration * x0,
                corner.y + yPerIteration * y0
            )

            pApplet.stroke(gradientFunction(point))
            pApplet.point(point)
        }
    }
}

fun linearGradientCircle(
    pApplet: PApplet,
    center: DPoint,
    size: Double,
    iterativeColorFunction: LinearColorFunction
) {
    pApplet.fill(Color(255, 255, 255, 0))
    val iterations = size.roundToInt() * 2 - 1
    val perIteration = size / iterations

    for (i in 0 until iterations) {
        val distance = i * perIteration
        pApplet.stroke(iterativeColorFunction(distance))
        pApplet.circle(
            center,
            distance
        )
    }
}

