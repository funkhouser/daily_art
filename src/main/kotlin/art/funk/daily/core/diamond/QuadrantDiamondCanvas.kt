package art.funk.daily.core.diamond

import art.funk.daily.core.DPoint
import art.funk.daily.core.HighColor
import art.funk.daily.core.rectViaSize
import processing.core.PApplet
import kotlin.math.roundToInt

typealias QuadrantDiamondDrawer = (QuadrantDiamondCanvas.Diamond) -> QuadrantDiamondCanvas.DiamondStyle


// For now, using row-offset for simplicity
// This means that when (x + y) % 2 == 1 the x position will add (diamondHeight / 2)
// And each y increment is always (perDiamondHeight / 2)
class QuadrantDiamondCanvas(val sketchHeight: Int, val sketchWidth: Int, val sideLength: Double, val diamondDrawer: QuadrantDiamondDrawer) {

    val perDiamondHeight = sideLength * Math.sqrt(2.0)
    val perDiamondWidth = sideLength * Math.sqrt(2.0)
    val yDiamondCount = Math.ceil(sketchHeight / perDiamondHeight).roundToInt() * 2
    val xDiamondCount = Math.ceil(sketchWidth / perDiamondWidth).roundToInt() * 2
    val canvas = Array(xDiamondCount) { x ->
        Array(yDiamondCount) { y ->
            Diamond(x, y)
        }
    }

    fun getCenter(diamond: Diamond): DPoint {
        return DPoint(
            diamond.x * perDiamondWidth / 2 + (if ((diamond.y + diamond.x) % 2 == 1) (perDiamondWidth / 2) else 0.0),
            diamond.y * perDiamondHeight / 2 + (if ((diamond.y + diamond.x) % 2 == 1) (perDiamondHeight / 2) else 0.0)
        )
    }

    fun getCoordinates(diamond: Diamond): List<DPoint> {
        val center = getCenter(diamond)
        return listOf(
            DPoint(center.x, center.y - perDiamondHeight / 2),
            DPoint(center.x + perDiamondWidth / 2, center.y),
            DPoint(center.x, center.y + perDiamondHeight / 2),
            DPoint(center.x - perDiamondWidth / 2, center.y)
        )
    }

    fun getInteriorCoordinates(diamond: Diamond, borderWidth: Int): List<DPoint> {
        val center = getCenter(diamond)
        val perDiamondHeightInterior = (sideLength - borderWidth) * Math.sqrt(2.0)
        val perDiamondWidthInterior = (sideLength - borderWidth) * Math.sqrt(2.0)
        return listOf(
            DPoint(center.x, center.y - perDiamondHeightInterior / 2),
            DPoint(center.x - perDiamondWidthInterior / 2, center.y),
            DPoint(center.x, center.y + perDiamondHeightInterior / 2),
            DPoint(center.x + perDiamondWidthInterior / 2, center.y)
        )
    }

    fun getOppositeCorners(diamond: Diamond, borderWidth: Int): List<DPoint> {
        val center = getCenter(diamond)
        return listOf(
            DPoint(center.x, center.y - perDiamondHeight - borderWidth / 2.0),
            DPoint(center.x - perDiamondWidth - borderWidth / 2.0, center.y)
        )
    }

    fun drawCanvas(applet: PApplet) {
        canvas.forEach { column ->
            column.forEach { diamond ->
                val style = diamondDrawer(diamond)
                val borderWidth = style.borderStyle?.width ?: 0
                applyBorderStyle(applet, style)
                applyFillStyle(applet, style)

                val corners = getOppositeCorners(diamond, borderWidth)
                applet.rotate((Math.PI / 4.0).toFloat())
                applet.rectViaSize(corners[0], sideLength, sideLength, 10.0)
                // Apparently the rotation needs to be reset...
                applet.rotate((-Math.PI / 4.0).toFloat())

//                applet.rectViaCorners(corners[0], corners[1], 0.0)

                // TODO Maybe allow this hollowshape to actual do this.
//                applet.hollowShape(getCoordinates(diamond), getInteriorCoordinates(diamond, borderWidth))
//                applet.stroke(Color(255, 255, 255, 0))
//                applet.shape(getInteriorCoordinates(diamond, borderWidth))
            }
        }
    }

    private fun applyBorderStyle(applet: PApplet, diamondStyle: DiamondStyle) {
        diamondStyle.borderStyle?.color?.toProcessingInt()?.let { applet.stroke(it) }
        diamondStyle.borderStyle?.width?.let { applet.strokeWeight(it.toFloat()) }
    }

    private fun applyFillStyle(applet: PApplet, diamondStyle: DiamondStyle) {
        diamondStyle.color?.toProcessingInt()?.let { applet.fill(it) }
    }

    class Diamond(val x: Int, val y: Int)

    class DiamondStyle(val diamond: Diamond) {
        var color: HighColor? = null
        var borderStyle: BorderStyle? = null
    }

    class BorderStyle(val width: Int, val color: HighColor)
}

