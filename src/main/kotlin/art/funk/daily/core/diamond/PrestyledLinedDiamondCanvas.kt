package art.funk.daily.core.diamond

import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color
import kotlin.math.roundToInt

typealias PrestyledLinedDiamondDrawer = (PrestyledLinedDiamondCanvas.PrestyledLinedDiamond) -> PrestyledLinedDiamondCanvas.PrestyledLinedDiamondStyle


// For now, using row-offset for simplicity
// This means that when (x + y) % 2 == 1 the x position will add (diamondHeight / 2)
// And each y increment is always (perDiamondHeight / 2)

// The better way to do this might be to have methods for both:
//   getting the neighbors for a given diamond
//   pushing diamonds onto the styling stack

class PrestyledLinedDiamondCanvas(
    val sketchHeight: Int,
    val sketchWidth: Int,
    val sideLength: Double,
    val defaultBorderStyle: PrestyledBorderStyle
) {

    val perDiamondHeight = sideLength * Math.sqrt(2.0)
    val perDiamondWidth = sideLength * Math.sqrt(2.0)
    val yDiamondCount = Math.ceil(sketchHeight / perDiamondHeight).roundToInt() * 2
    val xDiamondCount = Math.ceil(sketchWidth / perDiamondWidth).roundToInt() * 2

    var applyBorderAsSpace = true
    var doubleWithDraw = false

    val diamondStyles: Array<Array<PrestyledLinedDiamondStyle>> = Array(xDiamondCount) { x ->
        Array(yDiamondCount) { y ->
            PrestyledLinedDiamondStyle(PrestyledLinedDiamond(x, y))
        }
    }


    fun getCenter(diamond: PrestyledLinedDiamond): DPoint {
        return DPoint(
            diamond.x * perDiamondWidth / 2 + (if ((diamond.y) % 2 == 1) (perDiamondWidth / 2) else 0.0),
            diamond.y * perDiamondHeight / 2 + (if ((diamond.x) % 2 == 1) (perDiamondHeight / 2) else 0.0)
        )
    }

    fun getCoordinates(diamond: PrestyledLinedDiamond): List<DPoint> {
        val center = getCenter(diamond)
        return listOf(
            DPoint(center.x, center.y - perDiamondHeight / 2),
            DPoint(center.x + perDiamondWidth / 2, center.y),
            DPoint(center.x, center.y + perDiamondHeight / 2),
            DPoint(center.x - perDiamondWidth / 2, center.y)
        )
    }

    fun getInteriorCoordinates(diamond: PrestyledLinedDiamond, borderWidth: Double): List<DPoint> {
        val center = getCenter(diamond)
        val perDiamondHeightInterior = (sideLength - borderWidth) * Math.sqrt(2.0)
        val perDiamondWidthInterior = (sideLength - borderWidth) * Math.sqrt(2.0)
        return listOf(
            DPoint(center.x, center.y - perDiamondHeightInterior / 2),
            DPoint(center.x - perDiamondWidthInterior / 2, center.y),
            DPoint(center.x, center.y + perDiamondHeightInterior / 2),
            DPoint(center.x + perDiamondWidthInterior / 2, center.y)
        )
    }

    fun getOppositeCorners(diamond: PrestyledLinedDiamond, borderWidth: Double): List<DPoint> {
        val center = getCenter(diamond)
        return listOf(
            DPoint(
                center.x,
                center.y - perDiamondHeight - if (applyBorderAsSpace) borderWidth / 2.0 else 0.0
            ),
            DPoint(
                center.x - perDiamondWidth - if (applyBorderAsSpace) borderWidth / 2.0 else 0.0,
                center.y
            )
        )
    }

    fun drawCanvas(applet: PApplet) {
        diamondStyles.forEach { column ->
            column.forEach { style ->
                val borderStyle = style.borderStyle ?: defaultBorderStyle
                val borderWidth = borderStyle.width
                applyFillStyle(applet, style)
                applyBorderStyle(applet, borderStyle)

                val points = getCoordinates(style.diamond)
                if (borderStyle.topLeft) { applet.line(points[0], points[1]) }
                if (borderStyle.bottomLeft) { applet.line(points[1], points[2]) }
                if (borderStyle.bottomRight) { applet.line(points[2], points[3]) }
                if (borderStyle.topRight) { applet.line(points[3], points[0]) }

                if (doubleWithDraw) {
                    applet.stroke(Color(255, 255, 255, 0))
                    applet.strokeWeight(0F)

                    val corner = getOppositeCorners(style.diamond, borderWidth)[0]
                    applet.rotate((Math.PI / 4.0).toFloat())

                    // The shape is rotated from the origin rather than the corner so we need to apply
                    // the inverse transformation first to move it in the correct direction
                    val preRotatedCorner = DPoint(
                        corner.x * Math.cos(-Math.PI / 4) + corner.y * Math.cos(-Math.PI / 4),
                        corner.x * Math.sin(-Math.PI / 4) + corner.y * Math.cos(-Math.PI / 4)
                    )

                    applet.rectViaSize(preRotatedCorner, sideLength, sideLength, 10.0)
                    // Apparently the rotation needs to be reset...
                    applet.rotate((-Math.PI / 4.0).toFloat())
                }
            }
        }
    }

    private fun applyBorderStyle(applet: PApplet, style: PrestyledBorderStyle) {
        applet.stroke(style.color)
        applet.strokeWidth(style.width)
    }

    private fun applyFillStyle(applet: PApplet, diamondStyle: PrestyledLinedDiamondStyle) {
        diamondStyle.color?.toProcessingInt()?.let { applet.fill(it) }
    }

    class PrestyledLinedDiamond(val x: Int, val y: Int)

    class PrestyledLinedDiamondStyle(val diamond: PrestyledLinedDiamond) {
        var color: HighColor? = null
        var borderStyle: PrestyledBorderStyle? = null
    }

    class PrestyledBorderStyle(val width: Double, val color: HighColor) {
        var topLeft = true
        var topRight = true
        var bottomLeft = true
        var bottomRight = true
    }

}

