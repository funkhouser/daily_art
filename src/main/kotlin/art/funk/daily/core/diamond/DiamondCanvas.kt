package art.funk.daily.core.diamond

import art.funk.daily.core.DPoint
import art.funk.daily.core.HighColor
import art.funk.daily.core.rectViaSize
import processing.core.PApplet
import kotlin.math.roundToInt

typealias DiamondDrawer = (DiamondCanvas.Diamond) -> DiamondCanvas.DiamondStyle


// For now, using row-offset for simplicity
// This means that when (x + y) % 2 == 1 the x position will add (diamondHeight / 2)
// And each y increment is always (perDiamondHeight / 2)
class DiamondCanvas(val sketchHeight: Int, val sketchWidth: Int, val sideLength: Double, val diamondDrawer: DiamondDrawer) {

    val perDiamondHeight = sideLength * Math.sqrt(2.0)
    val perDiamondWidth = sideLength * Math.sqrt(2.0)
    val yDiamondCount = Math.ceil(sketchHeight / perDiamondHeight).roundToInt() * 2
    val xDiamondCount = Math.ceil(sketchWidth / perDiamondWidth).roundToInt() * 2

    var applyBorderAsSpace = true
    var rounding = 10.0

    val canvas = Array(xDiamondCount) { x ->
        Array(yDiamondCount) { y ->
            Diamond(x, y)
        }
    }

    fun getCenter(diamond: Diamond): DPoint {
        return DPoint(
            diamond.x * perDiamondWidth / 2 + (if ((diamond.y) % 2 == 1) (perDiamondWidth / 2) else 0.0),
            diamond.y * perDiamondHeight / 2 + (if ((diamond.x) % 2 == 1) (perDiamondHeight / 2) else 0.0)
        )
    }

    fun getCoordinates(diamond: Diamond): List<DPoint> {
        val center = getCenter(diamond)
        return listOf(
            DPoint(center.x, center.y - perDiamondHeight / 2),
            DPoint(center.x + perDiamondWidth / 2, center.y),
            DPoint(center.x, center.y + perDiamondHeight / 2),
            DPoint(center.x - perDiamondWidth / 2, center.y)
        )
    }

    fun getInteriorCoordinates(diamond: Diamond, borderWidth: Int): List<DPoint> {
        val center = getCenter(diamond)
        val perDiamondHeightInterior = (sideLength - borderWidth) * Math.sqrt(2.0)
        val perDiamondWidthInterior = (sideLength - borderWidth) * Math.sqrt(2.0)
        return listOf(
            DPoint(center.x, center.y - perDiamondHeightInterior / 2),
            DPoint(center.x - perDiamondWidthInterior / 2, center.y),
            DPoint(center.x, center.y + perDiamondHeightInterior / 2),
            DPoint(center.x + perDiamondWidthInterior / 2, center.y)
        )
    }

    fun getOppositeCorners(diamond: Diamond, borderWidth: Int): List<DPoint> {
        val center = getCenter(diamond)
        return listOf(
            DPoint(
                center.x,
                center.y - perDiamondHeight - if (applyBorderAsSpace) borderWidth / 2.0 else 0.0
            ),
            DPoint(
                center.x - perDiamondWidth - if (applyBorderAsSpace) borderWidth / 2.0 else 0.0,
                center.y
            )
        )
    }

    fun drawCanvas(applet: PApplet) {
        canvas.forEach { column ->
            column.forEach { diamond ->
                val style = diamondDrawer(diamond)
                val borderWidth = style.borderStyle?.width ?: 0
                applyBorderStyle(applet, style)
                applyFillStyle(applet, style)

                val corner = getOppositeCorners(diamond, borderWidth)[0]
                applet.rotate((Math.PI / 4.0).toFloat())

                // The shape is rotated from the origin rather than the corner so we need to apply
                // the inverse transformation first to move it in the correct direction
                val preRotatedCorner = DPoint(
                    corner.x * Math.cos(-Math.PI / 4) + corner.y * Math.cos(-Math.PI / 4),
                    corner.x * Math.sin(-Math.PI / 4) + corner.y * Math.cos(-Math.PI / 4)
                )

                applet.rectViaSize(preRotatedCorner, sideLength, sideLength, rounding)
                // Apparently the rotation needs to be reset...
                applet.rotate((-Math.PI / 4.0).toFloat())

            }
        }
    }

    private fun applyBorderStyle(applet: PApplet, diamondStyle: DiamondStyle) {
        diamondStyle.borderStyle?.color?.toProcessingInt()?.let { applet.stroke(it) }
        diamondStyle.borderStyle?.width?.let { applet.strokeWeight(it.toFloat()) }
    }

    private fun applyFillStyle(applet: PApplet, diamondStyle: DiamondStyle) {
        diamondStyle.color?.toProcessingInt()?.let { applet.fill(it) }
    }

    class Diamond(val x: Int, val y: Int)

    class DiamondStyle(val diamond: Diamond) {
        var color: HighColor? = null
        var borderStyle: BorderStyle? = null
    }

    class BorderStyle(val width: Int, val color: HighColor) {
        var topLeft = true
        var topRight = true
        var bottomLeft = true
        var bottomRight = true
    }
}

