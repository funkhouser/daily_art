package art.funk.daily.core.diamond

import art.funk.daily.core.*
import processing.core.PApplet
import java.awt.Color
import kotlin.math.roundToInt

typealias SpacedDiamondDrawer = (SpacedDiamondCanvas.Diamond) -> SpacedDiamondCanvas.DiamondStyle


// For now, using row-offset for simplicity
// This means that when (x + y) % 2 == 1 the x position will add (diamondHeight / 2)
// And each y increment is always (perDiamondHeight / 2)
class SpacedDiamondCanvas(val sketchHeight: Int, val sketchWidth: Int, val sideLength: Double, val diamondDrawer: SpacedDiamondDrawer) {

    val perDiamondHeight = sideLength * Math.sqrt(2.0)
    val perDiamondWidth = sideLength * Math.sqrt(2.0)
    val yDiamondCount = Math.ceil(sketchHeight / perDiamondHeight).roundToInt()
    val xDiamondCount = Math.ceil(sketchWidth / perDiamondWidth).roundToInt()
    val canvas = Array(xDiamondCount) { x ->
        Array(yDiamondCount) { y ->
            Diamond(x, y)
        }
    }

    fun getCenter(diamond: Diamond): DPoint {
        return DPoint(diamond.x * perDiamondWidth, diamond.y * perDiamondHeight)
    }

    fun getCoordinates(diamond: Diamond): List<DPoint> {
        val center = getCenter(diamond)
        return listOf(
            DPoint(center.x, center.y - perDiamondHeight / 2),
            DPoint(center.x + perDiamondWidth / 2, center.y),
            DPoint(center.x, center.y + perDiamondHeight / 2),
            DPoint(center.x - perDiamondWidth / 2, center.y)
        )
    }

    fun getInteriorCoordinates(diamond: Diamond, borderWidth: Int): List<DPoint> {
        val center = getCenter(diamond)
        val perDiamondHeightInterior = (sideLength - borderWidth) * Math.sqrt(2.0)
        val perDiamondWidthInterior = (sideLength - borderWidth) * Math.sqrt(2.0)
        return listOf(
            DPoint(center.x, center.y - perDiamondHeightInterior / 2),
            DPoint(center.x - perDiamondWidthInterior / 2, center.y),
            DPoint(center.x, center.y + perDiamondHeightInterior / 2),
            DPoint(center.x + perDiamondWidthInterior / 2, center.y)
        )
    }

    fun drawCanvas(applet: PApplet) {
        canvas.forEach { column ->
            column.forEach { diamond ->
                val style = diamondDrawer(diamond)
                val borderWidth = style.borderStyle?.width ?: 1
                applyBorderStyle(applet, style)
                applet.hollowShape(getCoordinates(diamond), getInteriorCoordinates(diamond, borderWidth))
                applet.stroke(Color(255, 255, 255, 0))
                applyFillStyle(applet, style)
                applet.shape(getInteriorCoordinates(diamond, borderWidth))
            }
        }
    }

    private fun applyBorderStyle(applet: PApplet, diamondStyle: DiamondStyle) {
        diamondStyle.borderStyle?.color?.toProcessingInt()?.let { applet.fill(it) }
        applet.stroke(Color(255, 255, 255, 0))
    }

    private fun applyFillStyle(applet: PApplet, diamondStyle: DiamondStyle) {
        diamondStyle.color?.toProcessingInt()?.let { applet.fill(it) }
    }

    class Diamond(val x: Int, val y: Int)

    class DiamondStyle(val diamond: Diamond) {
        var color: HighColor? = null
        var borderStyle: BorderStyle? = null
    }

    class BorderStyle(val width: Int, val color: HighColor)
}

