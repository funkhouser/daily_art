package art.funk.daily.core.diamond

import art.funk.daily.core.DPoint
import art.funk.daily.core.HighColor
import art.funk.daily.core.shape
import processing.core.PApplet
import kotlin.math.roundToInt

typealias OverlapDiamondDrawer = (OverlapDiamondCanvas.Diamond) -> OverlapDiamondCanvas.DiamondStyle

// This wasn't intended, but leaving it for the heck of it because it is a rather interesting look
class OverlapDiamondCanvas(val sketchHeight: Int, val sketchWidth: Int, val sideLength: Double, val diamondDrawer: OverlapDiamondDrawer) {

    val perDiamondHeight = sideLength * Math.sqrt(2.0)
    val perDiamondWidth = sideLength * Math.sqrt(2.0)
    val yDiamondCount = Math.ceil(sketchHeight / sideLength).roundToInt() + 2
    val xDiamondCount = Math.ceil(sketchWidth / sideLength).roundToInt() + 2
    val canvas = Array(xDiamondCount) { x ->
        Array(yDiamondCount) { y ->
            Diamond(x, y)
        }
    }

    fun getCenter(diamond: Diamond): DPoint {
        return DPoint(diamond.x * sideLength, diamond.y * sideLength)
    }

    fun getCoordinates(diamond: Diamond): List<DPoint> {
        val center = getCenter(diamond)
        return listOf(
            DPoint(center.x, center.y - perDiamondHeight / 2),
            DPoint(center.x + perDiamondWidth / 2, center.y),
            DPoint(center.x, center.y + perDiamondHeight / 2),
            DPoint(center.x - perDiamondWidth / 2, center.y)
        )
    }

    fun drawCanvas(applet: PApplet) {
        canvas.forEach { column ->
            column.forEach { diamond ->
                val style = diamondDrawer(diamond)
                applyStyle(applet, style)
                applet.shape(getCoordinates(diamond))
            }
        }
    }

    private fun applyStyle(applet: PApplet, diamondStyle: DiamondStyle) {
        diamondStyle.borderStyle?.color?.toProcessingInt()?.let { applet.stroke(it) }
        diamondStyle.borderStyle?.width?.let { applet.strokeWeight(it.toFloat()) }
        diamondStyle.color?.toProcessingInt()?.let { applet.fill(it) }
    }

    class Diamond(val x: Int, val y: Int)

    class DiamondStyle(val diamond: Diamond) {
        var color: HighColor? = null
        var borderStyle: BorderStyle? = null
    }

    class BorderStyle(val width: Int, val color: HighColor)
}

