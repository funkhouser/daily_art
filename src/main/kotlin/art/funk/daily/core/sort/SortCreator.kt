package art.funk.daily.core.sort

import kotlin.random.Random


class SortCreator(val size: Int, val random: Random) {

    val initialState = createInitialState()
    val sortingArray = initialState.toIntArray()

    /**
     * Returns a list of each step in the sort-state. Starts from the unsorted
     * state ands steps (using the given type) towards a sorted set.
     */
    fun runSort(sortType: SortType): List<List<Int>> {
        return when (sortType) {
            SortType.BUBBLE -> bubbleSort().overview
            SortType.IMMEDIATE -> {
                val result = initialState.sorted()
                listOf(initialState, result)
            }
            SortType.QUICK -> quickSort().overview
            else -> emptyList()
        }
    }

    fun bubbleSort(): SortResult {
        var swapping = true
        val swaps = mutableListOf<SortOperation>()
        while (swapping) {
            swapping = false
            for (i in 0..(sortingArray.size - 2)) {
                if (sortingArray[i] > sortingArray[i + 1]) {
                    swaps.add(swap(i, i + 1))
                    swapping = true
                }
            }
        }
        return createSortResult(swaps)
    }

    fun quickSort(): SortResult {
        val swaps = mutableListOf<SortOperation>()
        val partition: (Int, Int, Int) -> Int = { leftIndex: Int, rightIndex: Int, pivot: Int ->
            val value = sortingArray[pivot]
            var leftEndcap = leftIndex
            val rightEndcap = rightIndex - 1
            swaps.add(swap(pivot, rightEndcap))
            for (i in leftEndcap..(rightEndcap - 1)) {
                if (sortingArray[i] <= value) {
                    swaps.add(swap(i, leftEndcap))
                    leftEndcap += 1
                }
            }
            swaps.add(swap(leftEndcap, rightEndcap))
            leftEndcap
        }

        var recurse: ((Int, Int) -> Unit)? = null
        recurse = { leftIndex: Int, rightIndex: Int ->
            if (leftIndex < rightIndex - 1) {
                val pivot = partition(leftIndex, rightIndex, (leftIndex + rightIndex) / 2)
                recurse?.invoke(leftIndex, pivot)
                recurse?.invoke(pivot + 1, rightIndex)
            }
        }

        recurse.invoke(0, sortingArray.size)

        return createSortResult(swaps)
    }

    private fun createSortResult(swaps: List<SortOperation>): SortResult {
        val postSwaps = mutableListOf<List<Int>>()
        postSwaps.add(initialState)
        val mutatingList = initialState.toMutableList()
        swaps.forEach { swap ->
            val value0 = mutatingList[swap.index0]
            mutatingList[swap.index0] = mutatingList[swap.index1]
            mutatingList[swap.index1] = value0
            postSwaps.add(mutatingList.map { it })
        }

        return SortResult(swaps, postSwaps)
    }

    private fun swap(i: Int, j: Int): SortOperation {
        val oldIValue = sortingArray[i]
        val oldJValue = sortingArray[j]

        sortingArray[i] = oldJValue
        sortingArray[j] = oldIValue
        return SortOperation(i, j)
    }


    fun createInitialState(): List<Int> {
        val resultArray = arrayOfNulls<Int>(size)
        var i = 0
        while (resultArray.contains(null)) {
            val index = random.nextInt(size)
            resultArray[index] = resultArray[index] ?: {
                i += 1
                i
            }()
        }
        return resultArray.toList().mapNotNull { it }
    }
}

enum class SortType {
    BUBBLE,
    QUICK,
    RANDOM,
    IMMEDIATE
}

data class SortOperation(val index0: Int, val index1: Int)
data class SortResult(val operations: List<SortOperation>, val overview: List<List<Int>>)