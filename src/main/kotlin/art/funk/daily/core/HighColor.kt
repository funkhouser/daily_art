package art.funk.daily.core

// Expects red, green, and blue from a range of 0 -> MaxInt
// Opacity is a Double from 0.0 -> 1.0
// HighColor is a construct that was created to allow for low opacity colors to be layered over top
// each other with far higher fidelity. The downside is that this requires essentially a virtual diamondCanvas
// that keep tracks of the HighColor entries before bouncing down to a standard (Processing) diamondCanvas
data class HighColor(val red: Int, val green: Int, val blue: Int, val opacity: Double) {
    val bitMask: Int = 0x7F000000

    companion object {
        val MAX_VALUE: Int = 0x7FFFFFFF
    }

    constructor(color: java.awt.Color) : this(color.red.shl(23), color.green.shl(23), color.blue.shl(23), color.alpha / 255.0)

    // TODO Fix this. It just assumes 8-bit colors. If the colors are going to be full ints
    // these need to get bounced down into the actual Int. The simplest way is going to be
    // a shift to drop all of the lower pieces of information
    fun toProcessingInt(): Int {
        val redByte = red.and(bitMask).shr(23)
        val greenByte = green.and(bitMask).shr(23)
        val blueByte = blue.and(bitMask).shr(23)
        val opacity = (255 * opacity).toInt()

        return opacity.shl(24) + redByte.shl(16) + greenByte.shl(8) + blueByte
    }

    // Applies another HighColor with the appropriate opacity
    // Use https://en.wikipedia.org/wiki/Alpha_compositing OVER operator
    // Might have to assume a background of black or white always. Would keep things simple at least

    // Assumes that `other` is being applied on top (I don't think this matters, it might)
    // other = A
    // this = B
    fun apply(other: HighColor): HighColor {

        // Alpha0 = 1 - (1 - alphaA)(1 - alphaB)
        // Result = (alphaA/alpha0) * colorA + (1 - (alphaA/alpha0)) * colorB
        val alphaResult = 1.0 - (1.0 - other.opacity) * (1.0 - opacity)
        val alphaFactor = other.opacity / alphaResult

        val redResult = (alphaFactor * other.red + (1 - alphaFactor) * red).toInt()
        val greenResult = (alphaFactor * other.green + (1 - alphaFactor) * green).toInt()
        val blueResult = (alphaFactor * other.blue + (1 - alphaFactor) * blue).toInt()

        return HighColor(
            redResult,
            greenResult,
            blueResult,
            alphaResult)
    }

    fun percent(percent: Double): HighColor {
        return HighColor(
            red,
            green,
            blue,
            opacity * percent)
    }

    fun interpolate(other: HighColor, amount: Double): HighColor {
        val result = HighColor(
            (other.red * amount + red * (1 - amount)).toInt(),
            (other.green * amount + green * (1 - amount)).toInt(),
            (other.blue * amount + blue * (1 - amount)).toInt(),
            (other.opacity * amount + opacity * (1 - amount)))
        return result
    }

}