package art.funk.daily.core.space

import art.funk.daily.core.D3Point
import com.jogamp.opengl.math.Matrix4
import kotlin.math.tan

// TODO Come up with a better name for this
// Space is for transforming 3 dimensional structures into a 2 dimensional viewport
// Specifically this class needs to handle the camera and ordering because it is
// going to be simpler to handle that in a single place (here) rather than haphazardly
class Space {

    // The center of the camera will always be pointed at the origin (for now)
    val cameraLocation = D3Point(1.0, -1.0, 1.0)

    // The initial size of the viewport at the camera location
    val viewPortSize = 1.0

    val aspectRatio = 1.0 / viewPortSize

    // The angle at which the viewport will expand with
    // 45 degrees -> field of view is 1.0 (for a 90 degree total)
    val viewPortAngle = Math.PI / 4
    val fieldOfView = 1.0 / tan(viewPortAngle / 2.0)

    fun drawTriangle(triangle: D3Triangle) {
        TODO()
//        val clipMatrix = arrayOf(
//            arrayOf(fieldOfView * aspectRatio, 0, 0, 0),
//            arrayOf(0, fieldOfView, 0, 0),
//            arrayOf(0, 0, ())
//        )
    }
}

class TransformerThingy {

    fun doTheThing() {
        val cameraToWorldMatrix = arrayOf(
            arrayOf(0.718762, 0.615033, -0.324214, 0),
            arrayOf(-0.393732, 0.744416, 0.539277, 0),
            arrayOf(0.573024, -0.259959, 0.777216, 0),
            arrayOf(0.526967, 1.254234, -2.53215, 1)
        )
    }
}