package art.funk.daily.core.space

import art.funk.daily.core.D3Point

class D3Triangle(
    val point0: D3Point,
    val point1: D3Point,
    val point2: D3Point
)

