package art.funk.daily.core

import art.funk.daily.core.applet.rotate
import art.funk.daily.core.bitmap.BitMapCanvas
import processing.core.PApplet
import processing.core.PConstants
import processing.core.PConstants.CORNER
import processing.core.PConstants.CORNERS
import java.awt.Color
import kotlin.math.roundToInt

data class Point(val x: Int, val y: Int)
data class Vertex(val point0: Point, val point1: Point)
data class DPoint(val x: Double, val y: Double) {

    fun opposite(): DPoint {
        return DPoint(-x, -y)
    }

    operator fun minus(other: DPoint): DPoint {
        return DPoint(x - other.x, y - other.y)
    }

    operator fun plus(other: DPoint): DPoint {
        return DPoint(x + other.x, y + other.y)
    }

    operator fun times(scale: Double): DPoint {
        return DPoint(x * scale, y * scale)
    }

    operator fun times(scale: Int): DPoint {
        return DPoint(x * scale, y * scale)
    }

    /**
     * Overridden to cross product.... thingy. This is something else and I can't remember what
     *
     * Ahh, the 2d determinant
     */
    operator fun times(other: DPoint): Double {
        return x * other.y - y * other.x
    }

    /**
     * But we'll keep around the dot product
     */
    fun dot(other: DPoint): Double {
        return (x * other.x) + (y * other.y)
    }

    operator fun div(other: Double): DPoint {
        return DPoint(x / other, y / other)
    }

    fun magnitude(): Double {
        return Math.sqrt(x * x + y * y)
    }

    fun normalized(): DPoint {
        val magnitude = magnitude()
        return DPoint(
            x / magnitude,
            y / magnitude
        )
    }

    fun mirrored(): DPoint {
        return DPoint(y, x)
    }
}
data class DVertex(val point0: DPoint, val point1: DPoint) {

    fun distance(): Double {
        return (this.point0.x - this.point1.x) * (this.point0.x - this.point1.x) +
                (this.point0.y - this.point1.y) * (this.point0.y - this.point1.y)
    }
}
data class D3Point(val x: Double, val y: Double, val z: Double) {

    constructor(point: DPoint) : this(point.x, point.y, 0.0)

    fun cross(other: D3Point): D3Point {
        return D3Point(
            this.y * other.z - this.z * other.y,
            - this.x * other.z + this.z * other.x,
            this.x * other.y - this.y * other.x
        )
    }

    fun dot(other: D3Point): Double {
        return this.x * other.x + this.y * other.y + this.z + other.z
    }
}

fun PApplet.background(color: Color) {
    this.background(color.rgb)
}

fun PApplet.stroke(color: Color) {
    this.stroke(color.rgb)
}

fun PApplet.stroke(color: HighColor) {
    this.stroke(color.toProcessingInt())
}

fun PApplet.strokeWidth(width: Double) {
    this.strokeWeight(width.toFloat())
}

enum class StrokeCap {
    ROUND,
    SQUARE,
    PROJECT
}

fun PApplet.strokeCap(strokeCap: StrokeCap) {
    when(strokeCap) {
        StrokeCap.ROUND -> strokeCap(PConstants.ROUND)
        StrokeCap.SQUARE -> strokeCap(PConstants.SQUARE)
        StrokeCap.PROJECT -> strokeCap(PConstants.PROJECT)
    }
}

fun PApplet.fill(color: Color) {
    this.fill(color.rgb)
}

fun PApplet.point(x: Double, y: Double) {
    this.point(x.toFloat(), y.toFloat())
}

fun PApplet.point(point: DPoint) {
    this.point(point.x, point.y)
}

fun PApplet.point(x: Double, y: Double, color: Color) {
    stroke(color)
    point(x, y)
}

fun PApplet.point(point: DPoint, color: Color) {
    point(point.x, point.y, color)
}

fun PApplet.line(x0: Double, y0: Double, x1: Double, y1: Double) {
    this.line(x0.toFloat(), y0.toFloat(), x1.toFloat(), y1.toFloat())
}

fun PApplet.line(x0: Double, y0: Double, x1: Double, y1: Double, color: Color) {
    this.stroke(color.rgb)
    this.line(x0.toFloat(), y0.toFloat(), x1.toFloat(), y1.toFloat())
}

fun PApplet.line(point0: DPoint, point1: DPoint) {
    this.line(point0.x, point0.y, point1.x, point1.y)
}

fun PApplet.line(point0: DPoint, point1: DPoint, color: Color) {
    this.line(point0.x, point0.y, point1.x, point1.y, color)
}

fun PApplet.line(coloredLine: ColoredLine) {
    this.line(coloredLine.point0, coloredLine.point1, coloredLine.color)
}

fun PApplet.line(gradientLine: LinearGradientLine) {
    // How to determine the subdivisions? For now, just by using roughly one pixel per
    val distance = (gradientLine.point1 - gradientLine.point0).magnitude()
    val divisions = distance.roundToInt()
//    val divisions = 100
    // If we don't have enough length for multiple pixels, just give up and use the mix
    if (divisions < 2) {
        line(
            gradientLine.point0,
            gradientLine.point1,
            gradientLine.color0.interpolate(gradientLine.color1, 0.5)
        )

    } else {
        val deltaPoint = (gradientLine.point1 - gradientLine.point0).normalized()
        val perIndexScale = (distance / divisions)
        for (i in 0..(divisions - 2)) {
            line(
                gradientLine.point0 + deltaPoint * i * perIndexScale,
                gradientLine.point0 + deltaPoint * (i + 1) * perIndexScale,
                gradientLine.color0.interpolate(gradientLine.color1, i.toDouble() / divisions)
            )
        }
    }
}

fun PApplet.bezier(
    start: DPoint,
    control0: DPoint,
    control1: DPoint,
    finish: DPoint
) {
    this.bezier(
        start.x.toFloat(),
        start.y.toFloat(),
        control0.x.toFloat(),
        control0.y.toFloat(),
        control1.x.toFloat(),
        control1.y.toFloat(),
        finish.x.toFloat(),
        finish.y.toFloat()
    )
}


fun PApplet.vertex(point0: DPoint) {
    this.vertex(point0.x, point0.y)
}

fun PApplet.vertex(x: Double, y: Double) {
    this.vertex(x.toFloat(), y.toFloat())
}

fun PApplet.shape(points: List<DPoint>) {
    beginShape()
    points.forEach { vertex(it) }
    vertex(points[0])
    endShape()
}


fun PApplet.hollowShape(points: List<DPoint>, interiorPoints: List<DPoint>) {
    beginShape()
    points.forEach { vertex(it) }
    vertex(points[0])
    beginContour()
    interiorPoints.forEach { vertex(it) }
    endContour()
    endShape()
}

fun PApplet.rectViaSize(corner: DPoint, width: Double, height: Double, rounding: Double) {
    rectMode(CORNER)
    rect(
        corner.x.toFloat(),
        corner.y.toFloat(),
        width.toFloat(),
        height.toFloat(),
        rounding.toFloat()
    )
}

fun PApplet.rectViaSizeOffsetRotation(
    corner: DPoint,
    width: Double,
    height: Double,
    rounding: Double,
    rotation: Double
) {
    rotate(rotation)
    rectViaSize(corner, width, height, rounding)
    rotate(-rotation)
}

fun PApplet.rectViaCorners(corner: DPoint, oppositeCorner: DPoint) {
    rectViaCorners(corner, oppositeCorner, 0.0)
}

fun PApplet.rectViaCorners(corner: DPoint, oppositeCorner: DPoint, rounding: Double) {
    val existing = recorder?.rectMode ?: CORNER
    rectMode(CORNERS)
    rect(
        corner.x.toFloat(),
        corner.y.toFloat(),
        oppositeCorner.x.toFloat(),
        oppositeCorner.y.toFloat(),
        rounding.toFloat()
    )
    rectMode(existing)
}


data class Triangle(val point0: DPoint, val point1: DPoint, val point2: DPoint)

fun PApplet.triangle(triangle: Triangle) {
    triangle(triangle.point0, triangle.point1, triangle.point2)
}

fun PApplet.triangle(point0: DPoint, point1: DPoint, point2: DPoint) {
    triangle(
        point0.x.toFloat(),
        point0.y.toFloat(),
        point1.x.toFloat(),
        point1.y.toFloat(),
        point2.x.toFloat(),
        point2.y.toFloat()
    )
}

fun PApplet.circle(center: DPoint, radius: Double) {
    arc(
        center.x.toFloat(),
        center.y.toFloat(),
        (radius * 2).toFloat(),
        (radius * 2).toFloat(),
        0.0.toFloat(),
        (2 * Math.PI).toFloat()
    )
}

/**
 * Angles in radians
 */
fun PApplet.arc(
    center: DPoint,
    radius: Double,
    startAngle: Double,
    endAngle: Double
) {

    arc(
        center.x.toFloat(),
        center.y.toFloat(),
        (radius * 2).toFloat(),
        (radius * 2).toFloat(),
        startAngle.toFloat(),
        endAngle.toFloat()
    )
}

fun PApplet.gradientArc(
    center: DPoint,
    radius: Double,
    startAngle: Double,
    endAngle: Double,
    color0: Color,
    color1: Color
) {

    val steps = radius.toInt() * 10
    val perStep = (endAngle - startAngle) / steps
    for (i in 0..steps) {
        val angle0 = startAngle + i * perStep
        val angle1 = startAngle + (i + 1) * perStep

        val color = color0.interpolate(color1, (i.toDouble() / steps))

        stroke(color)
        arc(
            center,
            radius,
            angle0,
            angle1
        )
    }
}

/**
 * Just uses the line api directly rather than the shapes api
 */
fun PApplet.rawShape(points: List<DPoint>) {
    points.windowedCircular(2, 1).forEach { (point0: DPoint, point1: DPoint) ->
        vertex(point0)
    }
}

/**
 * Creates a border with the pre-specified fill
 */
fun PApplet.borderWindow(size: Double) {
    rectViaCorners(
        DPoint(0.0, 0.0),
        DPoint(720.0, size)
    )
    rectViaCorners(
        DPoint(720.0 - size, 0.0),
        DPoint(720.0, 720.0)
    )
    rectViaCorners(
        DPoint(0.0, 720.0 - size),
        DPoint(720.0, 720.0)
    )
    rectViaCorners(
        DPoint(0.0, 0.0),
        DPoint(size, 720.0)
    )
}

fun PApplet.applyBitMap(bitMapCanvas: BitMapCanvas) {
    graphics.loadPixels()
    graphics.pixels = bitMapCanvas.toProcessingArray()
    graphics.updatePixels()
}
