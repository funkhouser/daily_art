package art.funk.daily.core

import art.funk.daily.core.applet.rotate
import art.funk.daily.core.applet.scale
import art.funk.daily.core.applet.translate
import processing.core.PApplet

typealias RecursiveDrawFunction = (Int) -> Unit

object RecursiveDraw {

    /**
     * Shrinks and angles the diamondCanvas by the given angle and multiplies
     * the size by the shrinkFactor each time
     */
    fun recurseAngleDraw(
        applet: PApplet,
        drawFunction: RecursiveDrawFunction,
        angle: Double,
        shrinkFactor: Double,
        iterations: Int
    ) {
        val midPoint = DPoint(applet.width.toDouble() / 2, applet.height.toDouble() / 2)
        recurseAngleDraw(applet, drawFunction, angle, shrinkFactor, iterations, midPoint)
    }

    fun recurseAngleDraw(
        applet: PApplet,
        drawFunction: RecursiveDrawFunction,
        angle: Double,
        shrinkFactor: Double,
        iterations: Int,
        pivotPoint: DPoint
    ) {
        var currentShrink = 1.0
        var currentAngle = 0.0
        for (i in 1..iterations) {
            applet.translate(pivotPoint)
            applet.rotate(currentAngle)
            applet.translate(pivotPoint.opposite())
            applet.scale(currentShrink)

            drawFunction(i)

            applet.scale(1.0 / currentShrink)
            applet.translate(pivotPoint)
            applet.rotate(-currentAngle)
            applet.translate(pivotPoint.opposite())

            currentAngle += angle
            currentShrink *= shrinkFactor

        }
    }


    fun indexedRecurseAngleDraw(
        applet: PApplet,
        drawFunction: RecursiveDrawFunction,
        angle: Double,
        shrinkFactor: Double,
        i: Int
    ) {
        val midPoint = DPoint(applet.width.toDouble() / 2, applet.height.toDouble() / 2)
        indexedRecurseAngleDraw(applet, drawFunction, angle, shrinkFactor, i, midPoint)
    }

    fun indexedRecurseAngleDraw2(
        applet: PApplet,
        drawFunction: RecursiveDrawFunction,
        angle: Double,
        shrinkFactor: Double,
        i: Int
    ) {
        val pivotPoint = DPoint(applet.width.toDouble() / 2, applet.height.toDouble() / 2)
        applet.rotate(angle * i, pivotPoint)
        applet.scale(Math.pow(shrinkFactor, i.toDouble()))
        drawFunction(i)
    }

    fun indexedRecurseAngleDraw(
        applet: PApplet,
        drawFunction: RecursiveDrawFunction,
        angle: Double,
        shrinkFactor: Double,
        i: Int,
        pivotPoint: DPoint
    ) {
        applet.rotate(angle * i, pivotPoint)
        applet.translate(pivotPoint)
        applet.scale(Math.pow(shrinkFactor, i.toDouble()))
        drawFunction(i)
    }
}