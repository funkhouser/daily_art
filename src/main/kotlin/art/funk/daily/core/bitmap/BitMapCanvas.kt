package art.funk.daily.core.bitmap

import art.funk.daily.core.DPoint
import art.funk.daily.core.HighColor
import art.funk.daily.core.LinearGradientLine
import art.funk.daily.core.bitmap.CoreMath.fractionalPart
import art.funk.daily.core.bitmap.CoreMath.fractionalRemain
import art.funk.daily.core.interpolate
import java.awt.Color
import kotlin.math.roundToInt

/**
 * Internally the bitMapCanvas will store everything in HighColor to improve the rendering
 * where lots of colors are blended together. The Api however will consist of Color so that
 * there are not extra steps to interact with this class.
 */
class BitMapCanvas(val width: Int, val height: Int) {

    val pixels: Array<Array<HighColor>> = Matrix.create(width, height, baseColor)

    companion object {
        val baseColor = HighColor(0, 0, 0, 1.0)
    }

    fun setBackground(color: Color) {
        everyPixel { x, y -> HighColor(color) }
    }

    fun applyPixel(x: Int, y: Int, color: Color) {
        applyPixel(x, y, HighColor(color))
    }

    fun applyEveryPixel(pixelFunction: (x: Int, y: Int) -> Color?) {
        everyPixelApplied { x, y ->
            val color = pixelFunction(x, y)
            if (color == null) {
                HighColor(0, 0, 0, 0.0)
            } else {
                HighColor(color)
            }
        }
    }

    private fun applyPixel(x: Int, y: Int, color: HighColor) {
        if (x < 0 || x >= width || y < 0 || y >= height) {
            return
        }
        pixels[x][y] = pixels[x][y].apply(color)
    }

    fun toProcessingArray(): IntArray {
        val returnArray = IntArray(width * height)
        for (x in 0 until width) {
            for (y in 0 until height) {
                returnArray[x + y * width] = pixels[x][y].toProcessingInt()
            }
        }
        return returnArray
    }

    // TODO Take the line function and update the color piece to being a callback based
    // upon the proportion of the line piece that has been completed

    fun line(gradientLine: LinearGradientLine) {
        line(
            gradientLine.point0.x,
            gradientLine.point0.y,
            gradientLine.point1.x,
            gradientLine.point1.y
        ) { percent -> HighColor(gradientLine.color0.interpolate(gradientLine.color1, percent)) }
    }

    fun line(point0: DPoint, point1: DPoint, color: Color) {
        line(point0.x, point0.y, point1.x, point1.y, color)
    }

    fun line(x0: Double, y0: Double, x1: Double, y1: Double, color: Color) {
        line(x0, y0, x1, y1) { HighColor(color) }
    }

    private fun line(x0: Double, y0: Double, x1: Double, y1: Double, colorFunction: (Double) -> HighColor) {
        val isSteep = Math.abs(y1 - y0) > Math.abs(x1 - x0)
        val (xa, ya, xb, yb) = if (isSteep) {
            if (y0 > y1) {
                listOf(y1, x1, y0, x0)
            } else {
                listOf(y0, x0, y1, x1)
            }
        } else {
            if (x0 > x1) {
                listOf(x1, y1, x0, y0)
            } else {
                listOf(x0, y0, x1, y1)
            }
        }

        val dx = xb - xa
        val dy = yb - ya
        val gradient = if (dx == 0.0) {
            1.0
        } else {
            dy / dx
        }

        // Color first Endpoint
        val xEnd1 = xa.roundToInt()
        val yEnd1 = ya + gradient * (xEnd1 - xa)
        val xGap1 = fractionalRemain(xa + 0.5)
        val xp11 = xEnd1
        val yp11 = yEnd1.toInt()

        var color = colorFunction(0.0)
        if (isSteep) {
            applyPixel(yp11, xp11, color.percent(fractionalRemain(yEnd1) * xGap1))
            applyPixel(yp11 + 1, xp11, color.percent(fractionalPart(yEnd1) * xGap1))
        } else {
            applyPixel(xp11, yp11, color.percent(fractionalRemain(yEnd1) * xGap1))
            applyPixel(xp11, yp11 + 1, color.percent(fractionalPart(yEnd1) * xGap1))
        }

        if (x0 == x1 && y0 == y1) {
            return
        }
        // Color second Endpoint
        val xEnd2 = xb.roundToInt()
        val yEnd2 = yb + gradient * (xEnd2 - xb)
        val xGap2 = fractionalRemain(xb + 0.5)
        val xp12 = xEnd2
        val yp12 = yEnd2.toInt()

        color = colorFunction(1.0)
        if (isSteep) {
            applyPixel(yp12, xp12, color.percent(fractionalRemain(yEnd2) * xGap2))
            applyPixel(yp12 + 1, xp12, color.percent(fractionalPart(yEnd2) * xGap2))
        } else {
            applyPixel(xp12, yp12, color.percent(fractionalRemain(yEnd2) * xGap2))
            applyPixel(xp12, yp12 + 1, color.percent(fractionalPart(yEnd2) * xGap2))
        }
        var currentY = yEnd1 + gradient
        for (x in (xp11 + 1)..(xp12 - 1)) {
            color = colorFunction((currentY - yEnd1) / (yEnd2 - yEnd1))
            if (isSteep) {
                applyPixel(currentY.toInt(), x, color.percent(fractionalRemain(currentY)))
                applyPixel(currentY.toInt() + 1, x, color.percent(fractionalPart(currentY)))
            } else {
                applyPixel(x, currentY.toInt(), color.percent(fractionalRemain(currentY)))
                applyPixel(x, currentY.toInt() + 1, color.percent(fractionalPart(currentY)))
            }
            currentY += gradient
        }
    }

    private fun everyPixel(pixelFunction: (x: Int, y: Int) -> HighColor) {
        for (x in 0 until width) {
            for (y in 0 until height) {
                pixels[x][y] = pixelFunction(x, y)
            }
        }
    }

    private fun everyPixelApplied(pixelFunction: (x: Int, y: Int) -> HighColor) {
        for (x in 0 until width) {
            for (y in 0 until height) {
                pixels[x][y] = pixels[x][y].apply(pixelFunction(x, y))
            }
        }
    }
}

object CoreMath {
    fun fractionalPart(num: Double): Double {
        return num - num.toInt()
    }

    fun fractionalRemain(num: Double): Double {
        return 1 - fractionalPart(num)
    }
}


object Matrix {

    inline fun <reified T> create(width: Int, height: Int, initElement: T): Array<Array<T>> {
        var matrix = arrayOf<Array<T>>()
        for (x in 0 until width) {
            var yArray = arrayOf<T>()
            for (y in 0 until height) {
                yArray += initElement
            }
            matrix += yArray
        }
        return matrix
    }
}