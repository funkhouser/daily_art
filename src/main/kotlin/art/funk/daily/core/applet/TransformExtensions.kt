package art.funk.daily.core.applet

import art.funk.daily.core.DPoint
import processing.core.PApplet

fun PApplet.translate(point: DPoint) { this.translate(point.x.toFloat(), point.y.toFloat()) }

fun PApplet.rotate(angle: Double) { this.rotate(angle.toFloat()) }

fun PApplet.rotate(angle: Double, point: DPoint) {
    this.translate(point)
    this.rotate(angle)
    this.translate(point.opposite())
}

fun PApplet.scale(amount: Double) { this.scale(amount.toFloat()) }