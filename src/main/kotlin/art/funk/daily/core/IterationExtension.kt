package art.funk.daily.core

import java.awt.Color


// Time based description of a parametric function
typealias IterativePointFunction = (Int) -> DPoint

typealias IterativeColoredLineFunction = (Int) -> ColoredLine

typealias IterativeGradientFunction = (Int) -> Pair<Color, Color>
typealias IterativeGradientLineFunction = (Int) -> LinearGradientLine