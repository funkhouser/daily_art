package art.funk.daily.core.math

fun linearRange(initial: Int, final: Int, i: Int, totalIterations: Int): Int {
    return linearRange(initial.toDouble(), final.toDouble(), i, totalIterations).toInt()
}

fun linearRange(initial: Double, final: Double, i: Int, totalIterations: Int): Double {
    val delta = (initial - final)
    return (delta * i) / totalIterations + final
}