package art.funk.daily.core

fun <T> List<T>.windowedCircular(size: Int, step: Int = 1): List<List<T>> {
    val normalWindowResult = this.windowed(size, step, false)
    val listSize = this.size
    val lastIndex = normalWindowResult.size
    val newWindows = (0..(listSize - normalWindowResult.size - 1)).map { index ->
        val newWindow = (0..(size - 1)).map { windowIndex ->
            this[Math.floorMod(lastIndex + index + windowIndex, listSize)]
        }
        newWindow
    }
    return normalWindowResult + newWindows
}