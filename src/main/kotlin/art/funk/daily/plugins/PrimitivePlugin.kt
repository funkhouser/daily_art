package art.funk.daily.plugins

import java.awt.Color
import java.io.File
import java.lang.RuntimeException

object PrimitiveTester {
    @JvmStatic
    fun main(args: Array<String>) {
        val primitiveMainGo = args[0]
        val primitiveFileInput = args[1]
        val primitiveFileOutput = args[2]

        PrimitivePlugin.goFilePath = primitiveMainGo
        val api = PrimitiveApi(
                PrimitiveShape.HEXAGON,
                400
            )

        PrimitivePlugin.primitizeFile(
            File(primitiveFileInput),
            File(primitiveFileOutput),
            api
        )
    }
}

object PrimitivePlugin {

    var goFilePath: String = System.getProperty("primitivePath")

    init {
        if (goFilePath.isEmpty()) {
            error("Unable to load property `primitivePath`")
        }
    }

    fun primitizeFile(input: File, output: File, primitiveApi: PrimitiveApi) {
        val process = ProcessBuilder(
            "go",
            "run",
            goFilePath,
            "-i",
            input.absolutePath,
            "-o",
            output.absolutePath,
            "-n",
            primitiveApi.count.toString())
            .inheritIO()
            .start()

        val errorCode = process.waitFor()
        if (errorCode != 0) {
            throw RuntimeException(errorCode.toString())
        }
    }

}

enum class PrimitiveShape {
    COMBO,
    TRIANGLE,
    RECT,
    ELLIPSE,
    CIRCLE,
    ROTATED_RECT,
    BEZIER,
    ROTATED_ELLIPSE,
    POLYGON,
    HEXAGON,
    REGULAR_POLYGON,
    REGULAR_ROTATED_POLYGON
}

class PrimitiveApi(val shape: PrimitiveShape, val count: Int) {
    var shapesPerIterationExtra: Int = 0
    var polygonSides = 6
    var maxShapeSize: Int? = null
    var additionalApi = PrimitiveAdditionalApi()
    var saveSettings = PrimitiveSaveSettings()

}

class PrimitiveAdditionalApi {
    var alpha: Int = 128
    var background: Color = Color(128, 128, 128, 255)
    var resizeInput: Int = 256
}

class PrimitiveSaveSettings {
    var createFrames: Boolean = false
    var outputSize: Int = 720
}
